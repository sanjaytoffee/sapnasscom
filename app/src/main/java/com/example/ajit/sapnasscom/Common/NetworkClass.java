package com.example.ajit.sapnasscom.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by HSTPL on 2/28/2018.
 */

public class NetworkClass {

    private Context _context;
    public  boolean isNetworkAvailable()
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public NetworkClass(Context context) {
        this._context = context;
    }
}

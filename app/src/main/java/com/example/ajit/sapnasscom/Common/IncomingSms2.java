package com.example.ajit.sapnasscom.Common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.example.ajit.sapnasscom.Activity.SignupValidateActivity;

public class IncomingSms2 extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    try {
                        if (senderNum.equals("MD-UNNATI")) {
                            SignupValidateActivity Sms = new SignupValidateActivity();
                            Sms.recivedSms(message);
                        }
                        SignupValidateActivity.mPinFirstDigitEditText.setText("" + SignupValidateActivity.a0);
                        SignupValidateActivity.mPinSecondDigitEditText.setText("" + SignupValidateActivity.a1);
                        SignupValidateActivity.mPinThirdDigitEditText.setText("" + SignupValidateActivity.a2);
                        SignupValidateActivity.mPinForthDigitEditText.setText("" + SignupValidateActivity.a3);

                        Log.e("FIRST","" + SignupValidateActivity.a0);
                        Log.e("SECOND","" + SignupValidateActivity.a1);
                        Log.e("THIRD","" + SignupValidateActivity.a2);
                        Log.e("FOURTG","" + SignupValidateActivity.a3);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
        }
    }
}
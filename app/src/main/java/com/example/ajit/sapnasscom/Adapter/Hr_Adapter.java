package com.example.ajit.sapnasscom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Model.HrQuestion;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

/**
 * Created by DTP-110 on 1/27/2017.
 */

public class Hr_Adapter extends BaseAdapter
{

    Activity mActivity;
    ArrayList<HrQuestion> QuestionTypeList;

    public Hr_Adapter(Activity a, ArrayList<HrQuestion> data) {
        this.mActivity = a;
        this.QuestionTypeList = data;
    }

    @Override
    public int getCount() {
        return QuestionTypeList.size();
    }

    @Override
    public Object getItem(int position) {
        return QuestionTypeList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.hrquestion_text, null);

            viewHolder.hr_text = (TextView) convertView
                    .findViewById(R.id.question_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (QuestionTypeList != null) {
            try {
                viewHolder.hr_text.setText("" + QuestionTypeList.get(position).getName());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView hr_text;
    }

}



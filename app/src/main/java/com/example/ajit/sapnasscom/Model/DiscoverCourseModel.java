package com.example.ajit.sapnasscom.Model;

/**
 * Created by pratik on 2/19/2018.
 */

public class DiscoverCourseModel {

    public String getKeyFeature() {
        return KeyFeature;
    }

    public void setKeyFeature(String keyFeature) {
        KeyFeature = keyFeature;
    }

    public   String KeyFeature;
    public boolean isEnrolled() {
        return Enrolled;
    }

    public void setEnrolled(boolean enrolled) {
        Enrolled = enrolled;
    }

    public boolean Enrolled;
    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getLanguageName() {
        return LanguageName;
    }

    public void setLanguageName(String languageName) {
        LanguageName = languageName;
    }

    String CreatedOn;
    String LanguageName;
    double Id;
    String CourseName;
    String isFeedback;
    String Description;
    String Duration;
    String Creator;
    String CityName;

    public int getSkillId() {
        return SkillId;
    }

    public void setSkillId(int skillId) {
        SkillId = skillId;
    }

    int SkillId;

    public String getLearnStatus() {
        return LearnStatus;
    }

    public void setLearnStatus(String learnStatus) {
        LearnStatus = learnStatus;
    }

    String LearnStatus;

    public String getIsFeedback() {
        return isFeedback;
    }

    public void setIsFeedback(String isFeedback) {
        this.isFeedback = isFeedback;
    }

    public double getId() {
        return Id;
    }

    public void setId(double id) {
        Id = id;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCreator() {
        return Creator;
    }

    public void setCreator(String creator) {
        Creator = creator;
    }


    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }


}

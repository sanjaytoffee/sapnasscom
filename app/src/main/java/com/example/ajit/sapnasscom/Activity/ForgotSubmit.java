package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

/**
 * Created by HSTPL on 2/20/2018.
 */

public class ForgotSubmit extends AppCompatActivity {

    Toolbar toolbar;
    Button btnSubmit;
    String status;
    EditText ednewPassword, edconfirmPassword;
    String userEmail;
    SessionManager sessionManager;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_submit);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Forgot Password?");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        sessionManager = new SessionManager(getApplicationContext());

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        btnSubmit = (Button) findViewById(R.id.final_submit);

        ednewPassword = (EditText) findViewById(R.id.ednewPassword);
        edconfirmPassword = (EditText) findViewById(R.id.edconfirmPassword);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                HashMap<String, String> getmail = sessionManager.getEmail();
//                userEmail = getmail.get(SessionManager.KEY_EMAIL);

                userEmail = PrefUtils.getUserEmail(ForgotSubmit.this);

                if (ednewPassword.getText().toString().length() == 0) {
                    Toast.makeText(ForgotSubmit.this, "Please enter new password", Toast.LENGTH_SHORT).show();
                } else if (edconfirmPassword.getText().toString().length() == 0) {
                    Toast.makeText(ForgotSubmit.this, "Please enter confirm password", Toast.LENGTH_SHORT).show();
                } else if (!ednewPassword.getText().toString().equalsIgnoreCase(edconfirmPassword.getText().toString())) {
                    Toast.makeText(ForgotSubmit.this, "New Password and Confirm Password doesn't match", Toast.LENGTH_SHORT).show();
                } else {
                    new HttpAsyncTaskForgot().execute(CommonUrl.getDashboardData + "ManageForgetPassword");
                }
            }
        });
    }

    private class HttpAsyncTaskForgot extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskForgot() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {

            return forgetData(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (status.equalsIgnoreCase("Success")) {
                Common.showApplyDialog(ForgotSubmit.this, getString(R.string.reset_password));
                Intent intent = new Intent(getApplicationContext(), Main.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String forgetData(String url) {
        String result = "";

        try {
            JSONObject params = new JSONObject();
            params.put("emailID", userEmail);
            params.put("password", computeMD5Hash(edconfirmPassword.getText().toString()));

            Log.e("FORGOTPASSWORD", String.valueOf(params));

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());
                    Log.e("FORGOTRES",jsonoObject.toString());
                    status = jsonoObject.getString("Result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public final String computeMD5Hash(final String pass) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(pass.getBytes());
            byte messageDigest[] = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            System.out.println(hexString.toString());
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}

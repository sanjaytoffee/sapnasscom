package com.example.ajit.sapnasscom.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Activity.DetailsActivity;
import com.example.ajit.sapnasscom.Activity.LetsLearnActivity;
import com.example.ajit.sapnasscom.Activity.MycourseActivity;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.DiscoverCourseModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pratik on 12/7/2017.
 */

public class DiscoverCourseAdapter extends RecyclerView.Adapter<DiscoverCourseAdapter.MyViewHolder> {

    private List<DiscoverCourseModel> arrCourse;
    private Context context;
    LocaleClass localeClass;
    int count = 0, d;
    private String userId;
    SessionManager sessionManager;
    boolean EnrolledValue;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tv_course_languages, tv__cource_location, tv_course_duration, tvCourseName, tvDate, tvCreatedby, tvDuration, tvLocation, tvLanguage, tvEnrollNow, tvCourseDescription;
        private ImageView ivSite;

        public MyViewHolder(View view) {
            super(view);
            tvCourseName = (TextView) view.findViewById(R.id.tv_course_department);
            tv_course_duration = (TextView) view.findViewById(R.id.tv_course_duration);
            tv__cource_location = (TextView) view.findViewById(R.id.tv__cource_location);
            tv_course_languages = (TextView) view.findViewById(R.id.tv_course_languages);
            tvDate = (TextView) view.findViewById(R.id.tv_coursedate);
            tvCreatedby = (TextView) view.findViewById(R.id.tv_coursecompany_name);
            tvDuration = (TextView) view.findViewById(R.id.tv_course_experience_number);
            tvLocation = (TextView) view.findViewById(R.id.tv_course_location_name);
            tvLanguage = (TextView) view.findViewById(R.id.tv_course_laguages_name);
            tvCourseDescription = (TextView) view.findViewById(R.id.tv_course_detaill);
            tvEnrollNow = (TextView) view.findViewById(R.id.tv_course_enroll_now);
        }
    }

    public DiscoverCourseAdapter(Context context, List<DiscoverCourseModel> arrCourse) {
        this.context = context;
        this.arrCourse = arrCourse;
        sessionManager = new SessionManager(context);
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(context, "DiscoverCourseAdapter");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_discover_course, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Utilities.showLog(String.valueOf("Size=" + position));
        final DiscoverCourseModel discoverCourseModel = arrCourse.get(position);

        holder.tvCourseName.setText(discoverCourseModel.getCourseName());
        holder.tvCreatedby.setText("Created By: " + discoverCourseModel.getCreator());
        holder.tvDuration.setText(String.valueOf(discoverCourseModel.getDuration()) + " Weeks");
        holder.tvLocation.setText(discoverCourseModel.getCityName());
        holder.tvLanguage.setText(discoverCourseModel.getLanguageName());
        holder.tvDate.setText(discoverCourseModel.getCreatedOn());

        if (discoverCourseModel.isEnrolled()) {
            if (discoverCourseModel.getLearnStatus().equalsIgnoreCase("Let learn")) {
                holder.tvEnrollNow.setText(R.string.start_course);
            } else if (discoverCourseModel.getLearnStatus().equalsIgnoreCase("Finish")) {
                holder.tvEnrollNow.setText(R.string.finishh);
            } else {
                holder.tvEnrollNow.setText(R.string.continue_course);
            }
            //holder.tvEnrollNow.setText(R.string.Enrolled);
        } else {
            holder.tvEnrollNow.setText(R.string.Enroll_Now);
        }

        count = 0;
        holder.tvEnrollNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if (count == 1) {
                    if (discoverCourseModel.isEnrolled()) {
                        Utilities.showLog("description=" + discoverCourseModel.getDescription());
                        d = Integer.parseInt(String.valueOf(discoverCourseModel.getId()).replace(".0", ""));
                        Intent intent = new Intent(context, LetsLearnActivity.class);
                        intent.putExtra("courseid", String.valueOf(d));
                        intent.putExtra("LearnStatus", discoverCourseModel.getLearnStatus());
                        intent.putExtra("enrolledValue", discoverCourseModel.isEnrolled());
                        intent.putExtra("courseName", discoverCourseModel.getCourseName());
                        intent.putExtra("SkillId", discoverCourseModel.getSkillId());
                        intent.putExtra("SkillId", discoverCourseModel.getSkillId());

                        EnrolledValue = discoverCourseModel.isEnrolled();

                        if (Common.isInternet(context)) {
                            if (discoverCourseModel.getLearnStatus().equalsIgnoreCase("Let Learn")) {
                                new HttpAsyncTaskCourse().execute(CommonUrl.myCourseClick);
                            }
                        } else {
                            Toast.makeText(context, "Please Connect Internet", Toast.LENGTH_SHORT).show();
                        }
                        context.startActivity(intent);
                    } else {
                        Intent localImageintent = new Intent();
                        localImageintent.setAction("hitDiscoverEnrolledApi");
                        localImageintent.putExtra("Discover_course_enrolled_id", discoverCourseModel.getId());
                        context.sendBroadcast(localImageintent);
                    }
                }
            }
        });


        holder.tvCourseDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d = Integer.parseInt(String.valueOf(discoverCourseModel.getId()).replace(".0", ""));
                Intent intent = new Intent(context, DetailsActivity.class);
                intent.putExtra("COURSEID", String.valueOf(d));
                intent.putExtra("TITTLE", discoverCourseModel.getCourseName());
                intent.putExtra("DURATION", String.valueOf(discoverCourseModel.getDuration()) + " Weeks");
                intent.putExtra("LOCATION", discoverCourseModel.getCityName());
                intent.putExtra("LANGUAGE", discoverCourseModel.getLanguageName());
                intent.putExtra("COURSEENROLLED", discoverCourseModel.isEnrolled());
                intent.putExtra("KEYFEATURES", discoverCourseModel.getKeyFeature());
                intent.putExtra("DESCRIPTION", discoverCourseModel.getDescription());
                intent.putExtra("LEARNSTATUS", discoverCourseModel.getLearnStatus());
                intent.putExtra("SkillId", discoverCourseModel.getSkillId());
                context.startActivity(intent);
            }
        });
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return arrCourse.size();
    }

    private class HttpAsyncTaskCourse extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourse() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(context);
        }

        protected String doInBackground(String... urls) {
            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MyProgressDialog.hideDialog();
        }
    }

    public String getCourse(String url) {
        String result = "";
        System.out.print("MYCOURSEACTIVITY_URL : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", userId);
            params.put("CourseID", d);
            if (EnrolledValue == true) {
                params.put("Enrolled", 1);
            } else {
                params.put("Enrolled", 0);
            }

            params.put("LearnStatus", "Progress");

            Log.e("MYCOURSEACTIVITY", params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}

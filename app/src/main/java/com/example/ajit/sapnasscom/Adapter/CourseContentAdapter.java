package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.Main;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Model.CourseContentModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Utilities;

import java.util.List;

/**
 * Created by pratik on 12/7/2017.
 */

public class CourseContentAdapter extends RecyclerView.Adapter<CourseContentAdapter.MyViewHolder> {

    private List<CourseContentModel> arrCourse;
    private Context context;
    LocaleClass localeClass;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tv_course_tittle, tv_course_tagname, tv_course_content, tv_course_creater,tv_discover_without_login;

        public MyViewHolder(View view) {
            super(view);
            tv_course_tittle = (TextView) view.findViewById(R.id.tv_course_tittle);
            tv_course_tagname = (TextView) view.findViewById(R.id.tv_course_tagname);
            tv_course_content = (TextView) view.findViewById(R.id.tv_course_content);
            tv_course_creater = (TextView) view.findViewById(R.id.course_creater);
            tv_discover_without_login=(TextView)view.findViewById(R.id.tv_discover_without_login);
        }
    }

    public CourseContentAdapter(Context context, List<CourseContentModel> arrCourse) {
        this.context = context;
        this.arrCourse = arrCourse;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_courcecontent, parent, false);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(context, "CourseContentAdapter");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Utilities.showLog(String.valueOf("Size=" + position));

        final CourseContentModel courseModel = arrCourse.get(position);

        holder.tv_course_tittle.setText(courseModel.getCourseTitle());
        holder.tv_course_tagname.setText(courseModel.getCourseTagname());
        holder.tv_course_content.setText((courseModel.getCourseContent()));
        holder.tv_course_creater.setText(courseModel.getCourseCreater());

        holder.tv_discover_without_login.setText(R.string.Course_Details);

        holder.tv_discover_without_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, Main.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrCourse.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

package com.example.ajit.sapnasscom.Adapter;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.EditSelectInterest;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.PrefUtils;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class EditSecondryAdapter extends BaseAdapter {

    ArrayList<Skills> result, update;
    ArrayList<String> skillList;

    public static JSONObject jObj;
    ArrayList<String> list;
    HashMap<String, String> hm;
    Context context;
    int[] imageId;
    int count = 0;
    Holder holder;
    EditCustomAdapter customAdapter;
    private static LayoutInflater inflater = null;

    public EditSecondryAdapter(Context context, ArrayList<Skills> osNameList) {
        // TODO Auto-generated constructor stub
        result = osNameList;
        this.context = context;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        jObj = new JSONObject();
        update = new ArrayList<>();
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView os_text;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        skillList = new ArrayList<String>();
        final Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.type_text, null);
        holder.os_text = (TextView) rowView.findViewById(R.id.os_texts);
        holder.os_text.setText("" + result.get(position).getName());
        holder.os_text.setOnClickListener(new OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub


                if (holder.os_text.getBackground() instanceof ColorDrawable) {
                    ColorDrawable cd = (ColorDrawable) holder.os_text.getBackground();
                    String colorCode = String.valueOf(cd.getColor());

                    if (colorCode.equals("-14325")) {


                        try {
                            if (EditSelectInterest.edit_skillsArrayList.size() != 0) {
                                for (int i = 0; i < EditSelectInterest.edit_skillsArrayList.size(); i++) {
                                    Log.e("Remove", "" + EditSelectInterest.edit_skillsArrayList.get(i).getSecondrySkills() + ", Position=" + result.get(position).getID());
                                    if (EditSelectInterest.edit_skillsArrayList.get(i).getSecondrySkills().equalsIgnoreCase(result.get(position).getID())) {
                                        Log.e("Change Remove", "" + EditSelectInterest.edit_skillsArrayList.get(i).getSecondrySkills() + ", Position=" + result.get(position).getID());
                                        EditSelectInterest.edit_skillsArrayList.remove(i);

                                    }
                                    Log.e("After Delete", "" + EditSelectInterest.edit_skillsArrayList.get(i).getSecondrySkills().toString());
                                }


                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        // contactsArray.put(contactsObj);


                        holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.textWhite));
                    } else {
                        try {
                            if (EditSelectInterest.edit_skillsArrayList.size() == 0) {
                                JSONObject json = new JSONObject();
                                json.put("Id", "0");
                                json.put("UserId", PrefUtils.getUserID(context));
                                json.put("PrimarySkills", result.get(position).getPrimarySkillsId());
                                json.put("SecondarySkills", result.get(position).getID());
                                EditSelectInterest.jsonArray.put(json);
                            } else {
                                Skills skills = new Skills();

                                skills.setID("0");
                                skills.setUserID(PrefUtils.getUserID(context));
                                skills.setPrimarySkills(result.get(position).getPrimarySkillsId());
                                skills.setSecondrySkills(result.get(position).getID());

                                EditSelectInterest.edit_skillsArrayList.add(skills);
                            }


                            Log.e("SELEct Skill", "" + EditSelectInterest.edit_skillsArrayList.toString() + "," + EditSelectInterest.jsonArray.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        result.get(position).setSelected(true);
                        holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.splashColor));
                    }
                }


//                boolean isSelectedAfterClick = !v.isSelected();
//                v.setSelected(isSelectedAfterClick);
//
//                if (isSelectedAfterClick) {
//                    for (int i =0;i< EditSelectInterest.jsonArray.length(); i++) {
//                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                            //EditSelectInterest.jsonArray.remove(position);
//                            Log.e("Remove",""+EditSelectInterest.jsonArray.remove(position));
//                        }
//                    }
//                    holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.splashColor));
//
//                } else {
//
//                    try {
//                        JSONObject json = new JSONObject();
//                        json.put("Id", "0");
//                        json.put("UserId", "100560");
//                        json.put("PrimarySkills", result.get(position).getPrimarySkillsId());
//                        json.put("SecondarySkills", result.get(position).getID());
//                        EditSelectInterest.jsonArray.put(json);
//                        Log.e("SELEct Skill", "" + EditSelectInterest.jsonArray.toString());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    result.get(position).setSelected(true);
//
//                    holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.textWhite));
//                }
            }
        });


        holder.os_text.setTag(position);
        if (result.get(position).isSelected()) {
            holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.splashColor));
        } else {
            holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.textWhite));
        }


        if (EditSelectInterest.edit_skillsArrayList.size() != 0) {
            for (int i = 0; i < EditSelectInterest.edit_skillsArrayList.size(); i++) {

                if (result.get(position).getID().equalsIgnoreCase(EditSelectInterest.edit_skillsArrayList.get(i).getSecondrySkills())) {
                    Log.e("Select", "" + EditSelectInterest.edit_skillsArrayList.get(i).getSecondrySkills());
                    holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.splashColor));

                }

            }
        }


        return rowView;
    }

}
package com.example.ajit.sapnasscom.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.asksira.loopingviewpager.LoopingViewPager;
import com.example.ajit.sapnasscom.Adapter.DemoInfiniteAdapter;
import com.example.ajit.sapnasscom.R;
import com.rd.PageIndicatorView;

public class DescoverSognUpDashBoard extends AppCompatActivity implements View.OnClickListener {
    LoopingViewPager viewPager;
    DemoInfiniteAdapter adapter;
    PageIndicatorView indicatorView;
    private int currentDataSet = 1;
    TextView tvDescover,tvSignin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descover_sogn_up_dash_board);
        init();
    }

    private void init() {
        tvDescover=(TextView) findViewById(R.id.tv_descover);
        tvSignin=(TextView) findViewById(R.id.tv_signin);
        tvDescover.setOnClickListener(this);
        tvSignin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_descover:
                Intent intent1=new Intent(DescoverSognUpDashBoard.this,DescoverWithoutLogin.class);
                startActivity(intent1);
//                Intent
                break;
            case R.id.tv_signin:
                Intent intent=new Intent(DescoverSognUpDashBoard.this,Main.class);
                startActivity(intent);
//                Intent
                break;
        }
    }
}

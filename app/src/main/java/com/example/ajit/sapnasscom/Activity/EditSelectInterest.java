package com.example.ajit.sapnasscom.Activity;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Adapter.EditCustomAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;


public class EditSelectInterest extends AppCompatActivity {

    GridView gridview;

    ArrayList<Skills> skillsArrayList, skillarray;
    public static ArrayList<Skills> edit_skillsArrayList;
    Skills skills;
    public static TextView tv_tool_right;
    JSONObject catObj;
    Toolbar toolbar;
    String getResponse;
    final Context context = this;
    EditCustomAdapter adapter;

    public static JSONArray jsonArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        jsonArray = new JSONArray();
        edit_skillsArrayList = new ArrayList<>();



        //gridview.setSelected(true);

//
//        final Dialog dialog = new Dialog(context);
//        dialog.setContentView(R.layout.no_internet);
//        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
//        // if button is clicked, close the custom dialog
//        dialogButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//                Toast.makeText(getApplicationContext(),"Dismissed..!!",Toast.LENGTH_SHORT).show();
//            }
//        });
//        dialog.show();
        getSupportActionBar().setTitle("Select Interest");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        gridview = (GridView) findViewById(R.id.customgrid);
        tv_tool_right = (TextView) findViewById(R.id.tv_tool_right);
        tv_tool_right.setText("SAVE");

        try {
            new HttpAsyncTask_Skils().execute(CommonUrl.getSelectIntrest);
        } catch (Exception e) {
            e.printStackTrace();
        }


        // new EditSecondryAdapter.Edit_HttpAsyncTask_Skils().execute("http://api.toffeemail.com/api/Unnati/GetUserSkillsByiD");


        tv_tool_right.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    new SaveInterest().execute(CommonUrl.setSelectIntrest);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


    }


    class HttpAsyncTask_Skils extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchSkill(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {


                adapter = new EditCustomAdapter(getApplicationContext(), skillsArrayList, edit_skillsArrayList);
                gridview.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                try {
                    new Edit_HttpAsyncTask_Skils().execute("http://api.toffeemail.com/api/Unnati/GetUserSkillsByiD");
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }
    }

    public String fetchSkill(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("ID", PrefUtils.getUserID(EditSelectInterest.this));
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                skillsArrayList = new ArrayList<>();
                skillarray = new ArrayList<>();
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {
                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());

                    for (int i = 0; i < jsonArray.length(); i++) {
                        catObj = jsonArray.getJSONObject(i);
                        skills = new Skills();

                        skills.setName(catObj.getString("PrimarySkillsName"));
                        skills.setPrimarySkills(catObj.getString("PrimarySkills"));

                        JSONArray jArray = catObj.getJSONArray("UserSecondarySkillsLists");
                        skillarray = new ArrayList<>();
                        if (jArray.length() != 0) {
                            for (int j = 0; j < jArray.length(); j++) {
                                JSONObject catObj2 = jArray.getJSONObject(j);
                                Skills skills = new Skills();
                                skills.setName(catObj2.getString("SkillsName"));
                                skills.setID(catObj2.getString("Id"));
                                skills.setPrimarySkillsId(catObj2.getString("MainSkillsId"));
                                skillarray.add(skills);
                            }
                        }
                        skills.setSecondryList(skillarray);
                        skillsArrayList.add(skills);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    class SaveInterest extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return SaveInterest(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                if (getResponse.equals("Success")) {
                    Common.showUpdatedProfileDialog(EditSelectInterest.this);
                    Toast.makeText(EditSelectInterest.this, "" + getResponse, Toast.LENGTH_SHORT).show();
                }

            }
        }
    }

    public String SaveInterest(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            if (edit_skillsArrayList.size() != 0) {
                for (int i = 0; i < edit_skillsArrayList.size(); i++) {
                    JSONObject contact = new JSONObject();
                    contact.put("Id", edit_skillsArrayList.get(i).getID());
                    contact.put("UserId", edit_skillsArrayList.get(i).getUserID());
                    contact.put("PrimarySkills", edit_skillsArrayList.get(i).getPrimarySkills());
                    contact.put("PrimarySkillsName", "");
                    contact.put("SecondarySkills", edit_skillsArrayList.get(i).getSecondrySkills());
                    //contactsArray.put(contact);

                    jsonArray.put(contact);

                }
            }
            params.put("UserSkillsLists", jsonArray);
            Log.e("Save", "" + jsonArray.toString());
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }

            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                skillsArrayList = new ArrayList<>();
                skillarray = new ArrayList<>();
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    JSONObject json = new JSONObject(line);
                    getResponse = json.getString("Result");

                    // Log.d("Resonse",""+json.getString("Result"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class Edit_HttpAsyncTask_Skils extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetch_edit_interest(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                adapter = new EditCustomAdapter(getApplicationContext(), skillsArrayList, edit_skillsArrayList);
                gridview.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

        }
    }

    public String fetch_edit_interest(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", PrefUtils.getUserID(EditSelectInterest.this));
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                edit_skillsArrayList = new ArrayList<>();
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {
                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject edit_catObj = jsonArray.getJSONObject(i);
                        skills = new Skills();

                        skills.setID(edit_catObj.getString("Id"));
                        skills.setUserID(edit_catObj.getString("UserId"));
                        skills.setPrimarySkills(edit_catObj.getString("PrimarySkills"));
                        skills.setSecondrySkills(edit_catObj.getString("SecondarySkills"));

                        edit_skillsArrayList.add(skills);


                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}
package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Model.InterviewModel;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

/**
 * Created by Abhinav on 29-09-2017.
 */

public class InterviewAdapter extends ArrayAdapter<InterviewModel> {

    ArrayList<InterviewModel> questionList;
    LayoutInflater vi;
    int Resource;
    ViewHolder holder;
    Context context;


    public InterviewAdapter(Context context, int resource, ArrayList<InterviewModel> objects) {
        super(context, resource, objects);
        vi = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = resource;
        questionList = objects;
        this.context = context;
    }

    static class ViewHolder {
        public TextView tv_question, tv_answer;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // convert view = design
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);

            holder.tv_question = (TextView) v.findViewById(R.id.tv_question);
            holder.tv_answer = (TextView) v.findViewById(R.id.tv_answer);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.tv_question.setText(questionList.get(position).getInterviewQues());
        holder.tv_answer.setText(questionList.get(position).getInterviewAns());

        return v;
    }


}





package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import com.example.ajit.sapnasscom.Adapter.DiscoverJobsAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.DiscoverJobModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.dialog;

/**
 * Created by HSTPL on 3/7/2018.
 */

public class JobActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView rvJobs;
    DiscoverJobsAdapter discoverJobsAdapter;
    DiscoverJobModel discoverJobModel;
    ArrayList<DiscoverJobModel> arrJobsList;
    boolean enrolledStatus = false;
    SessionManager sessionManager;
    String userId;
    String jobId;
    Boolean AppliedStatus;
    double jobAppliedId;
    ProgressDialog pd;
    Parcelable recyclerViewState;

    public void onDestroy() {
        super.onDestroy();
        if (pd.isShowing()) {
            pd.dismiss();
        }
        JobActivity.this.unregisterReceiver(appliedJobApiCall);
    }

    public void onResume() {
        super.onResume();
        try {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            if (pd.isShowing()) {
                pd.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        new HttpAsyncTaskgetJobs().execute(CommonUrl.getDashboardData + "GetJobByJobId");
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover_job);

        JobActivity.this.registerReceiver(appliedJobApiCall, new IntentFilter("hitApplyjobApi"));

        rvJobs = (RecyclerView) findViewById(R.id.rc_job_activity);
        arrJobsList = new ArrayList<>();
        sessionManager = new SessionManager(getApplicationContext());

        pd = new ProgressDialog(JobActivity.this);
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.Jobs));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        setAdapter();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.dismiss();
                onBackPressed();
            }
        });

        jobId = getIntent().getStringExtra("JobId");
        Log.e("JOBID", jobId);
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvJobs.setLayoutManager(mLayoutManager);
        rvJobs.setItemAnimator(new DefaultItemAnimator());
    }

    private class HttpAsyncTaskgetJobs extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskgetJobs() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            arrJobsList = new ArrayList<>();
            discoverJobsAdapter = new DiscoverJobsAdapter(JobActivity.this, arrJobsList);
            recyclerViewState = rvJobs.getLayoutManager().onSaveInstanceState();
        }

        protected String doInBackground(String... urls) {

            return getJobs(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            rvJobs.setAdapter(discoverJobsAdapter);
            rvJobs.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            pd.dismiss();
            Utilities.showLog(result);
        }
    }

    public String getJobs(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", PrefUtils.getUserID(JobActivity.this));
            params.put("JobId", jobId);
            Log.e("JOBURL", url);
            //  params.put("Id", "100424");
            Log.e("DISCOVERJOBID", params.toString());
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        discoverJobModel = new DiscoverJobModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        discoverJobModel.setId(jsonObject.getDouble("Id"));
                        discoverJobModel.setProfile(jsonObject.getString("Profile"));
                        discoverJobModel.setComapnyName(jsonObject.getString("ComapnyName"));
                        discoverJobModel.setExperinceMin(jsonObject.getInt("ExperinceMin"));
                        discoverJobModel.setExperinceMax(jsonObject.getInt("ExperinceMax"));
                        discoverJobModel.setPrimarySkillsName(jsonObject.getString("PrimarySkillsName"));
                        discoverJobModel.setCityName(jsonObject.getString("CityName"));
                        discoverJobModel.setCreatedOn(jsonObject.getString("CreatedOn"));//Applied
                        discoverJobModel.setApplied(jsonObject.getBoolean("Applied"));//Applied
                        discoverJobModel.setLanguageName(jsonObject.getString("LanguageName"));
                        discoverJobModel.setDescription(jsonObject.getString("Description"));
                        discoverJobModel.setPackageMin(jsonObject.getString("PackageMin"));
                        discoverJobModel.setPackageMax(jsonObject.getString("PackageMax"));//IndustryName
                        Utilities.showLog("salary=" + jsonObject.getInt("PackageMin") + "-" + jsonObject.getInt("PackageMax"));
                        discoverJobModel.setIndustryName(jsonObject.getString("IndustryName"));

                        arrJobsList.add(discoverJobModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    BroadcastReceiver appliedJobApiCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                jobAppliedId = intent.getDoubleExtra("discover_jobApplied_id", 00);
                Utilities.showLog(String.valueOf(jobAppliedId));
                new HttpAsyncTaskJobApplied().execute(CommonUrl.setJobApplied);
            }
        }
    };

    private class HttpAsyncTaskJobApplied extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskJobApplied() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.show();
        }

        protected String doInBackground(String... urls) {
            return setJobApplied(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Utilities.showLog(result);
            if (AppliedStatus) {
                Common.showApplyDialog(JobActivity.this, getString(R.string.apply));
                new HttpAsyncTaskgetJobs().execute(CommonUrl.getDashboardData + "GetJobByJobId");
            }

        }
    }

    public String setJobApplied(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            Double d = new Double(jobAppliedId);
            params.put("JobId", String.valueOf(d.intValue()));
            params.put("Applied", "true");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        AppliedStatus = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        pd.dismiss();
        Intent intent = new Intent(JobActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Activity.JobDescriptionActivity;
import com.example.ajit.sapnasscom.Activity.MainActivity;
import com.example.ajit.sapnasscom.Activity.MyJobsActivity;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Fragment.FeedbackBottomSheetFragment;
import com.example.ajit.sapnasscom.Model.JobModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pratik on 12/7/2017.
 */

public class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.MyViewHolder> {

    private List<JobModel> arrJobs;
    private Context context;
    LocaleClass localeClass;
    public static String job, jobStatus = "";
    SessionManager sessionManager;
    int count = 0;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tvProfile, tv_experience, tvCompanyName, tvSkills, tvExperience, tvDate, tvCreatedby, tvDuration, tvLocation, tvLanguage, tv__cource_location, tv_duration, tvApplyNow, tvJobDescription;
        private ImageView ivSite;
        LinearLayout dot_click;
        public CardView cardView;
        public RelativeLayout JobRelative;

        public MyViewHolder(View view) {
            super(view);
            tv_experience = (TextView) view.findViewById(R.id.tv_experience);
            tvProfile = (TextView) view.findViewById(R.id.tv_department);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvCompanyName = (TextView) view.findViewById(R.id.tv_company_name);
            tvExperience = (TextView) view.findViewById(R.id.tv_experience_number);
            tvLocation = (TextView) view.findViewById(R.id.tv_location_name);
            tvSkills = (TextView) view.findViewById(R.id.tv_skills);
            tvJobDescription = (TextView) view.findViewById(R.id.tv_job_description);
            tvApplyNow = (TextView) view.findViewById(R.id.tv_apply_job);
            tv_duration = (TextView) view.findViewById(R.id.tv_duration);
            tv__cource_location = (TextView) view.findViewById(R.id.tv__cource_location);
            dot_click = (LinearLayout) view.findViewById(R.id.dot_click);
            cardView = (CardView) view.findViewById(R.id.cardView);
            JobRelative = (RelativeLayout) view.findViewById(R.id.JobRelative);
        }
    }

    public JobsAdapter(Context context, List<JobModel> arrJobs) {
        this.context = context;
        this.arrJobs = arrJobs;
        sessionManager = new SessionManager(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_job, parent, false);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(context, "JobsAdapter");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Utilities.showLog(String.valueOf("Size=" + position));
        final JobModel jobModel = arrJobs.get(position);
        holder.tvProfile.setText(jobModel.getProfile());
        holder.tvCompanyName.setText(jobModel.getComapnyName());
        holder.tvSkills.setText(jobModel.getPrimarySkillsName());
        holder.tvLocation.setText(jobModel.getCityName());
        holder.tvExperience.setText(jobModel.getExperinceMin() + "-" + jobModel.getExperinceMax() + " year");
        holder.tvDate.setText(jobModel.getCreatedOn());

        if (jobModel.isApplied()) {
            holder.tvApplyNow.setText(R.string.Applied);
            sessionManager.createApplied("" + R.string.Applied);
        } else {
            holder.tvApplyNow.setText(R.string.Apply_Now);
            sessionManager.createApplied("" + R.string.Apply_Now);
        }

        count = 0;
        holder.tvApplyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if (count == 1) {
                    if (jobModel.isApplied()) {
//                        Common.showApplyDialog(context, context.getString(R.string.applyalready));
                        Intent intent = new Intent(context, MyJobsActivity.class);
                        context.startActivity(intent);
                    } else {
                        Intent localImageintent = new Intent();
                        localImageintent.setAction("hitApplyjobApi");
                        localImageintent.putExtra("jobApplied_id", jobModel.getId());
                        context.sendBroadcast(localImageintent);
                    }
                }
            }
        });

        holder.tvJobDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, JobDescriptionActivity.class);
                intent.putExtra("jobid", jobModel.getId());
                intent.putExtra("company_name", jobModel.getComapnyName());
                intent.putExtra("profile_name", jobModel.getProfile());
                intent.putExtra("location", jobModel.getCityName());
                intent.putExtra("applystatus", jobModel.isApplied());
                intent.putExtra("description", jobModel.getDescription());
                intent.putExtra("experience", jobModel.getExperinceMin() + "-" + jobModel.getExperinceMax() + " year");
                intent.putExtra("salary", jobModel.getPackageMin() + " - " + jobModel.getPackageMax() + " LPA");
                intent.putExtra("industury", jobModel.getIndustryName());
                intent.putExtra("SkillId", jobModel.getSkillId());
                intent.putExtra("myJobClick", "false");
                context.startActivity(intent);
            }
        });

        try {
            if (jobModel.getFeadBack().equalsIgnoreCase("true")) {
                holder.cardView.setBackgroundColor(Color.parseColor("#fdd1c8"));
                holder.JobRelative.setBackgroundColor(Color.parseColor("#fdd1c8"));
            } else {
                holder.cardView.setBackgroundColor(Color.parseColor("#ffffff"));
                holder.JobRelative.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.dot_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jobModel.getFeadBack().equals("false")) {
                    FeedbackBottomSheetFragment bottomSheetDialogFragment = new FeedbackBottomSheetFragment();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("Skill_list", (Serializable) Common.fetch_Skill(context));
                    bundle.putString("name", "From Adapter");
                    bundle.putString("COURSE_FEEDBACK", "JOB_FEEDBACK");
                    sessionManager.CreateFeedback("JOB_FEEDBACK");
                    bottomSheetDialogFragment.setArguments(bundle);
                    bottomSheetDialogFragment.show(((MainActivity) context).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                    job = String.valueOf(jobModel.getId());
                    jobStatus = "JOB";
//                    crossText = "Give feedbackon this job";
                } else {
                    Toast.makeText(context, "You have already submited your feedback", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrJobs.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

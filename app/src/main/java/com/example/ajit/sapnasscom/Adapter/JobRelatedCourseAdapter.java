package com.example.ajit.sapnasscom.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Activity.CourseDescriptionActivity;
import com.example.ajit.sapnasscom.Activity.LetsLearnActivity;
import com.example.ajit.sapnasscom.Activity.MainActivity;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Fragment.FeedbackBottomSheetFragment;
import com.example.ajit.sapnasscom.Model.CourseModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pratik on 12/7/2017.
 */

public class JobRelatedCourseAdapter extends RecyclerView.Adapter<JobRelatedCourseAdapter.MyViewHolder> {

    private List<CourseModel> arrCourse;
    private Context context;
    LocaleClass localeClass;
    public static String job = "", course = "", crossText = "";
    SessionManager sessionManager;
    int count = 0, d;
    Boolean EnrolledValue;
    private String userId;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tv_languages, tv__cource_location, tv_duration, tvCourseName, tvDate, tvCreatedby, tvDuration, tvLocation, tvLanguage, tvEnrollNow, tvCourseDescription;
        private ImageView ivSite;
        public static LinearLayout dot_click;
        public BottomSheetBehavior mBottomSheetBehavior1;
        public static RelativeLayout cardView;
        public static CardView courseCard;

        public MyViewHolder(View view) {
            super(view);
            tvCourseName = (TextView) view.findViewById(R.id.tv_department);
            tv_duration = (TextView) view.findViewById(R.id.tv_duration);
            tv__cource_location = (TextView) view.findViewById(R.id.tv__cource_location);
            tv_languages = (TextView) view.findViewById(R.id.tv_languages);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvCreatedby = (TextView) view.findViewById(R.id.tv_company_name);
            tvDuration = (TextView) view.findViewById(R.id.tv_experience_number);
            tvLocation = (TextView) view.findViewById(R.id.tv_location_name);
            tvLanguage = (TextView) view.findViewById(R.id.tv_laguages_name);
            tvCourseDescription = (TextView) view.findViewById(R.id.tv_course_detail);
            tvEnrollNow = (TextView) view.findViewById(R.id.tv_enroll_now);
            dot_click = (LinearLayout) view.findViewById(R.id.course_dot_click);
            cardView = (RelativeLayout) view.findViewById(R.id.courseCardVire);
            courseCard = (CardView) view.findViewById(R.id.courseCard);
        }
    }

    public JobRelatedCourseAdapter(Context context, List<CourseModel> arrCourse) {
        this.context = context;
        this.arrCourse = arrCourse;
        sessionManager = new SessionManager(context);
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_job_related_cource, parent, false);
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(context, "JobRelatedCourseAdapter");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Utilities.showLog(String.valueOf("Size=" + position));
        final CourseModel courseModel = arrCourse.get(position);
        holder.tvCourseName.setText(courseModel.getCourseName());
        holder.tvCreatedby.setText("Created By: " + courseModel.getCreator());
        holder.tvDuration.setText(String.valueOf(courseModel.getDuration()) + " Weeks");
        holder.tvLocation.setText(courseModel.getCityName());
        holder.tvLanguage.setText(courseModel.getLanguageName());
        holder.tvDate.setText(courseModel.getCreatedOn());

        sessionManager.createEnroll("" + courseModel.isEnrolled());
        sessionManager.createRelatedCourseStatus("" + courseModel.getLearnStatus());

        if (courseModel.isEnrolled()) {
            if (courseModel.getLearnStatus().equalsIgnoreCase("Let learn")) {
                holder.tvEnrollNow.setText(R.string.start_course);
            } else if (courseModel.getLearnStatus().equalsIgnoreCase("Finish")) {
                holder.tvEnrollNow.setText(R.string.finishh);
            } else {
                holder.tvEnrollNow.setText(R.string.continue_course);
            }
            // holder.tvEnrollNow.setText(R.string.Enrolled);
        } else {
            holder.tvEnrollNow.setText(R.string.Enroll_Now);
        }

        count = 0;
        holder.tvEnrollNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if (count == 1) {
                    if (courseModel.isEnrolled()) {
                        Utilities.showLog("description=" + courseModel.getDescription());
//                        Common.showApplyDialog(context, context.getString(R.string.enrolledAlready));
                        d = Integer.parseInt(String.valueOf(courseModel.getId()).replace(".0", ""));
                        Intent intent = new Intent(context, LetsLearnActivity.class);
                        intent.putExtra("courseid", String.valueOf(d));
                        intent.putExtra("LearnStatus", courseModel.getLearnStatus());
                        intent.putExtra("enrolledValue", courseModel.isEnrolled());
                        intent.putExtra("courseName", courseModel.getCourseName());
                        intent.putExtra("SkillId", courseModel.getSkillId());

                        EnrolledValue = courseModel.isEnrolled();

                        if (Common.isInternet(context)) {
                            if (courseModel.getLearnStatus().equalsIgnoreCase("Let Learn")) {
                                new HttpAsyncTaskCourse().execute(CommonUrl.myCourseClick);
                            }
                        } else {
                            Toast.makeText(context, "Please Connect Internet", Toast.LENGTH_SHORT).show();
                        }

                        context.startActivity(intent);
                    } else {
                        Intent localImageintent = new Intent();
                        localImageintent.setAction("hitEnrolledApi");
                        localImageintent.putExtra("course_enrolled_id", courseModel.getId());
                        context.sendBroadcast(localImageintent);
                    }
                }
            }
        });

        holder.tvCourseDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CourseDescriptionActivity.class);
                intent.putExtra("courseid", courseModel.getId());
                intent.putExtra("title", courseModel.getCourseName());
                intent.putExtra("duration", String.valueOf(courseModel.getDuration()) + " Weeks");
                intent.putExtra("location", courseModel.getCityName());
                intent.putExtra("language", courseModel.getLanguageName());
                intent.putExtra("enrolledValue", courseModel.isEnrolled());
                intent.putExtra("keyfeatures", courseModel.getKeyFeature());
                intent.putExtra("description", courseModel.getDescription());
                intent.putExtra("LearnStatus", courseModel.getLearnStatus());
                intent.putExtra("courseName", courseModel.getCourseName());
                intent.putExtra("SkillId", courseModel.getSkillId());
                context.startActivity(intent);
            }
        });

        holder.dot_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Coures", "" + courseModel.getFeadBack());
                try {
                    if (courseModel.getFeadBack().equalsIgnoreCase("false")) {
                        FeedbackBottomSheetFragment bottomSheetDialogFragment = new FeedbackBottomSheetFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("Skill_list", (Serializable) Common.fetch_Skill(context));
                        bundle.putString("COURSE_FEEDBACK", "COURSE_FEEDBACK");
                        bottomSheetDialogFragment.setArguments(bundle);
                        bottomSheetDialogFragment.show(((MainActivity) context).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        job = String.valueOf(courseModel.getId());
                        course = "COURSE";

                    } else {
                        Toast.makeText(context, "You have already submited your feedback", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            if (courseModel.getFeadBack().equalsIgnoreCase("true")) {
                holder.cardView.setBackgroundColor(Color.parseColor("#FFE6E1"));
                holder.courseCard.setBackgroundColor(Color.parseColor("#FFE6E1"));
            } else {
                holder.cardView.setBackgroundColor(Color.parseColor("#ffffff"));
                holder.courseCard.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return arrCourse.size();
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private class HttpAsyncTaskCourse extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourse() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(context);
        }

        protected String doInBackground(String... urls) {
            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MyProgressDialog.hideDialog();
        }
    }

    public String getCourse(String url) {
        String result = "";
        System.out.print("MYCOURSEACTIVITY_URL : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", userId);
            params.put("CourseID", d);
            if (EnrolledValue == true) {
                params.put("Enrolled", 1);
            } else {
                params.put("Enrolled", 0);
            }

            params.put("LearnStatus", "Progress");

            Log.e("MYCOURSEACTIVITY", params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}

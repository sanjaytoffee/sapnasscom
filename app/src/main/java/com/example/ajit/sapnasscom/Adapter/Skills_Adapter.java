package com.example.ajit.sapnasscom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Model.Education;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

/**
 * Created by DTP-110 on 1/27/2017.
 */

public class Skills_Adapter extends BaseAdapter
{

    Activity mActivity;
    ArrayList<Skills> skillsArrayList;

    public Skills_Adapter(Activity a, ArrayList<Skills> data) {
        this.mActivity = a;
        this.skillsArrayList = data;
    }

    @Override
    public int getCount() {
        return skillsArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return skillsArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.skill_text, null);

            viewHolder.skill_text = (TextView) convertView
                    .findViewById(R.id.skill_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (skillsArrayList != null) {
            try {
                viewHolder.skill_text.setText("" + skillsArrayList.get(position).getName());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView skill_text;
    }

}



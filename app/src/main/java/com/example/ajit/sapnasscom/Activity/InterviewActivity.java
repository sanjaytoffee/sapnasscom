package com.example.ajit.sapnasscom.Activity;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import com.example.ajit.sapnasscom.Adapter.Hr_Adapter;
import com.example.ajit.sapnasscom.Adapter.InterviewAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.ExpendableListViews;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Model.HrQuestion;
import com.example.ajit.sapnasscom.Model.InterviewModel;
import com.example.ajit.sapnasscom.R;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by HSTPL on 2/19/2018.
 */

public class InterviewActivity extends AppCompatActivity {

    Toolbar toolbar;
    Spinner hr_question_spinner;
    ExpendableListViews listview;
    ArrayList<InterviewModel> questionList;
    InterviewAdapter interviewAdapter;
    InterviewModel interviewModel;
    HrQuestion hrQuestion;
    Hr_Adapter hr_adapter;
    ArrayList<HrQuestion> QuestionTypeList;
    String question_id;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.interview_question_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.Interview_Questions));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        hr_question_spinner = (Spinner) findViewById(R.id.question_spinner);
        listview = (ExpendableListViews) findViewById(R.id.interview_list);

        new HttpAsyncTask_HrQuestionType().execute(CommonUrl.drop_down + "GetDropdownList");

        hr_question_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                question_id = QuestionTypeList.get(i).getID();
                new HttpAsyncTask_Interview().execute(CommonUrl.drop_down + "GetDropdownList");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            hr_question_spinner.setPopupBackgroundResource(R.drawable.border_search_text);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    class HttpAsyncTask_HrQuestionType extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(InterviewActivity.this);

        }

        protected String doInBackground(String... urls) {
            return fetchHrQuestion(urls[0]);
        }

        protected void onPostExecute(String result) {
            MyProgressDialog.hideDialog();
            if (result != null) {
                hr_adapter = new Hr_Adapter(InterviewActivity.this, QuestionTypeList);
                hr_question_spinner.setAdapter(hr_adapter);
            }
        }
    }

    public String fetchHrQuestion(String url) {
        String result = "";

        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "CATEGORY");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                QuestionTypeList = new ArrayList<>();
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        hrQuestion = new HrQuestion();

                        hrQuestion.setID(catObj.getString("ID"));
                        hrQuestion.setCode(catObj.getString("Code"));
                        hrQuestion.setName(catObj.getString("Name"));

                        QuestionTypeList.add(hrQuestion);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    class HttpAsyncTask_Interview extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        protected String doInBackground(String... urls) {
            return fetchInterView(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                interviewAdapter = new InterviewAdapter(getApplicationContext(), R.layout.row_interview_question, questionList);
                listview.setAdapter(interviewAdapter);
            }
        }
    }

    public String fetchInterView(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "INTERVIEW");
            params.put("KeyOne", question_id);
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                questionList = new ArrayList<>();
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        interviewModel = new InterviewModel();

                        interviewModel.setInterviewQues(catObj.getString("Name"));
                        interviewModel.setInterviewAns(catObj.getString("Code"));

                        questionList.add(interviewModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
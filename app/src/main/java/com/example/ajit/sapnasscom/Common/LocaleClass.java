package com.example.ajit.sapnasscom.Common;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Activity.CourseDescriptionActivity;
import com.example.ajit.sapnasscom.Activity.DetailsActivity;
import com.example.ajit.sapnasscom.Activity.EditProfileShowDataActivity;
import com.example.ajit.sapnasscom.Activity.ForgotPasswordActivity;
import com.example.ajit.sapnasscom.Activity.JobDescriptionActivity;
import com.example.ajit.sapnasscom.Activity.JobDetailsActivity;
import com.example.ajit.sapnasscom.Activity.LetsLearnActivity;
import com.example.ajit.sapnasscom.Activity.LoginActivity;
import com.example.ajit.sapnasscom.Activity.Main;
import com.example.ajit.sapnasscom.Activity.MainActivity;
import com.example.ajit.sapnasscom.Activity.MyJobsActivity;
import com.example.ajit.sapnasscom.Activity.ProfileDetailsActivity;
import com.example.ajit.sapnasscom.Activity.SignUpDescoverActivity;
import com.example.ajit.sapnasscom.Activity.SignupActivity;
import com.example.ajit.sapnasscom.Adapter.CourseContentAdapter;
import com.example.ajit.sapnasscom.Adapter.CourseRelatedJobsAdapter;
import com.example.ajit.sapnasscom.Adapter.DiscoverAdapter;
import com.example.ajit.sapnasscom.Adapter.DiscoverCourseAdapter;
import com.example.ajit.sapnasscom.Adapter.DiscoverJobsAdapter;
import com.example.ajit.sapnasscom.Adapter.JobContentAdapter;
import com.example.ajit.sapnasscom.Adapter.JobRelatedCourseAdapter;
import com.example.ajit.sapnasscom.Adapter.LetsLearnReledJobsAdapter;
import com.example.ajit.sapnasscom.Adapter.MyCourseAdapter;
import com.example.ajit.sapnasscom.Adapter.MyJobsAdapter;
import com.example.ajit.sapnasscom.Adapter.JobsAdapter;
import com.example.ajit.sapnasscom.Adapter.CourseAdapter;
import com.example.ajit.sapnasscom.Adapter.PdfAdapter;
import com.example.ajit.sapnasscom.Fragment.CourseDeatailFragment;
import com.example.ajit.sapnasscom.Fragment.CourseFragment;
import com.example.ajit.sapnasscom.Fragment.DiscoverFragment;
import com.example.ajit.sapnasscom.Fragment.FeedbackBottomSheetFragment;
import com.example.ajit.sapnasscom.Fragment.JobDeatailFragment;
import com.example.ajit.sapnasscom.Fragment.JobsFragment;
import com.example.ajit.sapnasscom.R;

import java.util.HashMap;
import java.util.Locale;

public class LocaleClass {
    private Locale myLocale;
    public Activity activity;
    public String page, enrollStatus, learnStatus;
    SessionManager sessionManager;
    public static int Discover = R.string.Discover, dashboard;

    public void loadLocale(Context context, String page) {
        this.page = page;
        String langPref = "Language";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        String language = prefs.getString(langPref, "");
        sessionManager = new SessionManager(context);
        changeLang(language, context);
    }

    public void saveLocale(String lang, Context context) {
        String langPref = "Language";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }

    public void changeLang(String lang, Context context) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);
        saveLocale(lang, context);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        updateTexts();
    }

    private void updateTexts() {
        if (page.equalsIgnoreCase("Sign up")) {
            SignupActivity.textViewName.setText(R.string.name_);
            SignupActivity.textViewGender.setText(R.string.Gender_);
            SignupActivity.textViewEmail.setText(R.string.Email);
            SignupActivity.textViewPassword.setText(R.string.Password);
            SignupActivity.textViewMobileNumver.setText(R.string.Mobile_Number_);
            SignupActivity.radioFemale.setText(R.string.female);
            SignupActivity.radioMale.setText(R.string.male);
            SignupActivity.buttonSignup.setText(R.string.submit);
            SignupActivity.tvLanguage.setText(R.string.sign_language);
        } else if (page.equalsIgnoreCase("Login")) {
            LoginActivity.textViewEmailLogin.setText(R.string.email_login);
            LoginActivity.textViewEmailAddress.setText(R.string.e_mail_address);
            LoginActivity.textViewPasswordLabel.setText(R.string.Password);
            LoginActivity.loginButton.setText(R.string.submit);
            LoginActivity.forgot_passward.setText(R.string.ForgotPassword);
        } else if (page.equalsIgnoreCase("Profile Show")) {
            EditProfileShowDataActivity.textViewDastBoardText.setText(R.string.Dashboard);
            EditProfileShowDataActivity.textViewEducationText.setText(R.string.Education1);
            EditProfileShowDataActivity.textViewLanguageText.setText(R.string.Languages);
            EditProfileShowDataActivity.textViewSkillText.setText(R.string.skillsvalue);
            EditProfileShowDataActivity.textViewCurrentLocationText.setText(R.string.profile_current_location);
            EditProfileShowDataActivity.textViewPreferredLocationText.setText(R.string.profile_preferred_location);
            EditProfileShowDataActivity.textViewCourseEnrolledText.setText(R.string.courses_enrolled);
            EditProfileShowDataActivity.textViewJobsAppliedText.setText(R.string.Jobs_Applied);
            EditProfileShowDataActivity.textViewEditText.setText(R.string.Edit);
        } else if (page.equalsIgnoreCase("Profile Details")) {
            ProfileDetailsActivity.save.setText(R.string.Save);
            ProfileDetailsActivity.textViewEduation.setText(R.string.Education);
            ProfileDetailsActivity.textViewLanguage.setText(R.string.Language1);
            ProfileDetailsActivity.textViewSkill.setText(R.string.Skills);
            ProfileDetailsActivity.textViewCurrentLocation.setText(R.string.Current_Location);
            ProfileDetailsActivity.textViewPreferredLocation.setText(R.string.Preferred_Location);
            ProfileDetailsActivity.textViewExprence.setText(R.string.Experience);
            ProfileDetailsActivity.textViewResumeLabel.setText(R.string.upload_resume);
        } else if (page.equalsIgnoreCase("SignUpDescoverActivity")) {
            SignUpDescoverActivity.tvDescover.setText(R.string.Discover);
            SignUpDescoverActivity.tvSignin.setText(R.string.Sign_In);
        } else if (page.equalsIgnoreCase("Main")) {
            try {
                Main.textViewSignup.setText(R.string.in);
                Main.buttonLoginEmail.setText(R.string.Login_with_Email);
                Main.buttonConnectGoogle.setText(R.string.ConnectwithG_);
                Main.textViewByUsing.setText(R.string.by_usiing);
                Main.tvterms.setText(R.string.Terms_of_use);
                Main.tvprivacy.setText(R.string.Privacy_Policy);
                Main.textViewNotRegister.setText(R.string.Not_Registered);
                Main.buttonSignup.setText(R.string.Sign_Up);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (page.equalsIgnoreCase("MainActivity")) {
            try {
                MainActivity.menu.findItem(R.id.nav_dashboard).setTitle(R.string.Dashboard);
                MainActivity.menu.findItem(R.id.nav_mycourse).setTitle(R.string.My_Courses);
                MainActivity.menu.findItem(R.id.nav_myjob).setTitle(R.string.my_jobs);
                MainActivity.menu.findItem(R.id.nav_interview).setTitle(R.string.Interview_Questions);
                MainActivity.menu.findItem(R.id.nav_support).setTitle(R.string.Support);
                MainActivity.menu.findItem(R.id.nav_rate).setTitle(R.string.Rate_this_App);
                MainActivity.menu.findItem(R.id.refer_friend).setTitle(R.string.Refer_to_a_Friend);
                MainActivity.menu.findItem(R.id.nav_signout).setTitle(R.string.Sign_Out);
                MainActivity.menu.findItem(R.id.nav_exit).setTitle(R.string.Exit);
                MainActivity.textExitMessage.setText(R.string.exit_message);
                MainActivity.tvDeativate.setText(R.string.deactivate);
                MainActivity.tvLogout.setText(R.string.logout_string);
                Discover = R.string.Discover;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else if (page.equalsIgnoreCase("CourseFragment")) {
            CourseFragment.title.setText(R.string.recommend_texts);
            CourseFragment.title_text.setText(R.string.recommend_text);
        }else if (page.equalsIgnoreCase("JobsFragment")) {
            JobsFragment.title.setText(R.string.recommend_texts);
            JobsFragment.title_text.setText(R.string.recommend_text);
        }
            else if (page.equalsIgnoreCase("CourseAdapter")) {
            CourseAdapter.MyViewHolder.tv_duration.setText(R.string.Duration);
            CourseAdapter.MyViewHolder.tv__cource_location.setText(R.string.Location);
            CourseAdapter.MyViewHolder.tv_languages.setText(R.string.Languages);
            CourseAdapter.MyViewHolder.tv__enrolled_text.setText(R.string.Enrolled);
            CourseAdapter.MyViewHolder.tvCourseDescription.setText(R.string.Course_Details);
            CourseAdapter.MyViewHolder.tvCourseDescription.setText(R.string.Course_Details);
           /* CourseAdapter.RecyclerView_HeaderFooter_Holder.title.setText(R.string.recommend_texts);
            CourseAdapter.RecyclerView_HeaderFooter_Holder.title_text.setText(R.string.recommend_text);*/

            HashMap<String, String> enroll = sessionManager.getEnrollStatus();
            enrollStatus = enroll.get(SessionManager.KEY_ENROLL_STATUS);

            HashMap<String, String> learn = sessionManager.getlLearnStatus();
            learnStatus = learn.get(SessionManager.KEY_LEARN_STATUS);

            if (enrollStatus.equalsIgnoreCase("true")) {
                if (learnStatus.equalsIgnoreCase("Let learn")) {
                    CourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.start_course);
                } else if (learnStatus.equalsIgnoreCase("Finish")) {
                    CourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.finishh);
                } else {
                    CourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.continue_course);
                }
                //CourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.Enrolled);
            } else {
                CourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.Enroll_Now);
            }

        } else if (page.equalsIgnoreCase("LetsLearnReledJobsAdapter")) {
            HashMap<String, String> enroll = sessionManager.getJobApplied();
            String jobApplied = enroll.get(SessionManager.KEY_JOB_APPLIED);
            try {
                if (jobApplied.equalsIgnoreCase("true")) {
                    LetsLearnReledJobsAdapter.MyViewHolder.tvApplyNow.setText(R.string.Applied);
                } else {
                    LetsLearnReledJobsAdapter.MyViewHolder.tvApplyNow.setText(R.string.Apply_Now);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (page.equalsIgnoreCase("JobsAdapter")) {
            JobsAdapter.MyViewHolder.tv_experience.setText(R.string.experience);
            JobsAdapter.MyViewHolder.tv_duration.setText(R.string.Experience);
            JobsAdapter.MyViewHolder.tv__cource_location.setText(R.string.Location);
            JobsAdapter.MyViewHolder.tvJobDescription.setText(R.string.Job_Description);
           /* JobsAdapter.RecyclerView_HeaderFooter_Holder.title.setText(R.string.recommend_texts);
            JobsAdapter.RecyclerView_HeaderFooter_Holder.title_text.setText(R.string.recommend_text);*/
            HashMap<String, String> getmail = sessionManager.getApplied();
            String appliedValue = getmail.get(SessionManager.KEY_APPLIED);
            Toast.makeText(activity, "" + appliedValue, Toast.LENGTH_SHORT).show();

            if (enrollStatus.equalsIgnoreCase("true")) {
                JobsAdapter.MyViewHolder.tvApplyNow.setText(R.string.Applied);
            } else {
                JobsAdapter.MyViewHolder.tvApplyNow.setText(R.string.Apply_Now);
            }
        } else if (page.equalsIgnoreCase("DiscoverFragment")) {
            DiscoverFragment.tvbestCourse.setText(R.string.Find_the_best_fit_for_you);
            DiscoverFragment.radioCourse.setText(R.string.Course);
            DiscoverFragment.radioJob.setText(R.string.Job);
            DiscoverFragment.tvRecommendation.setText(R.string.recommendations);
        } else if (page.equalsIgnoreCase("CourseDescriptionActivity")) {
            CourseDescriptionActivity.tv_duration.setText(R.string.Duration);
            CourseDescriptionActivity.tv__cource_location.setText(R.string.Location);
            CourseDescriptionActivity.tv_languages.setText(R.string.Languages);
        } else if (page.equalsIgnoreCase("FeedbackBottomSheetFragment")) {

            HashMap<String, String> enroll = sessionManager.getFeedback();
            String feedback = enroll.get(SessionManager.KEY_GET_FEEDBACK);

            if (feedback.equalsIgnoreCase("COURSE_FEEDBACK")) {
                FeedbackBottomSheetFragment.cross_text.setText(R.string.give_feedback_on_this_course);
            } else {
                FeedbackBottomSheetFragment.cross_text.setText(R.string.give_feedback_on_this_job);
            }

            FeedbackBottomSheetFragment.headinh.setText(R.string.write_something);
            FeedbackBottomSheetFragment.comment_save.setText(R.string.Save);
            FeedbackBottomSheetFragment.feed.setText(R.string.we_use);
        } else if (page.equalsIgnoreCase("ForgotPasswordActivity")) {
            ForgotPasswordActivity.email_text.setText(R.string.Email1);
            ForgotPasswordActivity.btnForgot.setText(R.string.Reset_Password);
        } else if (page.equalsIgnoreCase("LetsLearnActivity")) {
            LetsLearnActivity.tvStartLearning.setText(R.string.start_learning);
            LetsLearnActivity.tvReltedVideo.setText(R.string.watch_video);
            LetsLearnActivity.tvLetsLearn.setText(R.string.lets_learn);
            LetsLearnActivity.tvProgress.setText(R.string.progress);
            LetsLearnActivity.tvFinish.setText(R.string.finishh);
            LetsLearnActivity.tvRelatedJobs.setText(R.string.related_jobs);
            LetsLearnActivity.textMessage.setText(R.string.finish_message);
        } else if (page.equalsIgnoreCase("DiscoverJobsAdapter")) {
            DiscoverJobsAdapter.MyViewHolder.tvApplyNow.setText(R.string.discover_job_apply_now);
            DiscoverJobsAdapter.MyViewHolder.tvApplyNow.setText(R.string.discover_job_apply_now);
            DiscoverJobsAdapter.MyViewHolder.tvLocation.setText(R.string.Location);
            DiscoverJobsAdapter.MyViewHolder.tvJobDuration.setText(R.string.experience);
        } else if (page.equalsIgnoreCase("DiscoverCourseAdapter")) {
            DiscoverCourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.Enroll_Now);
            DiscoverCourseAdapter.MyViewHolder.tvCourseDescription.setText(R.string.Course_Details);
        } else if (page.equalsIgnoreCase("DetailsActivity")) {
            if (DetailsActivity.enrolledValue) {
                DetailsActivity.tvEnrolledButton.setText(R.string.Enrolled);
            } else {
                DetailsActivity.tvEnrolledButton.setText(R.string.Enroll_Now);
            }

            DetailsActivity.tv_features_header.setText(R.string.Course_Key_Features);
            DetailsActivity.tv_course_features_header.setText(R.string.Course_Details);
            DetailsActivity.tv_description_duration.setText(R.string.Duration);
            DetailsActivity.tv__cource_location.setText(R.string.Location);
            DetailsActivity.tv_languages.setText(R.string.Languages);
        } else if (page.equalsIgnoreCase("DiscoverAdapter")) {
            DiscoverAdapter.MyViewHolder.tv_discover_primaryskill.setText(R.string.discover_primary_skills);
            DiscoverAdapter.MyViewHolder.tv_discover_secondary.setText(R.string.discover_secondary_skills);
        } else if (page.equalsIgnoreCase("CourseRelatedJobsAdapter")) {
            CourseRelatedJobsAdapter.MyViewHolder.tvJobDescription.setText(R.string.Job_Description);
            CourseRelatedJobsAdapter.MyViewHolder.tv_duration.setText(R.string.experience);
            HashMap<String, String> applied = sessionManager.getCourseJobApplied();
            String applieds = applied.get(SessionManager.KEY_COURSE_JOB_APPLIED);
            if (applieds.equalsIgnoreCase("true")) {
                CourseRelatedJobsAdapter.MyViewHolder.tvApplyNow.setText(R.string.Applied);
            } else {
                CourseRelatedJobsAdapter.MyViewHolder.tvApplyNow.setText(R.string.Apply_Now);
            }
        } else if (page.equalsIgnoreCase("CourseDeatailFragment")) {
            CourseDeatailFragment.tv_features_header.setText(R.string.Course_Key_Features);
            CourseDeatailFragment.tv_course_detail_header.setText(R.string.Course_Details);

            HashMap<String, String> enroll = sessionManager.getDescriptionStatus();
            String status = enroll.get(SessionManager.KEY_DESCRIPTION_STATUS);

            if (CourseDeatailFragment.enrolledValue) {
                if (status.equalsIgnoreCase("Let learn")) {
                    CourseDeatailFragment.tvEnrolledButtonn.setText(R.string.start_course);
                } else if (status.equalsIgnoreCase("Finish")) {
                    CourseDeatailFragment.tvEnrolledButtonn.setText(R.string.finishh);
                } else {
                    CourseDeatailFragment.tvEnrolledButtonn.setText(R.string.continue_course);
                }
                // CourseDeatailFragment.tvEnrolledButtonn.setText(R.string.Enrolled);
            } else {
                CourseDeatailFragment.tvEnrolledButtonn.setText(R.string.Enroll_Now);
            }

        } else if (page.equalsIgnoreCase("JobDeatailFragment")) {
            JobDeatailFragment.tvHeaderTitelDescription.setText(R.string.Job_Details);
            JobDeatailFragment.tv_salarys_header.setText(R.string.salary);
            JobDeatailFragment.tv_industury_header.setText(R.string.industry);
            if (JobDeatailFragment.ApplyVAlue) {
                JobDeatailFragment.tvApplyButton.setText(R.string.Applied);
            } else {
                JobDeatailFragment.tvApplyButton.setText(R.string.Apply_Now);
            }

        } else if (page.equalsIgnoreCase("JobDescriptionActivity")) {
            JobDescriptionActivity.tv_duration.setText(R.string.experience);
            JobDescriptionActivity.tv__cource_location.setText(R.string.Location);
            JobDescriptionActivity.tv_myjob_status.setText(R.string.status);
        } else if (page.equalsIgnoreCase("JobRelatedCourseAdapter")) {
            JobRelatedCourseAdapter.MyViewHolder.tv_duration.setText(R.string.Duration);
            JobRelatedCourseAdapter.MyViewHolder.tv__cource_location.setText(R.string.Location);
            JobRelatedCourseAdapter.MyViewHolder.tv_languages.setText(R.string.Languages);

            HashMap<String, String> enroll = sessionManager.getRelatedCourseStatus();
            String status = enroll.get(SessionManager.KEY_JOB_LEARN_STATUS);

            if (enrollStatus.equalsIgnoreCase("true")) {
                if (status.equalsIgnoreCase("Let learn")) {
                    JobRelatedCourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.start_course);
                } else if (status.equalsIgnoreCase("Finish")) {
                    JobRelatedCourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.finishh);
                } else {
                    JobRelatedCourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.continue_course);
                }
                // JobRelatedCourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.Enrolled);
            } else {
                JobRelatedCourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.Enroll_Now);
            }
        } else if (page.equalsIgnoreCase("MyJobsAdapter")) {
            MyJobsAdapter.MyViewHolder.tv_myjobs_status.setText(R.string.status);
            MyJobsAdapter.MyViewHolder.tvJobDescription.setText(R.string.Job_Description);
            MyJobsAdapter.MyViewHolder.tv__cource_location.setText(R.string.Location);
            MyJobsAdapter.MyViewHolder.tv_experience.setText(R.string.experience);
            MyJobsAdapter.MyViewHolder.tvApplyNow.setText(R.string.withdraw_application);
        } else if (page.equalsIgnoreCase("CourseContentAdapter")) {
            CourseContentAdapter.MyViewHolder.tv_discover_without_login.setText(R.string.Course_Details);
        } else if (page.equalsIgnoreCase("PdfAdapter")) {
            HashMap<String, String> applied = sessionManager.getActive();
            String active = applied.get(SessionManager.KEY_ACTIVE);

            HashMap<String, String> download = sessionManager.getDownload();
            String downloadString = download.get(SessionManager.KEY_DOWNLOAD);

            /*if (active.equalsIgnoreCase("true")) {
                if (downloadString.equalsIgnoreCase("Download")) {
                    PdfAdapter.movieVH.tvRead.setText(R.string.download);
                } else {
                    PdfAdapter.movieVH.tvRead.setText(R.string.read);
                }
            } else {
                PdfAdapter.movieVH.tvRead.setText(R.string.view);
            }*/
        } else if (page.equalsIgnoreCase("JobContentAdapter")) {
            JobContentAdapter.MyViewHolder.tv_job_discover_without_login.setText(R.string.Job_Details);
        } else if (page.equalsIgnoreCase("JobDetailsActivity")) {
            JobDetailsActivity.tv_job_duration.setText(R.string.experience);
            JobDetailsActivity.tv_job_cource_location.setText(R.string.Location);
            JobDetailsActivity.tv_jobcourse_detail_header.setText(R.string.Job_Details);
            JobDetailsActivity.tv_jobsalarys_header.setText(R.string.salary);
            JobDetailsActivity.tv_job_industury_header.setText(R.string.Industry);
            JobDetailsActivity.tvApplyButton.setText(R.string.Apply_Now);
        } else if (page.equalsIgnoreCase("MyJobsActivity")) {
            MyJobsActivity.tvWithdraw.setText(R.string.withdraw_message);
        } else if (page.equalsIgnoreCase("MyCourseAdapter")) {

            HashMap<String, String> status = sessionManager.getCourseStatus();
            String courseStatus = status.get(SessionManager.KEY_COURSE_STATUS);
            try {
                MyCourseAdapter.MyViewHolder.tv_duration.setText(R.string.experience);
                MyCourseAdapter.MyViewHolder.tv__cource_location.setText(R.string.Location);
                MyCourseAdapter.MyViewHolder.tv_languages.setText(R.string.Languages);
                MyCourseAdapter.MyViewHolder.tv__enrolled_text.setText(R.string.Enrolled);
                MyCourseAdapter.MyViewHolder.tvCourseDescription.setText(R.string.Course_Details);

                if (courseStatus.equalsIgnoreCase("Let learn")) {
                    MyCourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.start_course);
                } else if (courseStatus.equalsIgnoreCase("Finish")) {
                    MyCourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.finishh);
                } else {
                    MyCourseAdapter.MyViewHolder.tvEnrollNow.setText(R.string.continue_course);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}

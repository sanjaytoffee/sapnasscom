package com.example.ajit.sapnasscom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Model.CurrentLocation;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;


public class Location_Adapter extends BaseAdapter {

    Activity mActivity;
    ArrayList<CurrentLocation> locationArrayList;

    public Location_Adapter(Activity a, ArrayList<CurrentLocation> data) {
        this.mActivity = a;
        this.locationArrayList = data;
    }


    @Override
    public int getCount() {
        return locationArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return locationArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.current_location_text, null);

            viewHolder.location_text = (TextView) convertView
                    .findViewById(R.id.current_locatio_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (locationArrayList != null) {
            try {
                viewHolder.location_text.setText("" + locationArrayList.get(position).getName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView location_text;
    }

}



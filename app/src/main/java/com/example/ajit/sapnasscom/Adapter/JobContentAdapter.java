package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.Main;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Model.JobContentModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Utilities;

import java.util.List;

/**
 * Created by pratik on 12/7/2017.
 */

public class JobContentAdapter extends RecyclerView.Adapter<JobContentAdapter.MyViewHolder> {

    private List<JobContentModel> jobContentModelList;
    private Context context;
    LocaleClass localeClass;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tv_title, tv_skill,tv_content,tv_author,tv_job_discover_without_login;

        public MyViewHolder(View view) {
            super(view);
            tv_title = (TextView) view.findViewById(R.id.tv_title);
            tv_skill = (TextView) view.findViewById(R.id.tv_skill);
            tv_content = (TextView) view.findViewById(R.id.tv_content);
            tv_author = (TextView) view.findViewById(R.id.tv_author);
            tv_job_discover_without_login = (TextView) view.findViewById(R.id.tv_job_discover_without_login);
        }
    }

    public JobContentAdapter(Context context, List<JobContentModel> jobContentModelList) {
        this.context=context;
        this.jobContentModelList = jobContentModelList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_job_content, parent, false);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(context, "JobContentAdapter");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Utilities.showLog(String.valueOf("Size="+position));

        final JobContentModel courseModel = jobContentModelList.get(position);

        holder.tv_title.setText(courseModel.getTitle());
        holder.tv_skill.setText(courseModel.getPrimaryskills());
        holder.tv_content.setText((courseModel.getContent()));
        holder.tv_author.setText(courseModel.getAuthor());

        holder.tv_job_discover_without_login.setText(R.string.Job_Details);

        holder.tv_job_discover_without_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, Main.class);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return jobContentModelList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

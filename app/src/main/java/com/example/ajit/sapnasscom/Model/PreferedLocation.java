package com.example.ajit.sapnasscom.Model;

/**
 * Created by HSTPL on 2/16/2018.
 */

public class PreferedLocation {
    public String Id;
    public String Name;
    public String Code;

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }
}

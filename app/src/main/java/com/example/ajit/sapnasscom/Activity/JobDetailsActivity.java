package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Fragment.JobDeatailFragment;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by HSTPL on 3/7/2018.
 */

public class JobDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextView tvTitle, tvDuration, tvLocation, tvLanguage, tvKeyfeatures, tvCompanyName, tvDescription, tvSalary;
    String userId, profile, jobId, title, duration, location, language, keyFeatures, description, salary = "", industry = "";
    Intent intent;
    Boolean applyStatus, ApplyVAlue;
    SessionManager sessionManager;
    LocaleClass localeClass;
    public static TextView tvApplyButton, tv_job_duration, tv_job_cource_location, tv_jobcourse_detail_header, tv_jobsalarys_header, tv_job_industury_header, tv_jobapply_now;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_jobdetail_description);

        sessionManager = new SessionManager(JobDetailsActivity.this);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(JobDetailsActivity.this, "JobDetailsActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);

        intent = getIntent();
        if (intent != null) {
            jobId = intent.getStringExtra("JOBBID");
            title = intent.getStringExtra("JOBTITTLE");
            profile = intent.getStringExtra("JobPROFILE");
            duration = intent.getStringExtra("JOBDURATION");
            location = intent.getStringExtra("JOBLOCATION");
            ApplyVAlue = intent.getBooleanExtra("APPLYSTATUS", false);
            description = intent.getStringExtra("JOBDESCRIPTION");
            salary = intent.getStringExtra("salary");//salary/industury
            industry = intent.getStringExtra("industury");//salary/industury

            init();
            Utilities.showLog("JOBDESCRIPTION" + " " + jobId + " " + ApplyVAlue);

            setData();
        }
    }

    private void setData() {
        tvTitle.setText(profile);
        tvDuration.setText(duration);
        tvLocation.setText(location);
        //tvLanguage.setText(language);
        tvCompanyName.setText(industry);
        tvSalary.setText(salary);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            tvDescription.setText(Html.fromHtml(description));
        } else {
            tvDescription.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) findViewById(R.id.tv_job_title);
        tvDuration = (TextView) findViewById(R.id.tv_job_experience_number);
        tvLocation = (TextView) findViewById(R.id.tv_job_location_name);
        // tvLanguage = (TextView) findViewById(R.id.tv_des_laguages_name);
        //tvKeyfeatures = (TextView) findViewById(R.id.tv_des_features_content);
        tvDescription = (TextView) findViewById(R.id.tv_jobcourse_detail_content);
        tvCompanyName = (TextView) findViewById(R.id.tv_jobindustry_content);
        tvSalary = (TextView) findViewById(R.id.tv_jobsalary_content);

        tvApplyButton = (TextView) findViewById(R.id.tv_jobapply_now);

        tv_job_duration = (TextView) findViewById(R.id.tv_job_duration);
        tv_job_cource_location = (TextView) findViewById(R.id.tv_job_cource_location);
        tv_jobcourse_detail_header = (TextView) findViewById(R.id.tv_jobcourse_detail_header);
        tv_jobsalarys_header = (TextView) findViewById(R.id.tv_jobsalarys_header);
        tv_job_industury_header = (TextView) findViewById(R.id.tv_job_industury_header);

        tvApplyButton.setOnClickListener(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (ApplyVAlue) {
            tvApplyButton.setText(R.string.Applied);
        } else {
            tvApplyButton.setText(R.string.Apply_Now);
        }
    }

    public void onClick(View v) {
        if (v == tvApplyButton) {
            if (ApplyVAlue) {
                Common.showApplyDialog(JobDetailsActivity.this, getString(R.string.applyalready));
            } else {
                new HttpAsyncTaskCourseEnrolled().execute(CommonUrl.setJobApplied);
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private class HttpAsyncTaskCourseEnrolled extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourseEnrolled() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
//            arrCourse = new ArrayList<>();
//            courseAdapter = new CourseAdapter(getActivity(), arrCourse);
        }

        protected String doInBackground(String... urls) {

            return setEnrolled(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            rvCourse.setAdapter(courseAdapter);
            Utilities.showLog(result);
            if (applyStatus) {
                tvApplyButton.setText("Applied");
                Common.showApplyDialog(JobDetailsActivity.this, getString(R.string.apply));
            }
        }
    }

    public String setEnrolled(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();

            params.put("UserId", userId);
            Double d = new Double(jobId);
            params.put("JobId", d);
            params.put("Applied", "true");

            Utilities.showLog("OPTION" + userId + " " + jobId);

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                   /* JSONObject jsonoObject = new JSONObject(response.toString());
                    //status = jsonoObject.getString("Result");*/
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        applyStatus = true;
                        ApplyVAlue = true;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

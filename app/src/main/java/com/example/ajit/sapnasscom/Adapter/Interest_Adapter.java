package com.example.ajit.sapnasscom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Model.InterestModel;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

/**
 * Created by DTP-110 on 1/27/2017.
 */

public class Interest_Adapter extends BaseAdapter
{
    Activity mActivity;
    ArrayList<InterestModel> interestList;

    public Interest_Adapter(Activity a, ArrayList<InterestModel> data) {
        this.mActivity = a;
        this.interestList = data;
    }

    @Override
    public int getCount() {
        return interestList.size();
    }

    @Override
    public Object getItem(int position) {
        return interestList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.interest_text, null);

            viewHolder.tvInterest = (TextView) convertView
                    .findViewById(R.id.interest_text);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (interestList != null) {
            try {
                viewHolder.tvInterest.setText("" + interestList.get(position).getName());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView tvInterest;
    }

}



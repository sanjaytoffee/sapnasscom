package com.example.ajit.sapnasscom.Fragment;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.ajit.sapnasscom.Adapter.CourseAdapter;
import com.example.ajit.sapnasscom.Adapter.FeedBackAdapter;
import com.example.ajit.sapnasscom.Adapter.JobsAdapter;
import com.example.ajit.sapnasscom.Adapter.MyCourseAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.Feedback;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import static android.content.Context.INPUT_METHOD_SERVICE;
import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class FeedbackBottomSheetFragment extends BottomSheetDialogFragment {
    RecyclerView recyclerView;
    ArrayList<Feedback> skillarray;
    public static TextView tv_tool_right;
    ImageView cross_image;
    FeedBackAdapter adapter;
    String status, userId;
    public static EditText editComment;
    public static Button comment_save;
    public static TextView cross_text, feed, headinh;
    public static JSONArray jsonArray_Value;
    SessionManager sessionManager;
    DecimalFormat format;
    int mNoOfColumns;
    LocaleClass localeClass;
    ProgressDialog pd;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {

        View contentView = View.inflate(getContext(), R.layout.feedback_grid_activity, null);
        dialog.setContentView(contentView);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        hideSoftKeyboard();

        sessionManager = new SessionManager(getActivity());
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        jsonArray_Value = new JSONArray();
        skillarray = new ArrayList<>();
        recyclerView = (RecyclerView) contentView.findViewById(R.id.customgrid);
        cross_text = (TextView) contentView.findViewById(R.id.cross_text);
        feed = (TextView) contentView.findViewById(R.id.feed);
        headinh = (TextView) contentView.findViewById(R.id.headinh);

        pd = new ProgressDialog(getActivity());
        mNoOfColumns = Common.calculateNoOfColumns(getActivity());
        GridLayoutManager image_recycle_manager = new GridLayoutManager(getActivity(), mNoOfColumns);
        recyclerView.setLayoutManager(image_recycle_manager);
        adapter = new FeedBackAdapter(getActivity(), skillarray);
        recyclerView.setAdapter(adapter);

        tv_tool_right = (TextView) contentView.findViewById(R.id.tv_tool_right);
        cross_image = (ImageView) contentView.findViewById(R.id.cross_image);
        editComment = (EditText) contentView.findViewById(R.id.comment);
        comment_save = (Button) contentView.findViewById(R.id.comment_save);

        cross_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        skillarray = (ArrayList<Feedback>) getArguments().getSerializable("Skill_list");
        if (skillarray.size() != 0) {
            adapter = new FeedBackAdapter(getActivity(), skillarray);
            recyclerView.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }

        comment_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jsonArray_Value.length() != 0) {
                    askOption(getActivity());
                } else {
                    Toast.makeText(getActivity(), "Please select at least one feedback.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        format = new DecimalFormat();
        format.setDecimalSeparatorAlwaysShown(false);

        HashMap<String, String> enroll = sessionManager.getFeedback();
        String feedback = enroll.get(SessionManager.KEY_GET_FEEDBACK);

        if (feedback.equalsIgnoreCase("COURSE_FEEDBACK")) {
            cross_text.setText(R.string.give_feedback_on_this_course);
        } else {
            cross_text.setText(R.string.give_feedback_on_this_job);
        }

        localeClass = new LocaleClass();
        localeClass.loadLocale(getActivity(), "FeedbackBottomSheetFragment");
    }

    public void askOption(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_dialog);
        TextView text = (TextView) dialog.findViewById(R.id.tvMessage);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvBtnYes);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Common.isInternet(getActivity())) {
                    dialog.dismiss();
                    new HttpAsyncTaskFeedBack().execute(CommonUrl.feedBack);
                } else {
                    noInternet(getActivity());
                }
            }
        });
        text.setText("Are you sure you want to submit feedback?");

        TextView cancel = (TextView) dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private class HttpAsyncTaskFeedBack extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskFeedBack() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {
            return signUp(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pd.dismiss();

            if (status.equalsIgnoreCase("Success")) {
                Toast.makeText(getActivity(), "Your feedback submitted successfully.", Toast.LENGTH_LONG).show();
                dismiss();
                if (CourseAdapter.course.equalsIgnoreCase("COURSE")) {
                    Intent localImageintent = new Intent();
                    localImageintent.setAction("hitCoursedApi");
                    getActivity().sendBroadcast(localImageintent);
                } else if (MyCourseAdapter.mycourse.equalsIgnoreCase("MYCOURSE")) {
                    Intent localImageintent = new Intent();
                    localImageintent.setAction("hitMycourseEnrolledApiiiiii");
                    getActivity().sendBroadcast(localImageintent);
                } else if (JobsAdapter.jobStatus.equalsIgnoreCase("JOB")){
                    Intent localImageintent = new Intent();
                    localImageintent.setAction("hitjpbFeedbachapi");
                    getActivity().sendBroadcast(localImageintent);
                }
            } else {
                Toast.makeText(getActivity(), status, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String signUp(String url) {
        String result = "";
        try {
            JSONObject params = new JSONObject();
            if (CourseAdapter.course.equalsIgnoreCase("COURSE")) {
                String s = CourseAdapter.job;
                String[] split = s.split("\\.");
                String part1 = split[0];
                params.put("Type", "COURSE");
                params.put("RefID", "" + part1);
            } else if (JobsAdapter.jobStatus.equalsIgnoreCase("JOB")) {
                String s = JobsAdapter.job;
                String[] split = s.split("\\.");
                String part1 = split[0];
                params.put("Type", "JOB");
                params.put("RefID", "" + part1);
            }else if(MyCourseAdapter.mycourse.equalsIgnoreCase("MYCOURSE")) {
                String s = MyCourseAdapter.job;
                String[] split = s.split("\\.");
                String part1 = split[0];
                params.put("Type", "COURSE");
                params.put("RefID", "" + part1);
            }
            params.put("RefUserID", "" + userId);
            params.put("Feedback", "" + editComment.getText().toString());
            params.put("CreatedBy", "0");
            params.put("ModifiedBy", "0");
            params.put("IsActive", "1");
            params.put("lstJobsSubFeedback", jsonArray_Value);

            Log.e("FEEDBACK", params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());
                    status = jsonoObject.getString("Result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }
}

package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

/**
 * Created by HSTPL on 2/20/2018.
 */

public class ForgotSecond extends AppCompatActivity implements View.OnFocusChangeListener, View.OnKeyListener, TextWatcher {

    Toolbar toolbar;
    Button btnresend_otp, btnSubmitotp;
    String status;
    String userEmail, otp;
    SessionManager sessionManager;
    TextView response_email;
    public static EditText mPinFirstDigitEditText, mPinSecondDigitEditText, mPinThirdDigitEditText, mPinForthDigitEditText, mPinHiddenEditText;
    Intent intent;
    public static int j0 = 0, j1 = 0, j2 = 0, j3 = 0, j4 = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_password_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Forgot Password?");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        sessionManager = new SessionManager(getApplicationContext());

        init();
        setPINListeners();

        response_email = (TextView) findViewById(R.id.response_email);

        intent = getIntent();

        response_email.setText(intent.getStringExtra("mailId"));

        PrefUtils.setUserEmail(ForgotSecond.this, intent.getStringExtra("mailId"));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
    }

    public void recivedSms(String message) {
        try {
            Log.e("OTP", "" + message);
            String[] output = message.split("is ");
            String otp = output[1];
            Log.e("OTP", "" + otp);

            String number = String.valueOf(otp);
            for (int i = 0; i < number.length(); i++) {
                j0 = Character.digit(number.charAt(0), 10);
                j1 = Character.digit(number.charAt(1), 10);
                j2 = Character.digit(number.charAt(2), 10);
                j3 = Character.digit(number.charAt(3), 10);
            }
        } catch (Exception e) {
        }
    }

    private void init() {
        mPinFirstDigitEditText = (EditText) findViewById(R.id.pin_first_edittext);
        mPinSecondDigitEditText = (EditText) findViewById(R.id.pin_second_edittext);
        mPinThirdDigitEditText = (EditText) findViewById(R.id.pin_third_edittext);
        mPinForthDigitEditText = (EditText) findViewById(R.id.pin_forth_edittext);
        mPinHiddenEditText = (EditText) findViewById(R.id.pin_hidden_edittext);
        btnresend_otp = (Button) findViewById(R.id.resend_otp);
        btnSubmitotp = (Button) findViewById(R.id.submit_otp);
    }

    private void setPINListeners() {
        mPinHiddenEditText.addTextChangedListener(this);

        mPinFirstDigitEditText.setOnFocusChangeListener(this);
        mPinSecondDigitEditText.setOnFocusChangeListener(this);
        mPinThirdDigitEditText.setOnFocusChangeListener(this);
        mPinForthDigitEditText.setOnFocusChangeListener(this);

        mPinFirstDigitEditText.setOnKeyListener(this);
        mPinSecondDigitEditText.setOnKeyListener(this);
        mPinThirdDigitEditText.setOnKeyListener(this);
        mPinForthDigitEditText.setOnKeyListener(this);

        mPinHiddenEditText.setOnKeyListener(this);
        btnresend_otp.setOnKeyListener(this);
        btnSubmitotp.setOnKeyListener(this);
    }

    public static void setFocus(EditText editText) {
        if (editText == null)
            return;

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;
        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() == 0) {
            mPinFirstDigitEditText.setText("");
        } else if (s.length() == 1) {
            mPinFirstDigitEditText.setText(s.charAt(0) + "");
            mPinSecondDigitEditText.setText("");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 2) {
            mPinSecondDigitEditText.setText(s.charAt(1) + "");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 3) {
            mPinThirdDigitEditText.setText(s.charAt(2) + "");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 4) {
            mPinForthDigitEditText.setText(s.charAt(3) + "");
        }
    }

    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;
        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    public void onFocusChange(View v, boolean hasFocus) {
        final int id = v.getId();
        switch (id) {
            case R.id.pin_first_edittext:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin_second_edittext:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin_third_edittext:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin_forth_edittext:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            default:
                break;
        }
    }

    public void ButtonOnClick(View v) {
        switch (v.getId()) {

            case R.id.submit_otp:
                otp = mPinFirstDigitEditText.getText().toString() + mPinSecondDigitEditText.getText().toString() +
                        mPinThirdDigitEditText.getText().toString() + mPinForthDigitEditText.getText().toString();
                if (Common.isInternet(ForgotSecond.this)) {
                    if (otp.length() == 0) {
                        Toast.makeText(ForgotSecond.this, "Please Enter OTP First.", Toast.LENGTH_SHORT).show();
                    } else {
                        new HttpAsyncTaskForgot().execute(CommonUrl.getDashboardData + "ValidateUserOTP");
                    }
                } else {
                    noInternet(ForgotSecond.this);
                }
                break;

            case R.id.resend_otp:
                if (Common.isInternet(ForgotSecond.this)) {
                    new HttpAsyncTaskResendOtp().execute(CommonUrl.getDashboardData + "GenerateOTP");
                } else {
                    noInternet(ForgotSecond.this);
                }
                break;
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            final int id = v.getId();
            switch (id) {
                case R.id.pin_hidden_edittext:
                    if (keyCode == KeyEvent.KEYCODE_DEL) {

                        if (mPinHiddenEditText.getText().length() == 4)
                            mPinForthDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 3)
                            mPinThirdDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 2)
                            mPinSecondDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 1)
                            mPinFirstDigitEditText.setText("");

                        if (mPinHiddenEditText.length() > 0)
                            mPinHiddenEditText.setText(mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1));
                        return true;
                    }
                    break;
                default:
                    return false;
            }
        }

        return false;
    }

    private class HttpAsyncTaskForgot extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskForgot() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {

            return getOtp(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (status.equalsIgnoreCase("Success")) {
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), ForgotSubmit.class);
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), "You have entered incorrect OTP. Please enter correct OTP.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public String getOtp(String url) {
        String result = "";

        try {
            JSONObject params = new JSONObject();
            params.put("emailID", intent.getStringExtra("mailId"));
            params.put("OTP", otp);

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());
                    status = jsonoObject.getString("Result");
                    Log.e("OTPSTATUS", status);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private class HttpAsyncTaskResendOtp extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskResendOtp() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(ForgotSecond.this);
        }

        protected String doInBackground(String... urls) {

            return getresendOtp(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MyProgressDialog.hideDialog();
            if (status.equalsIgnoreCase("Success")) {

                response_email.setText(intent.getStringExtra("mailId"));
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
//                Intent intent = new Intent(getApplicationContext(), ForgotSecond.class);
//                startActivity(intent);

            } else {

                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String getresendOtp(String url) {
        String result = "";

        try {
            JSONObject params = new JSONObject();
            params.put("emailID", intent.getStringExtra("mailId"));

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());

                    status = jsonoObject.getString("Result");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}

package com.example.ajit.sapnasscom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Model.IndustryModel;
import com.example.ajit.sapnasscom.Model.InterestModel;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

/**
 * Created by DTP-110 on 1/27/2017.
 */

public class Industry_Adapter extends BaseAdapter
{
    Activity mActivity;
    ArrayList<IndustryModel> industryList;

    public Industry_Adapter(Activity a, ArrayList<IndustryModel> data) {
        this.mActivity = a;
        this.industryList = data;
    }

    @Override
    public int getCount() {
        return industryList.size();
    }

    @Override
    public Object getItem(int position) {
        return industryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.industry_text, null);

            viewHolder.tvIndustry = (TextView) convertView
                    .findViewById(R.id.industry_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (industryList != null) {
            try {
                viewHolder.tvIndustry.setText("" + industryList.get(position).getName());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView tvIndustry;
    }

}



package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Adapter.Education_Adapter;
import com.example.ajit.sapnasscom.Adapter.Language_Adapter;
import com.example.ajit.sapnasscom.Adapter.Location_Adapter;
import com.example.ajit.sapnasscom.Adapter.Month_Adapter;
import com.example.ajit.sapnasscom.Adapter.Preferedlocation_Adapter;
import com.example.ajit.sapnasscom.Adapter.Skills_Adapter;
import com.example.ajit.sapnasscom.Adapter.Specialization_Adapter;
import com.example.ajit.sapnasscom.Adapter.Year_Adapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.CurrentLocation;
import com.example.ajit.sapnasscom.Model.Education;
import com.example.ajit.sapnasscom.Model.Language;
import com.example.ajit.sapnasscom.Model.MonthModel;
import com.example.ajit.sapnasscom.Model.PreferedLocation;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.Model.Specilization;
import com.example.ajit.sapnasscom.Model.YearModel;
import com.example.ajit.sapnasscom.R;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

public class EditProfileActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button save;
    TextView tv_tool_right, tv_tool_left;
    Spinner education_spinner, specilization_spinner, language_spinner, skill_spinner;
    private ArrayList<Education> educationList;
    private ArrayList<Specilization> specilizationList;
    Education_Adapter education_adapter;
    Specialization_Adapter specialization_adapter;

    Education education;
    Specilization specilization;
    String specialization_id, month_name, status, month_id, year_name, year_id, specialization_name, userId, education_name, education_id, skill_id, skill_name, language_id, language_name, PreferedLocationId, currentLocationId, strResult, userPass, userEmail, lastName, firstName;

    Language language;
    ArrayList<Language> languageList;
    Language_Adapter language_adapter;

    Skills_Adapter skills_adapter;
    ArrayList<Skills> skillsArrayList;
    Skills skills;

    CurrentLocation currentLocation;
    Spinner preferred_Location, spinnerCurrentLocation, yearSpinner, monthSpinner;
    String location_id, location_name, prefered_location_id, prefered_location_name;

    ArrayList<CurrentLocation> locationArrayList;
    Location_Adapter location_adapter;
    SessionManager sessionManager;

    ArrayList<PreferedLocation> preferedLocationList;
    Preferedlocation_Adapter preferedlocation_adapter;
    PreferedLocation preferedLocation;

    ArrayList<YearModel> yearList;
    Year_Adapter year_adapter;
    YearModel yearModel;

    ArrayList<MonthModel> monthList;
    Month_Adapter month_adapter;
    MonthModel monthModel;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofile_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Profile Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        sessionManager = new SessionManager(getApplicationContext());
//        tv_tool_left = (TextView) findViewById(R.id.tv_tool_left);
        tv_tool_right = (TextView) findViewById(R.id.tv_tool_right);

        save = (Button) findViewById(R.id.save_data);
        spinnerCurrentLocation = (Spinner) findViewById(R.id.current_lication_spinner);
        preferred_Location = (Spinner) findViewById(R.id.preferreg_loc);
        education_spinner = (Spinner) findViewById(R.id.education_spinner);
        specilization_spinner = (Spinner) findViewById(R.id.specilization_spinner);
        language_spinner = (Spinner) findViewById(R.id.language_spinner);
        skill_spinner = (Spinner) findViewById(R.id.skill);
        yearSpinner = (Spinner) findViewById(R.id.year_spinner);
        monthSpinner = (Spinner) findViewById(R.id.month_sinner);

        educationList = new ArrayList<>();
        specilizationList = new ArrayList<>();

        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);

        new HttpAsyncGetProfile().execute(CommonUrl.editProfileDataById);

        new HttpAsyncTask_Education().execute(CommonUrl.drop_down + "GetDropdownList");
        new HttpAsyncTask_Skils().execute(CommonUrl.drop_down + "GetDropdownList");
        new HttpAsyncTask_CurrentLocation().execute(CommonUrl.drop_down + "GetDropdownList");
        new HttpAsyncTask_PreferedLocation().execute(CommonUrl.drop_down + "GetDropdownList");
        new HttpAsyncTask_Year().execute(CommonUrl.drop_down + "GetDropdownList");
        new HttpAsyncTask_Month().execute(CommonUrl.drop_down + "GetDropdownList");

        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                year_id = yearList.get(i).getID();
                year_name = yearList.get(i).getName();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                month_id = monthList.get(i).getID();
                month_name = monthList.get(i).getName();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        education_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                education_id = educationList.get(i).getID();
                education_name = educationList.get(i).getName();

                new HttpAsyncTask_Specilization().execute(CommonUrl.drop_down + "GetDropdownList");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        specilization_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                specialization_name = specilizationList.get(i).getName();
                specialization_id = specilizationList.get(i).getID();

                new HttpAsyncTask_Language().execute(CommonUrl.drop_down + "GetAllLanguage");
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        language_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                language_name = languageList.get(i).getName();
                language_id = languageList.get(i).getID();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        skill_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                skill_name = skillsArrayList.get(i).getName();
                skill_id = skillsArrayList.get(i).getID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerCurrentLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                location_name = locationArrayList.get(i).getName();
                location_id = locationArrayList.get(i).getID();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        preferred_Location.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                prefered_location_name = preferedLocationList.get(i).getName();
                prefered_location_id = preferedLocationList.get(i).getId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new HttpAsyncTaskSaveProfile().execute(CommonUrl.Web_login + "SaveUserProfileDetails");
            }
        });
    }

    private class HttpAsyncGetProfile extends AsyncTask<String, Void, String> {
        private HttpAsyncGetProfile() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {

            return getchProfileData(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

        }
    }

    public String getchProfileData(String url) {
        String result = "";

        try {
            JSONObject params = new JSONObject();
            params.put("Id", userId);

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());

                   Log.e("PROFILE",response.toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private class HttpAsyncTaskSaveProfile extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskSaveProfile() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {

            return fetchData(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (status.equalsIgnoreCase("Success")) {

                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
                finish();

            } else {

                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String fetchData(String url) {
        String result = "";

        try {
            JSONObject params = new JSONObject();
            params.put("Id", "");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserId", userId);
            jsonObject.put("CurrentLocId", location_id);
            jsonObject.put("PreferredLocId", prefered_location_id);
            jsonObject.put("DegreeId", education_id);
            jsonObject.put("SpecializationId", specialization_id);
            jsonObject.put("LaguageId", language_id);
            jsonObject.put("Year", year_id);
            jsonObject.put("Month", month_id);
            params.put("UserProfile", jsonObject);

            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject1 = new JSONObject();
            {
                jsonObject1.put("PrimarySkills", skill_id);
                jsonArray.put(jsonObject1);
            }
            params.put("UserSkillsList", jsonArray);

            JSONArray locationArray = new JSONArray();
            JSONObject locationObject = new JSONObject();
            {
                locationObject.put("CityId", location_id);
                locationArray.put(locationObject);
            }
            params.put("UserCurrentLocationList", locationArray);

            JSONArray preferedLocationArray = new JSONArray();
            JSONObject preferedLocationObject = new JSONObject();
            {
                preferedLocationObject.put("CityId",prefered_location_id);
                preferedLocationArray.put(preferedLocationObject);
            }
            params.put("UserPreferredLocationList", preferedLocationArray);

            Log.e("SAVEDATA", String.valueOf(params));

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());

                    status = jsonoObject.getString("Result");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Skils extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchSkill(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                skills_adapter = new Skills_Adapter(EditProfileActivity.this, skillsArrayList);
                skill_spinner.setAdapter(skills_adapter);
            }
        }
    }

    public String fetchSkill(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "PRIMARYSKILLS ");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                skillsArrayList = new ArrayList<>();
                skills = new Skills();
                skills.setName("Skills");
                skills.setID("0");
                skillsArrayList.add(skills);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        skills = new Skills();

                        skills.setID(catObj.getString("ID"));
                        skills.setCode(catObj.getString("Code"));
                        skills.setName(catObj.getString("Name"));

                        skillsArrayList.add(skills);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Education extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchEducation(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                education_adapter = new Education_Adapter(EditProfileActivity.this, educationList);
                education_spinner.setAdapter(education_adapter);

            }
        }
    }

    public String fetchEducation(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "DEGREE");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                educationList = new ArrayList<>();
                education = new Education();
                education.setName("Select Degree");
                education.setID("0");
                educationList.add(education);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        education = new Education();
                        education.setID(catObj.getString("ID"));
                        education.setCode(catObj.getString("Code"));
                        education.setName(catObj.getString("Name"));
                        educationList.add(education);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Specilization extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchSpecilization(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                specialization_adapter = new Specialization_Adapter(EditProfileActivity.this, specilizationList);
                specilization_spinner.setAdapter(specialization_adapter);
            }
        }
    }

    public String fetchSpecilization(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "SPECIALIZATION");
            params.put("KeyOne", education_id);
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                specilizationList = new ArrayList<>();
                specilization = new Specilization();
                specilization.setName("Specialization");
                specilizationList.add(specilization);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        specilization = new Specilization();

                        specilization.setID(catObj.getString("ID"));
                        specilization.setCode(catObj.getString("Code"));
                        specilization.setName(catObj.getString("Name"));

                        specilizationList.add(specilization);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Language extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchLanguage(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                language_adapter = new Language_Adapter(EditProfileActivity.this, languageList);
                language_spinner.setAdapter(language_adapter);
            }
        }
    }

    public String fetchLanguage(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "LANGUAGE");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                languageList = new ArrayList<>();
                language = new Language();
                language.setName("Select");
                language.setID("0");
                languageList.add(language);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        language = new Language();

                        language.setID(catObj.getString("ID"));
                        language.setCode(catObj.getString("Code"));
                        language.setName(catObj.getString("Name"));

                        languageList.add(language);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_CurrentLocation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchCurrentLocation(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                location_adapter = new Location_Adapter(EditProfileActivity.this, locationArrayList);
                spinnerCurrentLocation.setAdapter(location_adapter);
            }
        }
    }

    public String fetchCurrentLocation(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "INDIACITY");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                locationArrayList = new ArrayList<>();
                currentLocation = new CurrentLocation();
                currentLocation.setName("Select Location");
                currentLocation.setID("0");
                locationArrayList.add(currentLocation);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        currentLocation = new CurrentLocation();
                        currentLocation.setID(catObj.getString("ID"));
                        currentLocation.setCode(catObj.getString("Code"));
                        currentLocation.setName(catObj.getString("Name"));
                        locationArrayList.add(currentLocation);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_PreferedLocation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchPreferedLocation(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                preferedlocation_adapter = new Preferedlocation_Adapter(EditProfileActivity.this, preferedLocationList);
                preferred_Location.setAdapter(preferedlocation_adapter);

            }
        }
    }

    public String fetchPreferedLocation(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "INDIACITY");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                preferedLocationList = new ArrayList<>();
                preferedLocation = new PreferedLocation();
                preferedLocation.setName("Select Location");
                preferedLocation.setId("0");
                preferedLocationList.add(preferedLocation);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        preferedLocation = new PreferedLocation();
                        preferedLocation.setId(catObj.getString("ID"));
                        preferedLocation.setCode(catObj.getString("Code"));
                        preferedLocation.setName(catObj.getString("Name"));

                        preferedLocationList.add(preferedLocation);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    class HttpAsyncTask_Month extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchMonth(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                month_adapter = new Month_Adapter(EditProfileActivity.this, monthList);
                monthSpinner.setAdapter(month_adapter);

            }
        }
    }

    public String fetchMonth(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "MONTH");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                monthList = new ArrayList<>();
                monthModel = new MonthModel();
                monthModel.setName("Month");
                monthModel.setID("0");
                monthList.add(monthModel);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        monthModel = new MonthModel();

                        monthModel.setID(catObj.getString("ID"));
                        monthModel.setCode(catObj.getString("Code"));
                        monthModel.setName(catObj.getString("Name"));

                        monthList.add(monthModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    class HttpAsyncTask_Year extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchYear(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                year_adapter = new Year_Adapter(EditProfileActivity.this, yearList);
                yearSpinner.setAdapter(year_adapter);
            }
        }
    }

    public String fetchYear(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "YEAR");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                yearList = new ArrayList<>();
                yearModel = new YearModel();
                yearModel.setName("Year");
                yearModel.setID("0");
                yearList.add(yearModel);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        yearModel = new YearModel();

                        yearModel.setID(catObj.getString("ID"));
                        yearModel.setCode(catObj.getString("Code"));
                        yearModel.setName(catObj.getString("Name"));

                        yearList.add(yearModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
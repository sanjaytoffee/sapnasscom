package com.example.ajit.sapnasscom.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.SelectInterestActivity;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.PrefUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Secondry_adapter extends BaseAdapter {

    ArrayList<Skills> result;
    ArrayList<String> skillList;

    public static JSONObject jObj;
    ArrayList<String> list;
    HashMap<String, String> hm;
    Context context;
    int[] imageId;
    int count = 0;
    Holder holder;
    CustomAdapter customAdapter;
    private static LayoutInflater inflater = null;
    JSONObject json;

    public Secondry_adapter(Context context, ArrayList<Skills> osNameList) {
        // TODO Auto-generated constructor stub
        result = osNameList;
        this.context = context;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        jObj = new JSONObject();
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView os_text;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        skillList = new ArrayList<String>();
        final Holder holder = new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.type_text, null);
        holder.os_text = (TextView) rowView.findViewById(R.id.os_texts);
        holder.os_text.setText("" + result.get(position).getName());

        holder.os_text.setOnClickListener(new OnClickListener() {

            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                boolean isSelectedAfterClick = !v.isSelected();
                v.setSelected(isSelectedAfterClick);

                if (result.get(position).getPreSelected().equalsIgnoreCase("1")) {
                    for (int i = 0; i < SelectInterestActivity.jsonArray.length(); i++) {
                        try {
                            if (result.get(position).getID().equalsIgnoreCase(SelectInterestActivity.jsonArray.getJSONObject(i).getString("SecondarySkills"))) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    SelectInterestActivity.jsonArray.remove(i);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.textWhite));
                } else {
                    if (isSelectedAfterClick) {
                        try {
                            json = new JSONObject();
                            json.put("Id", "0");
                            json.put("UserId", PrefUtils.getUserID(context));
                            json.put("PrimarySkills", result.get(position).getPrimarySkillsId());
                            json.put("SecondarySkills", result.get(position).getID());
                            SelectInterestActivity.jsonArray.put(json);
                            Log.e("SELEct Skill", "" + SelectInterestActivity.jsonArray.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.splashColor));
                        result.get(position).setSelected(true);

                    } else if (!isSelectedAfterClick) {
                        for (int i = 0; i < SelectInterestActivity.jsonArray.length(); i++) {
                            try {
                                if (result.get(position).getID().equalsIgnoreCase(SelectInterestActivity.jsonArray.getJSONObject(i).getString("SecondarySkills"))) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                        SelectInterestActivity.jsonArray.remove(i);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.textWhite));
                    }
                }

            }
        });

        holder.os_text.setTag(position);

        if (result.get(position).isSelected()) {
            holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.splashColor));
        } else {
            holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.textWhite));
        }

        if (result.get(position).getPreSelected().equalsIgnoreCase("1")) {
            // result.get(position).setSelected(true);
            holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.splashColor));
        } else {
            holder.os_text.setBackgroundColor(context.getResources().getColor(R.color.textWhite));
        }

//        try {
//            if (result.get(position).getPreSelected().equalsIgnoreCase("")) {
//                json = new JSONObject();
//                json.put("Id", "0");
//                json.put("UserId", PrefUtils.getUserID(context));
//                json.put("PrimarySkills", result.get(position).getPrimarySkillsId());
//                json.put("SecondarySkills", result.get(position).getID());
//                SelectInterestActivity.jsonArray.put(json);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return rowView;
    }

}
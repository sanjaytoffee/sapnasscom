package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

/**
 * Created by HSTPL on 2/20/2018.
 */

public class SignupValidateActivity extends AppCompatActivity implements View.OnFocusChangeListener, View.OnKeyListener, TextWatcher {
    Toolbar toolbar;
    Button btnSubmitotp;
    String status;
    String otp;
    SessionManager sessionManager;
    TextView response_email;
    String responseId, userId;
    int count = 0;
    public static EditText mPinFirstDigitEditText, mPinSecondDigitEditText, mPinThirdDigitEditText, mPinHiddenEditText, mPinForthDigitEditText;
    Intent intent;
    Bundle bundle;
    String FirstName, LastName, GenderSelect, Email, Password, mobileNo;
    public static int a0 = 0, a1 = 0, a2 = 0, a3 = 0, j4 = 0;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_validate_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Validate OTP?");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        sessionManager = new SessionManager(getApplicationContext());

        bundle = getIntent().getExtras();
        FirstName = bundle.getString("FirstName");
        LastName = bundle.getString("LastName");
        GenderSelect = bundle.getString("GenderSelect");
        Email = bundle.getString("Email");
        Password = bundle.getString("Password");
        mobileNo = bundle.getString("MobileNumber");


//        Log.e("MOBILENO", mobileNo);
        count = 0;
        init();
        setPINListeners();

        response_email = (TextView) findViewById(R.id.response_email);

        intent = getIntent();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
    }

    public static void recivedSms(String message) {
        try {
            Log.e("OTP", "" + message);
            String[] output = message.split("is ");
            String otp = output[1];
            Log.e("OTP", "" + otp);

            String number = String.valueOf(otp);
            for (int i = 0; i < number.length(); i++) {
                a0 = Character.digit(number.charAt(0), 10);
                a1 = Character.digit(number.charAt(1), 10);
                a2 = Character.digit(number.charAt(2), 10);
                a3 = Character.digit(number.charAt(3), 10);
            }

        } catch (Exception e) {
        }
    }

    private void init() {
        mPinFirstDigitEditText = (EditText) findViewById(R.id.pin_first_edittext);
        mPinSecondDigitEditText = (EditText) findViewById(R.id.pin_second_edittext);
        mPinThirdDigitEditText = (EditText) findViewById(R.id.pin_third_edittext);
        mPinForthDigitEditText = (EditText) findViewById(R.id.pin_forth_edittext);
        mPinHiddenEditText = (EditText) findViewById(R.id.pin_hidden_edittext);
        btnSubmitotp = (Button) findViewById(R.id.validate_otp);
    }

    private void setPINListeners() {
        mPinHiddenEditText.addTextChangedListener(this);

        mPinFirstDigitEditText.setOnFocusChangeListener(this);
        mPinSecondDigitEditText.setOnFocusChangeListener(this);
        mPinThirdDigitEditText.setOnFocusChangeListener(this);
        mPinForthDigitEditText.setOnFocusChangeListener(this);

        mPinFirstDigitEditText.setOnKeyListener(this);
        mPinSecondDigitEditText.setOnKeyListener(this);
        mPinThirdDigitEditText.setOnKeyListener(this);
        mPinForthDigitEditText.setOnKeyListener(this);

        mPinHiddenEditText.setOnKeyListener(this);
        btnSubmitotp.setOnKeyListener(this);
    }

    public static void setFocus(EditText editText) {
        if (editText == null)
            return;

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;
        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if (s.length() == 0) {
            mPinFirstDigitEditText.setText("");
        } else if (s.length() == 1) {
            mPinFirstDigitEditText.setText(s.charAt(0) + "");
            mPinSecondDigitEditText.setText("");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 2) {
            mPinSecondDigitEditText.setText(s.charAt(1) + "");
            mPinThirdDigitEditText.setText("");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 3) {
            mPinThirdDigitEditText.setText(s.charAt(2) + "");
            mPinForthDigitEditText.setText("");
        } else if (s.length() == 4) {
            mPinForthDigitEditText.setText(s.charAt(3) + "");
        }
    }


    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;
        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    public void onFocusChange(View v, boolean hasFocus) {
        final int id = v.getId();
        switch (id) {
            case R.id.pin_first_edittext:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin_second_edittext:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin_third_edittext:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.pin_forth_edittext:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            default:
                break;
        }
    }

    public void ButtonOnClick(View v) {
        switch (v.getId()) {

            case R.id.validate_otp:
                otp = mPinFirstDigitEditText.getText().toString() + mPinSecondDigitEditText.getText().toString() +
                        mPinThirdDigitEditText.getText().toString() + mPinForthDigitEditText.getText().toString();
                if (Common.isInternet(SignupValidateActivity.this)) {
                    if (otp.length() == 0) {
                        Toast.makeText(SignupValidateActivity.this, "Please Enter OTP First.", Toast.LENGTH_SHORT).show();
                    } else {
                        veryfyOtp();
                    }
                } else {
                    noInternet(SignupValidateActivity.this);
                }
                break;
        }
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            final int id = v.getId();
            switch (id) {
                case R.id.pin_hidden_edittext:
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if (mPinHiddenEditText.getText().length() == 4)
                            mPinForthDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 3)
                            mPinThirdDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 2)
                            mPinSecondDigitEditText.setText("");
                        else if (mPinHiddenEditText.getText().length() == 1)
                            mPinFirstDigitEditText.setText("");

                        if (mPinHiddenEditText.length() > 0)
                            mPinHiddenEditText.setText(mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1));
                        return true;
                    }
                    break;
                default:
                    return false;
            }
        }
        return false;
    }

    private void veryfyOtp() {
        MyProgressDialog.showDialog(SignupValidateActivity.this);
        RequestQueue queue = Volley.newRequestQueue(SignupValidateActivity.this);
        queue.getCache().clear();
        JSONObject object = new JSONObject();
        try {
            object.put("Mobile", mobileNo);
            object.put("Email", Email);
            object.put("OTP", otp);

            final String requestBody = object.toString();
            Log.e("Response", requestBody);

            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, CommonUrl.ValidateSignUpOtp, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("onResponse", response.toString());
                    try {
                        status = response.getString("Result");
                        if (status.equalsIgnoreCase("Success")) {
                            Toast.makeText(getApplicationContext(), "OTP is verified.", Toast.LENGTH_LONG).show();
                            new HttpAsyncTaskSignup().execute(CommonUrl.Web_login + "SaveUserDetails");
                        } else {
                            Toast.makeText(getApplicationContext(), "Please enter valid OTP.", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    MyProgressDialog.hideDialog();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("onErrorResponse", error.toString());
                    MyProgressDialog.hideDialog();
                }
            }) {
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    String user = "unnati";
                    String pwd = "unnati@123";
                    headers.put("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
                    return headers;
                }
            };
            queue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class HttpAsyncTaskSignup extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskSignup() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(SignupValidateActivity.this);
        }

        protected String doInBackground(String... urls) {

            return signUp(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MyProgressDialog.hideDialog();
            if (status.equalsIgnoreCase("Success")) {
                sessionManager.createEmail("", responseId, "");
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), ProfileDetailsActivity.class);
                intent.putExtra("editProfile", "login");
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String signUp(String url) {
        String result = "";
        String encryptedPassword = computeMD5Hash(Password);
        try {
            JSONObject params = new JSONObject();
            params.put("Id", "0");
            params.put("Email", Email);
            params.put("FirstName", FirstName);
            params.put("LastName", LastName);
            params.put("Password", encryptedPassword);
            params.put("LoginType", "USER");
            params.put("ClientID", "0");
            params.put("UserFrom", "");
            params.put("PhoneNo", mobileNo);
            params.put("WebSite", "");
            params.put("FaceBook", "");
            params.put("Twitter", "");
            params.put("LinkedIn", "");
            params.put("Gender", GenderSelect);

            Log.e("SIGN", params.toString());
            Log.e("SIGNURL", url);
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());
                    status = jsonoObject.getString("Response");
                    Log.e("RESPONSE", response.toString());
                    responseId = jsonoObject.getString("Id");
                    PrefUtils.setUserID(SignupValidateActivity.this, responseId);
                    PrefUtils.setUserEmail(SignupValidateActivity.this, jsonoObject.getString("Email"));
                    PrefUtils.setUserFullName(SignupValidateActivity.this, jsonoObject.getString("UserFullName"));
                    PrefUtils.setUserFirstName(SignupValidateActivity.this, jsonoObject.getString("FirstName"));
                    PrefUtils.setUserLastName(SignupValidateActivity.this, jsonoObject.getString("LastName"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public final String computeMD5Hash(final String pass) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(pass.getBytes());
            byte messageDigest[] = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            System.out.println(hexString.toString());
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


}

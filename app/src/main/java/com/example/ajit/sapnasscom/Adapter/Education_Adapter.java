package com.example.ajit.sapnasscom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Model.Education;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

/**
 * Created by DTP-110 on 1/27/2017.
 */

public class Education_Adapter extends BaseAdapter
{

    Activity mActivity;
    ArrayList<Education> education;

    public Education_Adapter(Activity a, ArrayList<Education> data) {
        this.mActivity = a;
        this.education = data;
    }



    @Override
    public int getCount() {
        return education.size();
    }

    @Override
    public Object getItem(int position) {
        return education.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.education_text, null);

            viewHolder.educationtext = (TextView) convertView
                    .findViewById(R.id.education_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (education != null) {
            try {
                viewHolder.educationtext.setText("" + education.get(position).getName());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView educationtext;
    }

}



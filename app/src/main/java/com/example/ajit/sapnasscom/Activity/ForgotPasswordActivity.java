package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

/**
 * Created by HSTPL on 2/20/2018.
 */

public class ForgotPasswordActivity extends AppCompatActivity {

    Toolbar toolbar;
    public static Button btnForgot;
    String status;
    EditText ed_forgetEmail;
    public static TextView email_text;
    LocaleClass localeClass;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgot_password_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("Forgot Password?");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        btnForgot = (Button) findViewById(R.id.reset_button);
        ed_forgetEmail = (EditText) findViewById(R.id.user_forgetemail);
        email_text = (TextView) findViewById(R.id.email_text);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });

        btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.isInternet(ForgotPasswordActivity.this)) {
                    if (ed_forgetEmail.getText().length() == 0) {
                        Toast.makeText(ForgotPasswordActivity.this, "Please Enter Email Id first.", Toast.LENGTH_SHORT).show();
                    } else {
                        new HttpAsyncTaskForgot().execute(CommonUrl.getDashboardData + "GenerateOTP");
                    }
                } else {
                    noInternet(ForgotPasswordActivity.this);
                }
            }
        });

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "ForgotPasswordActivity");
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private class HttpAsyncTaskForgot extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskForgot() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(ForgotPasswordActivity.this);
        }

        protected String doInBackground(String... urls) {

            return signUp(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MyProgressDialog.hideDialog();
            if (status.equalsIgnoreCase("Success")) {
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), ForgotSecond.class);
                intent.putExtra("mailId", ed_forgetEmail.getText().toString());
                startActivity(intent);
            } else {
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String signUp(String url) {
        String result = "";

        try {
            JSONObject params = new JSONObject();
            params.put("emailID", ed_forgetEmail.getText().toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());

                    status = jsonoObject.getString("Result");
                    Log.e("FORGOT", status);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}

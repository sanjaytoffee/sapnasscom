package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class SignupActivity extends AppCompatActivity implements View.OnClickListener, ActivityCompat.OnRequestPermissionsResultCallback {
    public static Toolbar toolbar;
    EditText editfirstName, editlastName, editEmail, editPassword, editMobileNumber;
    public static Button buttonSignup;
    TextView tv_tool_left, tv_tool_right;
    public static RadioButton radioFemale, radioMale;
    String radioButtonSelected = "";
    String responseId, status, userId;
    SessionManager sessionManager;
    public static TextView textViewName, textViewGender, textViewEmail, textViewPassword, textViewMobileNumver, tvLanguage;
    LocaleClass localeClass;
    String lang = "en";
    Spinner spinnerMultipleLanguage;
    public String selectedLanguage = null, selectedLang,mobileNumber,EmailId;
    LinearLayout multiLanguage, mainSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Sign Up");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textFieldValidation();

        buttonSignup.setOnClickListener(this);
        radioFemale.setOnClickListener(this);
        radioMale.setOnClickListener(this);
        mainSignup.setOnClickListener(this);
        multiLanguage.setOnClickListener(this);

        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        localeClass = new LocaleClass();
        localeClass.loadLocale(getApplicationContext(), "Sign up");
    }

    public void textFieldValidation() {
        sessionManager = new SessionManager(getApplicationContext());

        HashMap<String, String> langh = sessionManager.getLanguage();
        selectedLang = langh.get(SessionManager.KEY_LANGUAGE);

        mainSignup = (LinearLayout) findViewById(R.id.mainSignup);
        multiLanguage = (LinearLayout) findViewById(R.id.multiLanguage);

        editfirstName = (EditText) findViewById(R.id.first_name);
        editlastName = (EditText) findViewById(R.id.last_name);
        editEmail = (EditText) findViewById(R.id.email_address);
        editPassword = (EditText) findViewById(R.id.password);
        editMobileNumber = (EditText) findViewById(R.id.mobile_numbaer);
        buttonSignup = (Button) findViewById(R.id.signup_button);
        tv_tool_left = (TextView) findViewById(R.id.tv_tool_left);
        tv_tool_right = (TextView) findViewById(R.id.tv_tool_right);
        radioFemale = (RadioButton) findViewById(R.id.female);
        radioMale = (RadioButton) findViewById(R.id.male);
        textViewName = (TextView) findViewById(R.id.txt_name);
        textViewGender = (TextView) findViewById(R.id.txt_gender);
        textViewEmail = (TextView) findViewById(R.id.txt_Email);
        textViewPassword = (TextView) findViewById(R.id.txt_password);
        textViewMobileNumver = (TextView) findViewById(R.id.txt_mobileNumber);
        tvLanguage = (TextView) findViewById(R.id.tv_signup_language);

        spinnerMultipleLanguage = (Spinner) findViewById(R.id.multiple_language);

        editMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() == 10)
                    hideSoftKeyboard();
            }
        });

        editEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                 EmailId = editEmail.getText().toString();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        editMobileNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                 mobileNumber = editMobileNumber.getText().toString();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {

            }
        });

        List<String> list = new ArrayList<String>();
        list.add("Select Language");
        list.add("English");
        list.add("Hindi");
        list.add("Kannada");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMultipleLanguage.setAdapter(dataAdapter);

        if (selectedLang != null) {
            int spinnerPosition = dataAdapter.getPosition(selectedLang);
            spinnerMultipleLanguage.setSelection(spinnerPosition);
        }

        spinnerMultipleLanguage.setOnItemSelectedListener(new MyOnItemSelectedListener());

        spinnerMultipleLanguage.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard();
                return false;
            }
        });
    }

    public class MyOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
            String selectedItem = parent.getItemAtPosition(pos).toString();
            switch (parent.getId()) {
                case R.id.multiple_language:

                    if (selectedLanguage != null) {
                        if (selectedItem.equals("English")) {
                            lang = "en";
                            localeClass.changeLang(lang, getApplicationContext());
                        } else if (selectedItem.equals("Hindi")) {
                            lang = "hi";
                            localeClass.changeLang(lang, getApplicationContext());
                        } else if (selectedItem.equals("Kannada")) {
                            lang = "ka";
                            localeClass.changeLang(lang, getApplicationContext());
                        }
                    }
                    selectedLanguage = selectedItem;
                    break;
            }
        }

        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void submitLogin() {

        if (editfirstName.getText().toString().length() == 0) {
            Toast.makeText(this, "Please enter first name.", Toast.LENGTH_SHORT).show();
        } else if (editlastName.getText().toString().length() == 0) {
            Toast.makeText(this, "Please enter last name.", Toast.LENGTH_SHORT).show();
        } else if (radioButtonSelected.length() == 0) {
            Toast.makeText(this, "Please select gender.", Toast.LENGTH_SHORT).show();
        } else if (!isValidEmaillId(editEmail.getText().toString().trim())) {
            Toast.makeText(this, "Please enter vaild email.", Toast.LENGTH_SHORT).show();
        } else if (editPassword.getText().toString().length() == 0) {
            Toast.makeText(this, "Please enter password.", Toast.LENGTH_SHORT).show();
        } else if (!editMobileNumber.getText().toString().matches("[6-9][0-9]{9}")) {
            Toast.makeText(this, "Please enter valid mobile number.", Toast.LENGTH_SHORT).show();
        } else {
            if (Common.isInternet(SignupActivity.this)) {
                getOtp();
            } else {
                noInternet(SignupActivity.this);
            }
        }
    }

    private void getOtp() {
        MyProgressDialog.showDialog(SignupActivity.this);
        RequestQueue queue = Volley.newRequestQueue(SignupActivity.this);
        queue.getCache().clear();
        JSONObject object = new JSONObject();
        try {
            object.put("Mobile", mobileNumber);
            object.put("Email", EmailId);

            final String requestBody = object.toString();
            Log.e("Response", requestBody);

            Log.e("GENERATEOTP", CommonUrl.SignOtpGenerate);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, CommonUrl.SignOtpGenerate, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("onResponse", response.toString());
                    MyProgressDialog.hideDialog();
                    try {
                        if (response.getString("Result").toString().equalsIgnoreCase("Success")) {
                           // Toast.makeText(getApplicationContext(), response.getString("Result").toString(), Toast.LENGTH_SHORT).show();

                            Intent intent = new Intent(SignupActivity.this, SignupValidateActivity.class);
                            intent.putExtra("FirstName", editfirstName.getText().toString());
                            intent.putExtra("LastName", editlastName.getText().toString());
                            intent.putExtra("GenderSelect", radioButtonSelected);
                            intent.putExtra("Email", EmailId);
                            intent.putExtra("Password", editPassword.getText().toString());
                            intent.putExtra("MobileNumber", mobileNumber);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), response.getString("Result").toString(), Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("onErrorResponse", error.toString());
                    MyProgressDialog.hideDialog();
                }
            }) {
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    String user = "unnati";
                    String pwd = "unnati@123";
                    headers.put("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
                    return headers;
                }
            };
            queue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class HttpAsyncTaskSignup extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskSignup() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(SignupActivity.this);
        }

        protected String doInBackground(String... urls) {

            return signUp(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MyProgressDialog.hideDialog();
            if (status.equalsIgnoreCase("Success")) {
                sessionManager.createEmail("", responseId, "");
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(SignupActivity.this, SignupValidateActivity.class);

                intent.putExtra("FirstName", editfirstName.getText().toString());
                intent.putExtra("LastName", editlastName.getText().toString());
                intent.putExtra("GenderSelect", radioButtonSelected);
                intent.putExtra("Email", EmailId);
                intent.putExtra("Password", editPassword.getText().toString());
                intent.putExtra("MobileNumber", editMobileNumber.getText().toString());

                startActivity(intent);

            } else {
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String signUp(String url) {
        String result = "";
        String encryptedPassword = computeMD5Hash(editPassword.getText().toString());
        try {
            JSONObject params = new JSONObject();
            params.put("Id", "0");
            params.put("Email", editEmail.getText().toString());
            params.put("FirstName", editfirstName.getText().toString());
            params.put("LastName", editlastName.getText().toString());
            params.put("Password", encryptedPassword);
            params.put("LoginType", "USER");
            params.put("ClientID", "0");
            params.put("UserFrom", "");
            params.put("PhoneNo", editMobileNumber.getText().toString());
            params.put("WebSite", "");
            params.put("FaceBook", "");
            params.put("Twitter", "");
            params.put("LinkedIn", "");
            params.put("Gender", radioButtonSelected);

            Log.e("SIGN", params.toString());
            Log.e("SIGNURL", url);
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());

                    status = jsonoObject.getString("Response");
                    Log.e("RESPONSE", response.toString());
                    responseId = jsonoObject.getString("Id");
                    PrefUtils.setUserID(SignupActivity.this, responseId);
                    PrefUtils.setUserEmail(SignupActivity.this, jsonoObject.getString("Email"));
                    PrefUtils.setUserFullName(SignupActivity.this, jsonoObject.getString("UserFullName"));
                    PrefUtils.setUserFirstName(SignupActivity.this, jsonoObject.getString("FirstName"));
                    PrefUtils.setUserLastName(SignupActivity.this, jsonoObject.getString("LastName"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.multiLanguage:
                hideSoftKeyboard();
                break;

            case R.id.signup_button:

                submitLogin();
                break;

            case R.id.female:
                radioButtonSelected = "Female";
                break;

            case R.id.male:
                radioButtonSelected = "Male";
                break;

            case R.id.mainSignup:
                hideSoftKeyboard();
                break;
        }
    }

    public final String computeMD5Hash(final String pass) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(pass.getBytes());
            byte messageDigest[] = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            System.out.println(hexString.toString());
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }


    private boolean isValidEmaillId(String email) {

        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }
}

package com.example.ajit.sapnasscom.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.LetsLearnActivity;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

/**
 * Created by pratik on 2/21/2018.
 */

public class CourseDeatailFragment extends Fragment implements View.OnClickListener {
    public static TextView tvKeyFeaturesContent, tvCourseDetailContent, tvEnrolledButtonn;
    private Bundle bundle;
    private String courseId, description, keyfeatures;
    public static boolean enrolledValue, enrolledStatus;
    private String userId;
    private SessionManager sessionManager;
    WebView webView;
    public static TextView tv_features_header, tv_course_detail_header;
    LocaleClass localeClass;
    ProgressDialog pd;
    Intent intent;
    String LearnStatus, courseName, courseID;
    boolean enrolled;
    int Skillid;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.coruse_description_fragment, container, false);

        sessionManager = new SessionManager(getActivity());
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);

        Utilities.showLog("userid=" + userId);

        pd = new ProgressDialog(getActivity());
        getIntentId();
        init(v);
        getIntentValue();

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getActivity(), "CourseDeatailFragment");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return v;
    }

    private void getIntentId() {
        intent = getActivity().getIntent();
        if (intent != null) {
            //enrolled = intent.getStringExtra("enrolledValue");
            enrolled = intent.getBooleanExtra("enrolledValue", false);
            LearnStatus = intent.getStringExtra("LearnStatus");
            courseID = intent.getStringExtra("courseid");
            courseName = intent.getStringExtra("courseName");
            Skillid = intent.getIntExtra("Skillid", 0);
            sessionManager.createDesriiptionStatus(LearnStatus);
        }
    }

    private void init(View v) {
        tvKeyFeaturesContent = v.findViewById(R.id.tv_features_content);
        tvCourseDetailContent = v.findViewById(R.id.tv_course_detail_content);
        tvEnrolledButtonn = v.findViewById(R.id.tv_enroll_now);
        webView = v.findViewById(R.id.webView);
        tvEnrolledButtonn.setOnClickListener(this);

        tv_features_header = (TextView) v.findViewById(R.id.tv_features_course_header);
        tv_course_detail_header = (TextView) v.findViewById(R.id.tv_coursee_detail_header);
    }

    private void getIntentValue() {
        bundle = this.getArguments();
        courseId = bundle.getString("courseid");
        Utilities.showLog("jobId=" + courseId);
        keyfeatures = bundle.getString("keyfeatures");
        description = bundle.getString("description");
        enrolledValue = bundle.getBoolean("enrolledvalue");

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            tvKeyFeaturesContent.setText(Html.fromHtml(keyfeatures));
            webView.loadData(String.valueOf(Html.fromHtml(keyfeatures)), "text/html; charset=utf-8", "utf-8");
            webView.setBackgroundColor(Color.TRANSPARENT);
            tvCourseDetailContent.setText(Html.fromHtml(description));
        } else {
            webView.loadData(String.valueOf(Html.fromHtml(keyfeatures)), "text/html; charset=utf-8", "utf-8");
            webView.setBackgroundColor(Color.TRANSPARENT);
            tvKeyFeaturesContent.setText(Html.fromHtml(keyfeatures, Html.FROM_HTML_MODE_COMPACT));
            tvCourseDetailContent.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
        }

        if (enrolledValue) {
            if (LearnStatus.equalsIgnoreCase("Let learn")) {
                tvEnrolledButtonn.setText(R.string.start_course);
            } else if (LearnStatus.equalsIgnoreCase("Finish")) {
                tvEnrolledButtonn.setText(R.string.finishh);
            } else {
                tvEnrolledButtonn.setText(R.string.continue_course);
            }
            //tvEnrolledButtonn.setText(R.string.Enrolled);
        } else {
            tvEnrolledButtonn.setText(R.string.Enroll_Now);
        }
    }

    @Override
    public void onClick(View v) {
        if (v == tvEnrolledButtonn) {
            if (enrolledValue) {
                Log.e("Learn Status", "" + courseID + ", " + LearnStatus + ", " + enrolled + ", " + courseName);
                Intent intent = new Intent(getActivity(), LetsLearnActivity.class);
                intent.putExtra("courseid", courseID);
                intent.putExtra("LearnStatus", LearnStatus);
                intent.putExtra("enrolledValue", "" + enrolled);
                intent.putExtra("courseName", courseName);
                intent.putExtra("Skillid", Skillid);
                startActivity(intent);
            } else {
                if (Common.isInternet(getActivity())) {
                    new HttpAsyncTaskCourseEnrolled().execute(CommonUrl.setCourseEnrolled);
                } else {
                    noInternet(getActivity());
                }
            }
        }
    }

    private class HttpAsyncTaskCourseEnrolled extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourseEnrolled() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {

            return setEnrolled(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pd.dismiss();
            Utilities.showLog(result);
            if (enrolledStatus) {
                tvEnrolledButtonn.setText(R.string.start_course);
                Common.showApplyDialog(getActivity(), getString(R.string.enrolled));
            }
        }
    }

    public String setEnrolled(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            params.put("CourseID", courseId);
            params.put("Enrolled", "true");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        enrolledStatus = true;
                        enrolledValue = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

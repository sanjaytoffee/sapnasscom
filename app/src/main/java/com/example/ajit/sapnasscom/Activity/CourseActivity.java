package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.example.ajit.sapnasscom.Adapter.DiscoverCourseAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.DiscoverCourseModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by HSTPL on 3/7/2018.
 */

public class CourseActivity extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView rvCourse;
    DiscoverCourseAdapter discoverCourseAdapter;
    DiscoverCourseModel discoverCourseModel;
    ArrayList<DiscoverCourseModel> arrCourse;
    boolean enrolledStatus = false;
    SessionManager sessionManager;
    String userId;
    String courseId;
    double courseEnrolledId;
    ProgressDialog pd;
    Parcelable recyclerViewState;

    public void onDestroy() {
        super.onDestroy();
        CourseActivity.this.unregisterReceiver(enrolledApiCall);
    }

    public void onResume() {
        super.onResume();
        try {
            if (Common.dialog.isShowing()) {
                Common.dialog.dismiss();
            }
            if (pd.isShowing()) {
                pd.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        new HttpAsyncTask_Course().execute(CommonUrl.getDashboardData + "GetCourseByCourseId");
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_discover_course);

        CourseActivity.this.registerReceiver(enrolledApiCall, new IntentFilter("hitDiscoverEnrolledApi"));

        rvCourse = (RecyclerView) findViewById(R.id.rc_course_activity);

        sessionManager = new SessionManager(getApplicationContext());
        pd = new ProgressDialog(CourseActivity.this);

        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.Courses));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        setAdapter();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        courseId = getIntent().getStringExtra("CourseId");
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvCourse.setLayoutManager(mLayoutManager);
        rvCourse.setItemAnimator(new DefaultItemAnimator());
    }

    private class HttpAsyncTask_Course extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            arrCourse = new ArrayList<>();
            discoverCourseAdapter = new DiscoverCourseAdapter(CourseActivity.this, arrCourse);
            recyclerViewState = rvCourse.getLayoutManager().onSaveInstanceState();
        }

        protected String doInBackground(String... urls) {

            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            rvCourse.setAdapter(discoverCourseAdapter);
            rvCourse.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            pd.dismiss();
        }
    }

    public String getCourse(String url) {
        String result = "";
        Log.e("COURSEACTIVITY : ", url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", PrefUtils.getUserID(CourseActivity.this));
            params.put("CourseID", courseId);

            Log.e("PARAMS", String.valueOf(params));

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Log.e("DISCOVERCOURSE", response.toString());
                    enrolledStatus = false;
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        discoverCourseModel = new DiscoverCourseModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        discoverCourseModel.setId(jsonObject.getDouble("Id"));
                        Utilities.showLog("LogPrint" + String.valueOf(jsonObject.getDouble("Id")) + jsonObject.getString("CourseName"));
                        discoverCourseModel.setCourseName(jsonObject.getString("CourseName"));
                        discoverCourseModel.setDuration(jsonObject.getString("Duration"));
                        discoverCourseModel.setCityName(jsonObject.getString("CityName"));
                        discoverCourseModel.setCreator(jsonObject.getString("Creator"));
                        discoverCourseModel.setCreatedOn(jsonObject.getString("CreatedOn"));
                        discoverCourseModel.setLanguageName(jsonObject.getString("LanguageName"));
                        discoverCourseModel.setEnrolled(jsonObject.getBoolean("Enrolled"));
                        discoverCourseModel.setKeyFeature(jsonObject.getString("KeyFeature"));
                        discoverCourseModel.setDescription(jsonObject.getString("Description"));
                        discoverCourseModel.setIsFeedback(jsonObject.getString("IsFeedback"));
                        discoverCourseModel.setLearnStatus(jsonObject.getString("LearnStatus"));
                        discoverCourseModel.setSkillId(jsonObject.getInt("PrimarySkills"));

                        arrCourse.add(discoverCourseModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    BroadcastReceiver enrolledApiCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                courseEnrolledId = intent.getDoubleExtra("Discover_course_enrolled_id", 00);
                Utilities.showLog(String.valueOf(courseEnrolledId));
                new HttpAsyncTaskCourseEnrolled().execute(CommonUrl.setCourseEnrolled);
            }
        }
    };

    private class HttpAsyncTaskCourseEnrolled extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {

            return setEnrolled(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Utilities.showLog(result);
            if (enrolledStatus) {
                Common.showApplyDialog(CourseActivity.this, getString(R.string.enrolled));
                new HttpAsyncTask_Course().execute(CommonUrl.getDashboardData + "GetCourseByCourseId");
            }
        }
    }

    public String setEnrolled(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            Double d = new Double(courseEnrolledId);
            params.put("CourseID", String.valueOf(d.intValue()));
            params.put("Enrolled", "true");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                   /* JSONObject jsonoObject = new JSONObject(response.toString());
                    //status = jsonoObject.getString("Result");*/
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        enrolledStatus = true;
                    }
//                    rvCourse.setAdapter(courseAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}
package com.example.ajit.sapnasscom.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Common.Utility;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.example.ajit.sapnasscom.Utils.Utilities;
import com.squareup.picasso.Picasso;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

/**
 * Created by HSTPL on 2/21/2018.
 */

public class EditProfileShowDataActivity extends AppCompatActivity implements View.OnClickListener {

    private AlertDialog.Builder builder;
    Uri filePath;
    private int PICK_IMAGE_REQUEST = 1;
    public static int REQUEST_CAMERA = 100;
    Bitmap thumbnail, bm;
    public String userChoosenTask;
    String path, filename = "", encodedImage = "";
    private String picturePath = "", cvPath = "", Profile_path = "", Profile_name = "";

    Toolbar toolbar;
    public static TextView textViewEditText,tvInterst,tv_interest, textViewJobsAppliedText, textViewCourseEnrolledText, textViewPreferredLocationText, textViewCurrentLocationText, textViewSkillText, textViewLanguageText, textViewEducationText, textViewDastBoardText, tvEducation, tvLanguage, tvSkills, tvCurrentLocation, tvPreferredLocation, tvCourseApplied, tvJobsApplied, tvProfileName, tvProfileEmail;
    RelativeLayout editRelativeLayout;
    String langu = "";
    SessionManager sessionManager;
    String selectedLanguage, userEmail, userName, userId, educationValue, languageValue, interestValue,skillsValue, current_locationValue, preferred_locationValue, courseAppliedValue, jobsAppliedValue;
    CircleImageView circleImageView;
    LocaleClass localeClass;
    LinearLayout jobEnrolledLinear, courseEnrolledLinear;
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 0;
    TextView tvViewResume, tvResumeName;
    ProgressDialog pd;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile_show_activity);

        toolbar = (Toolbar) findViewById(R.id.profile_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        pd = new ProgressDialog(EditProfileShowDataActivity.this);
        sessionManager = new SessionManager(getApplicationContext());

        tvViewResume = (TextView) findViewById(R.id.tvViewResume);
        tvResumeName = (TextView) findViewById(R.id.tvResumeName);

        courseEnrolledLinear = (LinearLayout) findViewById(R.id.courseEnrolledLinear);
        jobEnrolledLinear = (LinearLayout) findViewById(R.id.jobEnrolledLinear);
        textViewDastBoardText = (TextView) findViewById(R.id.profile_dashboard_text);
        textViewEducationText = (TextView) findViewById(R.id.profile_education_text);
        textViewLanguageText = (TextView) findViewById(R.id.profile_language);
        textViewSkillText = (TextView) findViewById(R.id.profile_skill);
        textViewCurrentLocationText = (TextView) findViewById(R.id.profile_current_location);
        textViewPreferredLocationText = (TextView) findViewById(R.id.profile_Preferred_Location);
        textViewCourseEnrolledText = (TextView) findViewById(R.id.profile_Course_Enrolled);
        textViewJobsAppliedText = (TextView) findViewById(R.id.profile_Jobs_Applied);
        textViewEditText = (TextView) findViewById(R.id.profile_Edit);
        tvEducation = (TextView) findViewById(R.id.tv_profile_education);
        tvLanguage = (TextView) findViewById(R.id.tv_profile_language);
        tvSkills = (TextView) findViewById(R.id.tv_profile_skill);
        tvCurrentLocation = (TextView) findViewById(R.id.tv_profile_current_location);
        tvPreferredLocation = (TextView) findViewById(R.id.tv_profile_prefered_location);
        tvCourseApplied = (TextView) findViewById(R.id.tv_course_applied);
        tvJobsApplied = (TextView) findViewById(R.id.tv_job_applied);
        tvProfileName = (TextView) findViewById(R.id.profile_name);
        tvProfileEmail = (TextView) findViewById(R.id.profile_email);
        tvInterst = (TextView) findViewById(R.id.tvInterst);
        tv_interest = (TextView) findViewById(R.id.tv_interest);

        circleImageView = (CircleImageView) findViewById(R.id.profile_image);

        courseEnrolledLinear.setOnClickListener(this);
        jobEnrolledLinear.setOnClickListener(this);
        circleImageView.setOnClickListener(this);
        tvViewResume.setOnClickListener(this);

        HashMap<String, String> getmail = sessionManager.getEmail();
        HashMap<String, String> mail = sessionManager.getEmail();
        HashMap<String, String> name = sessionManager.getEmail();
        HashMap<String, String> langh = sessionManager.getLanguage();
        selectedLanguage = langh.get(SessionManager.KEY_LANGUAGE);

        userId = getmail.get(SessionManager.KEY_USER_ID);
        userEmail = mail.get(SessionManager.KEY_EMAIL);
        userName = name.get(SessionManager.KEY_USERNAME);

        tvProfileEmail.setText(PrefUtils.getUserEmail(EditProfileShowDataActivity.this));
        tvProfileName.setText(PrefUtils.getUserFullName(EditProfileShowDataActivity.this));

        editRelativeLayout = (RelativeLayout) findViewById(R.id.ed_profileEdit);
        editRelativeLayout.setOnClickListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (PrefUtils.getUser_Profileurl(EditProfileShowDataActivity.this).length() != 0) {
            Picasso.with(getApplicationContext()).load(PrefUtils.getUser_Profileurl(EditProfileShowDataActivity.this))
                    .placeholder(R.drawable.user).error(R.drawable.user)//http://192.168.1.241/DMS/ProfileImage/2d2dd35a-c3b6-423a-9650-20670c145941.jpg
                    .into(circleImageView);
        }

        if (PrefUtils.getUser_Profileurl(EditProfileShowDataActivity.this).length() != 0) {
            Picasso.with(getApplicationContext()).load(PrefUtils.getUser_Profileurl(EditProfileShowDataActivity.this))
                    .placeholder(R.drawable.user).error(R.drawable.user)//http://192.168.1.241/DMS/ProfileImage/2d2dd35a-c3b6-423a-9650-20670c145941.jpg
                    .into(circleImageView);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        new HttpAsyncTask_EditProfile().execute(CommonUrl.getEditFrofileData);
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "Profile Show");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profile_image: {
                checkPermission();
                selectImage();
            }
            break;

            case R.id.ed_profileEdit:
                if (Common.isInternet(EditProfileShowDataActivity.this)) {
                    Intent intent1 = new Intent(EditProfileShowDataActivity.this, ProfileDetailsActivity.class);
                    intent1.putExtra("editProfile", "editProfile");
                    startActivity(intent1);
                } else {
                    noInternet(EditProfileShowDataActivity.this);
                }
                break;

            case R.id.courseEnrolledLinear:
                Intent intent = new Intent(EditProfileShowDataActivity.this, MycourseActivity.class);
                startActivity(intent);
                break;

            case R.id.jobEnrolledLinear:
                Intent jobIntent = new Intent(EditProfileShowDataActivity.this, MyJobsActivity.class);
                startActivity(jobIntent);
                break;

            case R.id.tvViewResume:
                try {
                    Uri uri = Uri.parse(Profile_path);
                    Intent resumeIntent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(resumeIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "Resume is not found.", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    class HttpAsyncTask_EditProfile extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {
            return fetchProfileDetails(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                try {
                    if (!cvPath.equalsIgnoreCase("null")) {
                        Profile_path = CommonUrl.IMAGE + cvPath;
                        Profile_name = Profile_path.substring(Profile_path.lastIndexOf("\\") + 1);

                        String animals_list[] = Profile_name.split(",");
                        tvResumeName.setText(Profile_name);
                        tvResumeName.setTextColor(Color.BLUE);
                        tvResumeName.setPaintFlags(tvResumeName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    }

                    if (educationValue.equalsIgnoreCase("null")) {
                        tvEducation.setText("N/A");
                    } else {
                        tvEducation.setText(educationValue);
                    }

                    if (languageValue.equalsIgnoreCase("null")) {
                        tvLanguage.setText("N/A");
                    } else {
                        tvLanguage.setText(languageValue);
                    }

                    if (skillsValue.equalsIgnoreCase("null")) {
                        tvSkills.setText("N/A");
                    } else {
                        tvSkills.setText(skillsValue);
                    }

                    if (interestValue.equalsIgnoreCase("null")) {
                        tv_interest.setText("N/A");
                    } else {
                        tv_interest.setText(interestValue);
                    }

                    if (current_locationValue.equalsIgnoreCase("null")) {
                        tvCurrentLocation.setText("N/A");
                    } else {
                        tvCurrentLocation.setText(current_locationValue);
                    }

                    if (preferred_locationValue.equalsIgnoreCase("null")) {
                        tvPreferredLocation.setText("N/A");
                    } else {
                        tvPreferredLocation.setText(preferred_locationValue);
                    }

                    if (courseAppliedValue.equalsIgnoreCase("null")) {
                        tvCourseApplied.setText("N/A");
                    } else {
                        tvCourseApplied.setText(courseAppliedValue);
                    }

                    if (jobsAppliedValue.equalsIgnoreCase("null")) {
                        tvJobsApplied.setText("N/A");
                    } else {
                        tvJobsApplied.setText(jobsAppliedValue);
                    }
                    pd.dismiss();

                } catch (Exception e) {
                    e.printStackTrace();
                    pd.dismiss();
                }
            }
        }
    }

    public String fetchProfileDetails(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", userId);

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                Log.e("GETPROFILE", response.toString());
                try {
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());

                    educationValue = jsonObject.getString("Eudcation");
                    languageValue = jsonObject.getString("Languages");
                    skillsValue = jsonObject.getString("Skills");
                    current_locationValue = jsonObject.getString("CurrentLocation");
                    preferred_locationValue = jsonObject.getString("PreffredLocation");
                    courseAppliedValue = jsonObject.getString("CourseEnrolled");
                    jobsAppliedValue = jsonObject.getString("JobApplied");
                    cvPath = jsonObject.getString("CV");
                    interestValue = jsonObject.getString("SecondarySkills");

                    Log.e("languageValue", languageValue);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //set rotation according to picture
    private Bitmap rotate(Bitmap old, int rotation) {

        Matrix matrix = new Matrix();
        switch (rotation) {
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;

            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;

            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;

            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;

            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;

            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;

            case ExifInterface.ORIENTATION_NORMAL:
            default:
                break;
        }
        Bitmap newBitmap = Bitmap.createBitmap(old, 0, 0, old.getWidth(), old.getHeight(), matrix, true);
        old.recycle();
        return newBitmap;
    }

    private Bitmap bitmap_front;

    //set rotation for noughat
    private void setRotate(int roatation) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        if (roatation != 0) {
            Bitmap old = BitmapFactory.decodeFile(picturePath, bmOptions);
            bitmap_front = rotate(old, roatation);
        } else {
            bitmap_front = BitmapFactory.decodeFile(picturePath, bmOptions);
        }
        //getBase64(getImageUri(EditProfileShowDataActivity.this, bitmap_front));
        // CropingIMG(getImageUri(UploadImagesOnLine.this, bitmap_front));
//        performCrop(getImageUri(UploadImagesOnLine.this, bitmap_front), RESULT_LOAD_CROP);
//        startCropImageActivity(getImageUri(UploadImagesOnLine.this, bitmap_front));
    }

    private void selectImage() {
        final CharSequence[] items = {"Take Photo",/*, "Choose from Library",*/
                "Cancel"};

        builder = new AlertDialog.Builder(EditProfileShowDataActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(EditProfileShowDataActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                }
//                else if (items[item].equals("Choose from Library")) {
//                    userChoosenTask = "Choose from Library";
//                    if (result)
//                        showFileChooser();
//                }
                else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            bm = null;
            if (data != null) {
                filePath = data.getData();
                try {
                    bm = MediaStore.Images.Media.getBitmap(EditProfileShowDataActivity.this.getContentResolver(), data.getData());
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    Bitmap.createScaledBitmap(bm, 150, 106, true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Uri tempUri = getImageUri(EditProfileShowDataActivity.this, bm);
            File finalFile = new File(getRealPathFromURI(tempUri));
            path = finalFile.getAbsolutePath();
            filename = path.substring(path.lastIndexOf("/") + 1);
//            Log.e("Gallery : ", "path : " + path + "filename : " + filename);
            builder.setView(0);
            getStringImage(bm);
            // uploadImage(bm);
            // profileImage.setImageBitmap(bm);

        } else if (requestCode == REQUEST_CAMERA)
            onCaptureImageResult(data);
    }

    private void onCaptureImageResult(Intent data) {

        try {
            thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
            Bitmap.createScaledBitmap(thumbnail, 150, 106, true);
            final File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Uri tempUri = getImageUri(EditProfileShowDataActivity.this, thumbnail);
            File finalFile = new File(getRealPathFromURI(tempUri));
            path = finalFile.getAbsolutePath();
            filename = path.substring(path.lastIndexOf("/") + 1);
//            Log.e("CAMERA : ", "path : " + path + "filename : " + filename);
            //uploadImage(thumbnail);
            // profileImage.setImageBitmap(thumbnail);
            builder.setView(0);
            getStringImage(thumbnail);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        new HttpAsyncTask_SetProfilePick().execute(CommonUrl.setUSerImage);
        return encodedImage;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = EditProfileShowDataActivity.this.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    class HttpAsyncTask_SetProfilePick extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {
            return setProfileImage(urls[0]);
        }

        protected void onPostExecute(String result) {

            if (result != null) {
                Toast.makeText(EditProfileShowDataActivity.this, "Profile picture has been updated successfully.", Toast.LENGTH_SHORT).show();
                Utilities.showLog("imageUrl='" + PrefUtils.getUser_Profileurl(EditProfileShowDataActivity.this) + "'");
                Picasso.with(getApplicationContext()).load(PrefUtils.getUser_Profileurl(EditProfileShowDataActivity.this))
                        .placeholder(R.drawable.user).error(R.drawable.user)
                        .into(circleImageView);
            }
            pd.dismiss();
        }
    }

    public String setProfileImage(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", userId);
            params.put("Image", encodedImage);
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {
                try {
                    Utilities.showLog(response.toString());
                    Log.e("IMAGEUPLOAD", response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    String imageUrl = jsonObject.getString("image");
                    Utilities.showLog(imageUrl);
                    Utilities.showLog("url=" + CommonUrl.IMAGE + jsonObject.getString("image"));
                    PrefUtils.setUser_Profileurl(EditProfileShowDataActivity.this, CommonUrl.IMAGE + jsonObject.getString("image"));
                    Utilities.showLog(PrefUtils.getUser_Profileurl(EditProfileShowDataActivity.this));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @SuppressLint("NewApi")
    public void checkPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
//            permissionsNeeded.add("GPS");
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
//            permissionsNeeded.add("Network location");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @SuppressLint("NewApi")
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
//                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
//                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (
//                        perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
//                        && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
//                        &&
                        perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(EditProfileShowDataActivity.this, R.style.MyDialogTheme)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();

    }
}

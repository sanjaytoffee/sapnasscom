package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.ProfileDetailsActivity;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class DropDownListAdapter extends BaseAdapter {

    private ArrayList<Skills> mListItems;
    private LayoutInflater mInflater;
    private TextView mSelectedItems;
    public static int selectedCount = 0;
    private static String firstSelected = "";
    private ViewHolder holder;
    private static String selected = "";
    public JSONArray jsonArray;

    //shortened selected values representation

    public static String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {

        DropDownListAdapter.selected = selected;
    }

    public DropDownListAdapter(Context context, ArrayList<Skills> items, TextView tv, JSONArray jsonArray_skill) {
        mListItems = new ArrayList<Skills>();
        mListItems.addAll(items);
        mInflater = LayoutInflater.from(context);
        mSelectedItems = tv;
        this.jsonArray = jsonArray_skill;

        //selectedCount=0;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mListItems.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.drop_down_list_row, null);
            holder = new ViewHolder();
            holder.tv = (TextView) convertView.findViewById(R.id.SelectOption);
            holder.chkbox = (CheckBox) convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv.setText("" + mListItems.get(position).getName());

        final int position1 = position;

        //whenever the checkbox is clicked the selected values textview is updated with new selected values
        holder.chkbox.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setText(position1);
            }
        });

        if (ProfileDetailsActivity.checkSelected[position]) {
            holder.chkbox.setChecked(true);
        } else
            holder.chkbox.setChecked(false);
        try {
            if (jsonArray.length() != 0) {

                for (int k = 0; k < jsonArray.length(); k++) {
                    try {
                        if (mListItems.get(position).getID().equalsIgnoreCase(jsonArray.get(k).toString())) {
                            holder.chkbox.setChecked(true);
                            selectedCount++;
                            if (holder.chkbox.isChecked()) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    jsonArray.remove(k);

                                }
                                ProfileDetailsActivity.checkSelected[position] = true;
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    /*
     * Function which updates the selected values display and information(checkSelected[])
     * */
    private void setText(int position1) {
        if (!ProfileDetailsActivity.checkSelected[position1]) {

            ProfileDetailsActivity.checkSelected[position1] = true;
            selectedCount++;
        } else {
            ProfileDetailsActivity.checkSelected[position1] = false;
            selectedCount--;
        }
        if (selectedCount == -1) {
            selectedCount = ProfileDetailsActivity.skillList.size() - 1;

        }
        if (selectedCount == 0) {
            mSelectedItems.setText("Select");
        } else if (selectedCount == 1) {
            for (int i = 0; i < ProfileDetailsActivity.checkSelected.length; i++) {
                if (ProfileDetailsActivity.checkSelected[i] == true) {
                    firstSelected = "" + mListItems.get(i).getName();
                    break;
                }
            }

            mSelectedItems.setText(firstSelected);
            setSelected(firstSelected);
        } else if (selectedCount > 1) {

            for (int i = 0; i < ProfileDetailsActivity.checkSelected.length; i++) {

                if (ProfileDetailsActivity.checkSelected[i] == true) {
                    firstSelected = "" + mListItems.get(i).getName();
                    break;
                }
            }

            mSelectedItems.setText(firstSelected + " & " + (selectedCount - 1) + " more");
            setSelected(firstSelected + " & " + (selectedCount - 1) + " more");
        }
        Log.e("SELECTED", "" + selectedCount);
    }

    private class ViewHolder {
        TextView tv;
        CheckBox chkbox;
    }
}
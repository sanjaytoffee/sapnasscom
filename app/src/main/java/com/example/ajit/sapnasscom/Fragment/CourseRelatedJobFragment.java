package com.example.ajit.sapnasscom.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.ajit.sapnasscom.Adapter.CourseRelatedJobsAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.JobModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class CourseRelatedJobFragment extends Fragment {

    public CourseRelatedJobFragment() {
    }

    private String userId, status;
    private RecyclerView rvCourse;
    private SessionManager sessionManager;
    private ArrayList<JobModel> arrJobs;
    private CourseRelatedJobsAdapter courseRelatedJobsAdapter;
    private double jobAppliedId;
    private boolean AppliedStatus = false;
    LocaleClass localeClass;
    ProgressDialog pd;
    Parcelable recyclerViewState;
    int Skillid;
    RelativeLayout header_view,rl_no_data_found;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(appliedJobApiCall, new IntentFilter("course_related_hitApplyjobApi"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(appliedJobApiCall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_job, container, false);

        Bundle bundle = this.getArguments();
        Skillid = bundle.getInt("Skillid");

        Log.e("Skillid", "" + Skillid);
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getActivity(), "CourseRelatedJobFragment");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        pd = new ProgressDialog(getActivity());
        init(v);
        setAdapter();

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Common.isInternet(getActivity())) {
            new CourseRelatedJobFragment.HttpAsyncTaskgetJobs().execute(CommonUrl.getCourseRelatedJobs);
        } else {
            noInternet(getActivity());
        }

        try {
            if (Common.dialog.isShowing()) {
                Common.dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void init(View v) {
        rvCourse = (RecyclerView) v.findViewById(R.id.rc_course);
        rl_no_data_found = (RelativeLayout) v.findViewById(R.id.rl_no_data_found);
        header_view = (RelativeLayout) v.findViewById(R.id.header_view);
        header_view.setVisibility(View.GONE);
        arrJobs = new ArrayList<>();
        sessionManager = new SessionManager(getActivity());
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        Utilities.showLog("userid=" + userId);
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCourse.setLayoutManager(mLayoutManager);
        rvCourse.setItemAnimator(new DefaultItemAnimator());
    }

    private class HttpAsyncTaskgetJobs extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskgetJobs() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
            arrJobs = new ArrayList<>();
            courseRelatedJobsAdapter = new CourseRelatedJobsAdapter(getActivity(), arrJobs);
            recyclerViewState = rvCourse.getLayoutManager().onSaveInstanceState();
        }

        protected String doInBackground(String... urls) {

            return getJobs(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (arrJobs.size() == 0) {
                rl_no_data_found.setVisibility(View.VISIBLE);
            } else {
                rvCourse.setAdapter(courseRelatedJobsAdapter);
                rvCourse.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            }
            pd.dismiss();
            Utilities.showLog(result);
        }
    }

    public String getJobs(String url) {
        String result = "";
        System.out.print("CourseRelatedJobFragment : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Id", userId);
            params.put("SkillsDetails", Skillid);

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JobModel jobModel = new JobModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        jobModel.setId(jsonObject.getDouble("Id"));
                        jobModel.setProfile(jsonObject.getString("Profile"));
                        jobModel.setComapnyName(jsonObject.getString("ComapnyName"));
                        jobModel.setExperinceMin(jsonObject.getInt("ExperinceMin"));
                        jobModel.setExperinceMax(jsonObject.getInt("ExperinceMax"));
                        jobModel.setPrimarySkillsName(jsonObject.getString("PrimarySkillsName"));
                        jobModel.setCityName(jsonObject.getString("CityName"));
                        jobModel.setCreatedOn(jsonObject.getString("CreatedOn"));//Applied
                        jobModel.setApplied(jsonObject.getBoolean("Applied"));
                        jobModel.setLanguageName(jsonObject.getString("LanguageName"));
                        jobModel.setDescription(jsonObject.getString("Description"));
                        jobModel.setPackageMin(jsonObject.getString("PackageMin"));
                        jobModel.setPackageMax(jsonObject.getString("PackageMax"));//IndustryName
                        jobModel.setIndustryName(jsonObject.getString("IndustryName"));
                        jobModel.setSkillId(jsonObject.getInt("PrimarySkills"));

                        arrJobs.add(jobModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    BroadcastReceiver appliedJobApiCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                jobAppliedId = intent.getDoubleExtra("jobApplied_id", 00);
                Utilities.showLog(String.valueOf(jobAppliedId));
                new CourseRelatedJobFragment.HttpAsyncTaskJobApplied().execute(CommonUrl.setJobApplied);
            }

        }
    };

    private class HttpAsyncTaskJobApplied extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskJobApplied() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {

            return setJobApplied(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Utilities.showLog(result);
            if (AppliedStatus) {
                Common.showApplyDialog(getActivity(), getString(R.string.apply));
                new CourseRelatedJobFragment.HttpAsyncTaskgetJobs().execute(CommonUrl.getCourseRelatedJobs);
            }
        }
    }

    public String setJobApplied(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            Double d = new Double(jobAppliedId);
            params.put("JobId", String.valueOf(d.intValue()));
            params.put("Applied", "true");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        AppliedStatus = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

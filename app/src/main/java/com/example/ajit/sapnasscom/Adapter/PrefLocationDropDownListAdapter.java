package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.ProfileDetailsActivity;
import com.example.ajit.sapnasscom.Model.PreferedLocation;
import com.example.ajit.sapnasscom.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class PrefLocationDropDownListAdapter extends BaseAdapter {

    private ArrayList<PreferedLocation> mListItems2;
    private LayoutInflater mInflater2;
    private TextView mSelectedItems2;
    public static int selectedCount2 = 0;
    private static String firstSelected2 = "";
    private ViewHolder holder;
    JSONArray jsonArray;
    private static String selected2 = "";    //shortened selected values representation


    public static String getSelected() {
        return selected2;
    }

    public void setSelected(String selected) {
        PrefLocationDropDownListAdapter.selected2 = selected;
    }

    public PrefLocationDropDownListAdapter(Context context, ArrayList<PreferedLocation> items,
                                           TextView tv2, JSONArray jsonArray) {
        mListItems2 = new ArrayList<PreferedLocation>();
        mListItems2.addAll(items);
        mInflater2 = LayoutInflater.from(context);
        mSelectedItems2 = tv2;
        this.jsonArray = jsonArray;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mListItems2.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (convertView == null) {
            convertView = mInflater2.inflate(R.layout.prelocationdrop_down_list_row, null);
            holder = new ViewHolder();
            holder.tv = (TextView) convertView.findViewById(R.id.pre_SelectOption);
            holder.chkbox = (CheckBox) convertView.findViewById(R.id.pre_checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv.setText("" + mListItems2.get(position).getName());

        final int position1 = position;

        //whenever the checkbox is clicked the selected values textview is updated with new selected values
        holder.chkbox.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setText(position1);
            }
        });

        if (ProfileDetailsActivity.checkSelected2[position])
            holder.chkbox.setChecked(true);
        else
            holder.chkbox.setChecked(false);

        try {
            if (jsonArray.length() != 0) {
                for (int k = 0; k < jsonArray.length(); k++) {
                    try {
                        if (mListItems2.get(position).getId().equalsIgnoreCase(jsonArray.get(k).toString())) {
                            holder.chkbox.setChecked(true);
                            selectedCount2++;
                            if(holder.chkbox.isChecked()) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    jsonArray.remove(k);
                                }
                                ProfileDetailsActivity.checkSelected2[position] = true;

                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private void setText(int position1) {

        if (!ProfileDetailsActivity.checkSelected2[position1]) {
            ProfileDetailsActivity.checkSelected2[position1] = true;
            selectedCount2++;
        } else {
            ProfileDetailsActivity.checkSelected2[position1] = false;
            selectedCount2--;
        }
        if (selectedCount2==-1) {
            selectedCount2=ProfileDetailsActivity.preferedList.size()-1;

        }
        if (selectedCount2 == 0) {
            mSelectedItems2.setText("Select");
        } else if (selectedCount2 == 1) {
            for (int i = 0; i < ProfileDetailsActivity.checkSelected2.length; i++) {
                if (ProfileDetailsActivity.checkSelected2[i] == true) {
                    firstSelected2 = "" + mListItems2.get(i).getName();
                    break;
                }
            }
            mSelectedItems2.setText(firstSelected2);
            setSelected(firstSelected2);
        } else if (selectedCount2 > 1) {
            for (int i = 0; i < ProfileDetailsActivity.checkSelected2.length; i++) {
                if (ProfileDetailsActivity.checkSelected2[i] == true) {
                    firstSelected2 = "" + mListItems2.get(i).getName();
                    break;
                }
            }
            mSelectedItems2.setText(firstSelected2 + " & " + (selectedCount2 - 1) + " more");
            setSelected(firstSelected2 + " & " + (selectedCount2 - 1) + " more");
        }
    }

    private class ViewHolder {
        TextView tv;
        CheckBox chkbox;
    }
}
package com.example.ajit.sapnasscom.Fragment;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class JobDeatailFragment extends Fragment implements View.OnClickListener {

    public static TextView tvKeyFeaturesContent, tvJobDetailContent, tvApplyButton, tvHeaderTitelDescription, tvSalary, tv_industury_header, tvIndustry, tv_salarys_header;
    private Bundle bundle;
    private String description, keyfeatures, companyName, profileName, salary, industry;
    public static boolean ApplyVAlue, applyStatus;
    private String userId;
    private SessionManager sessionManager;
    private String jobId;
    LocaleClass localeClass;
    ProgressDialog pd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.job_description_fragment, container, false);

        pd = new ProgressDialog(getActivity());
        init(v);
        getIntent();

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getActivity(), "JobDeatailFragment");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return v;
    }

    private void getIntent() {
        //within OncreateView
        bundle = this.getArguments();

        jobId = bundle.getString("jobid");
        Utilities.showLog("jobId=" + jobId);
        description = bundle.getString("description");
        Utilities.showLog("jobId=" + jobId + " descriptionFragment=" + description);
        ApplyVAlue = bundle.getBoolean("applyvalue");
        companyName = bundle.getString("company_name");
        profileName = bundle.getString("profile_name");//salary/industury
        salary = bundle.getString("salary");//salary/industury
        industry = bundle.getString("industury");//salary/industury
        tvHeaderTitelDescription.setText(R.string.Job_Details);
        tvSalary.setText(salary);
        tvIndustry.setText(industry);



        Log.e("ApplyVAlue", "" + ApplyVAlue);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            //  tvKeyFeaturesContent.setText(Html.fromHtml(keyfeatures));
            if (description == null || description == "" || description == "null") {
                tvJobDetailContent.setText(Html.fromHtml("N/A"));
            } else {
                tvJobDetailContent.setText(Html.fromHtml(description));
            }

            //tvJobDetailContent.setText(Html.fromHtml(description));
        } else {
            if (description == null || description == "" || description == "null") {
                //  tvJobDetailContent.setText(Html.fromHtml("N/A"));
                tvJobDetailContent.setText(Html.fromHtml("N/A", Html.FROM_HTML_MODE_COMPACT));
            } else {
                tvJobDetailContent.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
            }
            //tvJobDetailContent.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
        }

        if (ApplyVAlue) {
            tvApplyButton.setText(R.string.Applied);
        } else {
            tvApplyButton.setText(R.string.Apply_Now);
        }
    }

    private void init(View v) {
        sessionManager = new SessionManager(getActivity());
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        Utilities.showLog("userid=" + userId);
        tvHeaderTitelDescription = (TextView) v.findViewById(R.id.tv_course_detail_header);
        tvKeyFeaturesContent = (TextView) v.findViewById(R.id.tv_features_content);
        tvJobDetailContent = (TextView) v.findViewById(R.id.tv_course_detail_content);
        tvIndustry = (TextView) v.findViewById(R.id.tv_industry_content);
        tvSalary = (TextView) v.findViewById(R.id.tv_salary_content);
        tvApplyButton = (TextView) v.findViewById(R.id.tv_apply_now);
        tv_salarys_header = (TextView) v.findViewById(R.id.tv_salarys_header);
        tv_industury_header = (TextView) v.findViewById(R.id.tv_industury_header);

        tvApplyButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == tvApplyButton) {
            if (ApplyVAlue) {
                Common.showApplyDialog(getActivity(), getString(R.string.applyalready));
            } else {
                if (Common.isInternet(getActivity())) {
                    new JobDeatailFragment.HttpAsyncTaskCourseEnrolled().execute(CommonUrl.setJobApplied);
                } else {
                    noInternet(getActivity());
                }

            }
        }
    }

    private class HttpAsyncTaskCourseEnrolled extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourseEnrolled() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {

            return setEnrolled(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pd.dismiss();
            Utilities.showLog(result);
            if (applyStatus) {
                tvApplyButton.setText(R.string.Applied);
                Common.showApplyDialog(getActivity(), getString(R.string.apply));
            }
        }
    }

    public String setEnrolled(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            /*
            params.put("UserId", userId);
            Double d = new Double( jobAppliedId);
            params.put("JobId", String.valueOf(d.intValue()));
            params.put("Applied", "true");
             */
            params.put("UserId", userId);
            params.put("JobId", jobId);
            params.put("Applied", "true");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                   /* JSONObject jsonoObject = new JSONObject(response.toString());
                    //status = jsonoObject.getString("Result");*/
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        applyStatus = true;
                        ApplyVAlue = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

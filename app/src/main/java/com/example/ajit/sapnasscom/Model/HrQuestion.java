package com.example.ajit.sapnasscom.Model;

/**
 * Created by DTP-110 on 1/27/2017.
 */

public class HrQuestion {

    private String ID;
    private String Code;
    private String description;
    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return description;
    }

    public void setName(String name) {
        description = name;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


}

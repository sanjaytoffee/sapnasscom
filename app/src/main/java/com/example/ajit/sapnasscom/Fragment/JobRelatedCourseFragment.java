package com.example.ajit.sapnasscom.Fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.ajit.sapnasscom.Adapter.JobRelatedCourseAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.CourseModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;


public class JobRelatedCourseFragment extends Fragment {

    public JobRelatedCourseFragment() {
    }

    private double courseEnrolledId;
    private String userId, status;
    private RecyclerView rvCourse;
    private SessionManager sessionManager;
    private CourseModel courseModel;
    private ArrayList<CourseModel> arrCourse;
    private JobRelatedCourseAdapter courseAdapter;
    private boolean enrolledStatus = false;
    ProgressDialog pd;
    RelativeLayout header_view,rl_no_data_found;
    Parcelable recyclerViewState;
    int SkillId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(enrolledApiCall, new IntentFilter("hitEnrolledApi"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(enrolledApiCall);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_course, container, false);

        pd = new ProgressDialog(getActivity());
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            SkillId = bundle.getInt("SkillId", 0);
        }

        init(v);
        setAdapter();

        return v;
    }

    private void init(View v) {
        rvCourse = (RecyclerView) v.findViewById(R.id.rc_course);
        rl_no_data_found = (RelativeLayout) v.findViewById(R.id.rl_no_data_found);
        header_view = (RelativeLayout) v.findViewById(R.id.header_view);
        header_view.setVisibility(View.GONE);
        arrCourse = new ArrayList<>();
        sessionManager = new SessionManager(getActivity());
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        Utilities.showLog("userid=" + userId);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Common.isInternet(getActivity())) {
            new HttpAsyncTaskCourse().execute(CommonUrl.getJobRelatedCourse);
        } else {
            noInternet(getActivity());
        }

        try {
            if (Common.dialog.isShowing()) {
                Common.dialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCourse.setLayoutManager(mLayoutManager);
        rvCourse.setItemAnimator(new DefaultItemAnimator());
    }

    private class HttpAsyncTaskCourse extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourse() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
            arrCourse = new ArrayList<>();
            courseAdapter = new JobRelatedCourseAdapter(getActivity(), arrCourse);
            recyclerViewState = rvCourse.getLayoutManager().onSaveInstanceState();
        }

        protected String doInBackground(String... urls) {

            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (arrCourse.size() == 0) {
                rl_no_data_found.setVisibility(View.VISIBLE);
            } else {
                rvCourse.setAdapter(courseAdapter);
                rvCourse.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            }

            Utilities.showLog(result);
            pd.dismiss();
        }
    }

    public String getCourse(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("id", userId);
            params.put("SkillsDetails", SkillId);

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    enrolledStatus = false;
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        CourseModel courseModel = new CourseModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        courseModel.setId(jsonObject.getDouble("Id"));
                        Utilities.showLog("LogPrint" + String.valueOf(jsonObject.getDouble("Id")) + jsonObject.getString("CourseName"));
                        courseModel.setCourseName(jsonObject.getString("CourseName"));
                        courseModel.setDuration(jsonObject.getString("Duration"));
                        courseModel.setCityName(jsonObject.getString("CityName"));
                        courseModel.setCreator(jsonObject.getString("Creator"));
                        courseModel.setCreatedOn(jsonObject.getString("CreatedOn"));
                        courseModel.setLanguageName(jsonObject.getString("LanguageName"));
                        courseModel.setEnrolled(jsonObject.getBoolean("Enrolled"));
                        courseModel.setKeyFeature(jsonObject.getString("KeyFeature"));
                        courseModel.setDescription(jsonObject.getString("Description"));
                        courseModel.setLearnStatus(jsonObject.getString("LearnStatus"));
                        courseModel.setSkillId(jsonObject.getInt("PrimarySkills"));

                        arrCourse.add(courseModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    BroadcastReceiver enrolledApiCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                courseEnrolledId = intent.getDoubleExtra("course_enrolled_id", 00);
                Utilities.showLog(String.valueOf(courseEnrolledId));
                new HttpAsyncTaskCourseEnrolled().execute(CommonUrl.setCourseEnrolled);
            }
        }
    };

    private class HttpAsyncTaskCourseEnrolled extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourseEnrolled() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {
            return setEnrolled(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Utilities.showLog(result);
            if (enrolledStatus) {
                Common.showApplyDialog(getActivity(), getString(R.string.enrolled));
                new HttpAsyncTaskCourse().execute(CommonUrl.getJobRelatedCourse);
            }

        }
    }

    public String setEnrolled(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            Double d = new Double(courseEnrolledId);
            params.put("CourseID", String.valueOf(d.intValue()));
            params.put("Enrolled", "true");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        enrolledStatus = true;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Common.GridViewScrollable;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

public class CustomAdapter extends BaseAdapter {
    private int previousSelectedPosition = -1;

    ArrayList<Skills> result;
    Context context;
    int[] imageId;
    private static LayoutInflater inflater = null;

    public CustomAdapter(Context context, ArrayList<Skills> osNameList) {
        // TODO Auto-generated constructor stub
        result = osNameList;
        this.context = context;
        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView os_text;
        GridViewScrollable secondry_grid;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Holder holder = new Holder();
        final View rowView;

        rowView = inflater.inflate(R.layout.type_text_view, null);
        holder.os_text = (TextView) rowView.findViewById(R.id.os_texts);
        holder.secondry_grid = (GridViewScrollable) rowView.findViewById(R.id.secondry_grid);

        holder.os_text.setText(" " + result.get(position).getName() + "" + "");

        if (result.get(position).getSecondryList().size() != 0) {
            holder.secondry_grid.setAdapter(new Secondry_adapter(context, result.get(position).getSecondryList()));
        }

        return rowView;
    }

}
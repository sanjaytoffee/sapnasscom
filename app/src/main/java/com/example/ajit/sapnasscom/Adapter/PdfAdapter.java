package com.example.ajit.sapnasscom.Adapter;


import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Activity.DocumentActivity;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.PdfModel;
import com.example.ajit.sapnasscom.R;

import java.util.List;

public class PdfAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<PdfModel> trainerList;
    private Context context;
    SessionManager sessionManager;

    private boolean isLoadingAdded = false;

    public PdfAdapter(Context context, List<PdfModel> arrPdf) {
        this.context = context;
        this.trainerList = arrPdf;
        sessionManager = new SessionManager(context);
    }

    public List<PdfModel> getMovies() {
        return trainerList;
    }

    public void setMovies(List<PdfModel> trainerList) {
        this.trainerList = trainerList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                // View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = getViewHolder(parent, inflater);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.row_start_learning, parent, false);
        viewHolder = new MovieVH(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {


        switch (getItemViewType(position)) {
            case ITEM:
                final MovieVH movieVH = (MovieVH) holder;


                movieVH.tvPdfName.setText( trainerList.get(position).getName());

                sessionManager.createActive("" +  trainerList.get(position).isActive());

                if ( trainerList.get(position).isActive()) {
                   if (movieVH.tvRead.getText().toString().equalsIgnoreCase(context.getResources().getString(R.string.download))) {
                        sessionManager.createDownload(context.getResources().getString(R.string.download));
                        movieVH.tvRead.setText(context.getResources().getString(R.string.download));
                        trainerList.get(position).setStatus(context.getResources().getString(R.string.download));
                    } else {
                        movieVH.tvRead.setText(context.getResources().getString(R.string.read));
                        trainerList.get(position).setStatus(context.getResources().getString(R.string.read));
                    }
                } else {
                    movieVH.tvRead.setText(context.getResources().getString(R.string.view));
                    trainerList.get(position).setStatus(context.getResources().getString(R.string.view));
                }


                movieVH.tvRead.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (trainerList.get(position).getStatus().equalsIgnoreCase(context.getResources().getString(R.string.download))) {
                            trainerList.get(position).setStatus(context.getResources().getString(R.string.read));
                            movieVH.tvRead.setText(context.getResources().getString(R.string.read));
                            changeText( trainerList.get(position),movieVH,position);
                            Intent intent = new Intent();
                            intent.setAction("pdf");
                            intent.putExtra("pdf", "download");
                            intent.putExtra("pdfUrl",  trainerList.get(position).getCode());
                            intent.putExtra("pdfName",  trainerList.get(position).getName());
                            intent.putExtra("pdfId",  trainerList.get(position).getID());
                            context.sendBroadcast(intent);

                            notifyDataSetChanged();
                        } else {
                            if ( trainerList.get(position).isActive()) {
                                Intent intent = new Intent();
                                intent.setAction("pdf");
                                intent.putExtra("pdf", "read");
                                intent.putExtra("pdfUrl",  trainerList.get(position).getCode());
                                intent.putExtra("pdfName",  trainerList.get(position).getName());
                                intent.putExtra("pdfId",  trainerList.get(position).getID());
                                context.sendBroadcast(intent);
                            } else {
                                try {
                                    Uri uri = Uri.parse( trainerList.get(position).getCode().toLowerCase());
//                            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
//                            context.startActivity(intent);
                                    Intent intent = new Intent(context, DocumentActivity.class);
                                    intent.putExtra("Code", "" + uri);
                                    context.startActivity(intent);
                                } catch (ActivityNotFoundException e) {
                                    Toast.makeText(context, "Address is not found.", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                });
                break;
            case LOADING:
//                Do nothing
                break;
        }

    }
    private void changeText(PdfModel  pdfModel, MovieVH holder, int position) {
        if ( pdfModel.isActive()) {
            if (trainerList.get(position).getStatus().equalsIgnoreCase(context.getResources().getString(R.string.download))) {
                //   sessionManager.createDownload("Download");
                holder.tvRead.setText(context.getResources().getString(R.string.download));
                trainerList.get(position).setStatus(context.getResources().getString(R.string.download));
            } else {
                holder.tvRead.setText(context.getResources().getString(R.string.read));
                trainerList.get(position).setStatus(context.getResources().getString(R.string.read));
            }
        } else {
            holder.tvRead.setText(context.getResources().getString(R.string.view));
            trainerList.get(position).setStatus(context.getResources().getString(R.string.view));
        }
    }

    @Override
    public int getItemCount() {
        return trainerList == null ? 0 : trainerList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == trainerList.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(PdfModel mc) {
        trainerList.add(mc);
        notifyItemInserted(trainerList.size() - 1);
    }

    public void addAll(List<PdfModel> mcList) {
        for (PdfModel mc : mcList) {
            add(mc);
        }
    }

    public void remove(PdfModel city) {
        int position = trainerList.indexOf(city);
        if (position > -1) {
            trainerList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new PdfModel());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = trainerList.size() - 1;
        PdfModel item = getItem(position);

        if (item != null) {
            trainerList.remove(position);
            notifyItemRemoved(position);
        }
    }

    public PdfModel getItem(int position) {
        return trainerList.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    public   class MovieVH extends RecyclerView.ViewHolder {
        public  TextView tvPdfName, tvRead, tvCourseName, tvDate, tvCreatedby, tvDuration, tvLocation, tvLanguage, tvEnrollNow, tvCourseDescription;

        public MovieVH(View view) {
            super(view);
            tvRead = (TextView) view.findViewById(R.id.tv_read);
            tvPdfName = (TextView) view.findViewById(R.id.tv_name_pdf);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


}
package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Common.GridViewScrollable;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;






public class EditCustomAdapter extends BaseAdapter{
    private int previousSelectedPosition = -1;

    ArrayList<Skills> result;
    Context context;
    int [] imageId;
    ViewHolder holder;
    ArrayList<Skills> edit_skill_array;
    private static LayoutInflater inflater=null;
    public EditCustomAdapter(Context context, ArrayList<Skills> osNameList, ArrayList<Skills> edit_skill_array) {
        // TODO Auto-generated constructor stub
        result=osNameList;
        this.context=context;
        this.edit_skill_array=edit_skill_array;
        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class ViewHolder
    {
        TextView os_text;
        public GridViewScrollable secondry_grid;
        int position;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View rowView = null;


        if (rowView == null) {
            holder = new ViewHolder();
            rowView = inflater.inflate(R.layout.type_text_view, null);
            holder.os_text =(TextView) rowView.findViewById(R.id.os_texts);
            holder.secondry_grid =(GridViewScrollable) rowView.findViewById(R.id.secondry_grid);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        holder.position = position;
        holder.os_text.setText(""+result.get(position).getName()+""+"  (Select any 3)");



        if(result.get(position).getSecondryList().size()!=0) {
            holder.secondry_grid.setAdapter(new EditSecondryAdapter(context, result.get(position).getSecondryList()));
        }


        holder.secondry_grid.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
        return rowView;
    }


}

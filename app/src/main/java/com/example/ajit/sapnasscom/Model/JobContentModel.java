package com.example.ajit.sapnasscom.Model;

/**
 * Created by HSTPL on 2/21/2018.
 */

public class JobContentModel {
    String Title;
    String Primaryskills;
    String Content;
    String Author;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getPrimaryskills() {
        return Primaryskills;
    }

    public void setPrimaryskills(String primaryskills) {
        Primaryskills = primaryskills;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }
}

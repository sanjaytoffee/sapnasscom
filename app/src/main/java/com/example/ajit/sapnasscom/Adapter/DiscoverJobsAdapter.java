package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.JobDetailsActivity;
import com.example.ajit.sapnasscom.Activity.MyJobsActivity;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Model.DiscoverJobModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import java.util.List;

/**
 * Created by pratik on 12/7/2017.
 */

public class DiscoverJobsAdapter extends RecyclerView.Adapter<DiscoverJobsAdapter.MyViewHolder> {

    private List<DiscoverJobModel> arrJobsList;
    private Context context;
    LocaleClass localeClass;
    int count = 0;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tvProfile, tvCompanyName, tvSkills, tvExperience, tvDate, tvCreatedby, tvDuration, tvJobDuration, tvLocation, tvLanguage, tvApplyNow, tvJobDescription;
        private ImageView ivSite;

        public MyViewHolder(View view) {
            super(view);
            tvProfile = (TextView) view.findViewById(R.id.tv_job_department);
            tvDate = (TextView) view.findViewById(R.id.tv_job_date);
            tvCompanyName = (TextView) view.findViewById(R.id.tv_job_company_name);
            tvExperience = (TextView) view.findViewById(R.id.tv_job_experience_number);
            tvLocation = (TextView) view.findViewById(R.id.tv_job_location_name);
            tvJobDuration = (TextView) view.findViewById(R.id.tv_job_duration);
            //tvSkills = (TextView) view.findViewById(R.id.tv_skills);
            tvJobDescription = (TextView) view.findViewById(R.id.tv_jobdescription);
            tvApplyNow = (TextView) view.findViewById(R.id.tv_applied_job);
        }
    }

    public DiscoverJobsAdapter(Context context, List<DiscoverJobModel> arrJobsList) {
        this.context = context;
        this.arrJobsList = arrJobsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_discover_jobs, parent, false);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(context, "DiscoverJobsAdapter");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Utilities.showLog(String.valueOf("Size=" + position));
        final DiscoverJobModel discoverJobModel = arrJobsList.get(position);
        holder.tvProfile.setText(discoverJobModel.getProfile());
        holder.tvCompanyName.setText(discoverJobModel.getComapnyName());
        // holder.tvSkills.setText(discoverJobModel.getPrimarySkillsName());
        holder.tvLocation.setText(discoverJobModel.getCityName());
        holder.tvExperience.setText(discoverJobModel.getExperinceMin() + "-" + discoverJobModel.getExperinceMax() + " year");
        holder.tvDate.setText(discoverJobModel.getCreatedOn());
        Utilities.showLog(String.valueOf(discoverJobModel.isApplied()));

        if (discoverJobModel.isApplied()) {
            holder.tvApplyNow.setText(R.string.Applied);
        } else {
            holder.tvApplyNow.setText(R.string.discover_job_apply_now);
        }

        count = 0;
        holder.tvApplyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if (count == 1) {
                    if (discoverJobModel.isApplied()) {
//                        Common.showApplyDialog(context, context.getString(R.string.applyalready));
                        Intent intent = new Intent(context, MyJobsActivity.class);
                        context.startActivity(intent);
                    } else {
                        Intent localImageintent = new Intent();
                        localImageintent.setAction("hitApplyjobApi");
                        localImageintent.putExtra("discover_jobApplied_id", discoverJobModel.getId());
                        context.sendBroadcast(localImageintent);
                    }
                }
            }
        });

        holder.tvJobDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, JobDetailsActivity.class);
                intent.putExtra("JOBBID", String.valueOf(discoverJobModel.getId()));
                intent.putExtra("JOBTITTLE", discoverJobModel.getComapnyName());
                intent.putExtra("JobPROFILE", discoverJobModel.getProfile());
                intent.putExtra("JOBLOCATION", discoverJobModel.getCityName());
                intent.putExtra("APPLYSTATUS", discoverJobModel.isApplied());
//                intent.putExtra("KEYFEATURES", discoverJobModel.getDescription());
                intent.putExtra("JOBDESCRIPTION", discoverJobModel.getDescription());
                intent.putExtra("JOBDURATION", discoverJobModel.getExperinceMin() + "-" + discoverJobModel.getExperinceMax() + " year");
                intent.putExtra("salary", discoverJobModel.getPackageMin() + " - " + discoverJobModel.getPackageMax() + " LPA");
                intent.putExtra("industury", discoverJobModel.getIndustryName());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrJobsList.size();
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}

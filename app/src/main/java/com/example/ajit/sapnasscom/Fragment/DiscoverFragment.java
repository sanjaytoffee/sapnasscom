package com.example.ajit.sapnasscom.Fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.MainActivity;
import com.example.ajit.sapnasscom.Activity.MycourseActivity;
import com.example.ajit.sapnasscom.Adapter.DiscoverAdapter;
import com.example.ajit.sapnasscom.Adapter.Industry_Adapter;
import com.example.ajit.sapnasscom.Adapter.Interest_Adapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Model.DiscoverModel;
import com.example.ajit.sapnasscom.Model.IndustryModel;
import com.example.ajit.sapnasscom.Model.InterestModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

public class DiscoverFragment extends Fragment {

    public static RadioButton radioCourse, radioJob;
    public static Spinner interest_spinner, industry_spinner;

    InterestModel interestModel;
    ArrayList<InterestModel> interestList;
    Interest_Adapter interest_adapter;

    IndustryModel industryModel;
    Industry_Adapter industry_adapter;
    ArrayList<IndustryModel> industryList;

    DiscoverAdapter discoverAdapter;
    public static ArrayList<DiscoverModel> discoverList;
    DiscoverModel discoverModel;

    public static RecyclerView rvDiscover;
    String interest_name, industry_name, SelectedCourse;
    int industry_id = 0, interest_id = 0;
    public static RadioGroup radioGroup;
    public static TextView tvbestCourse, tvRecommendation;
    int industryPosition = 0;
    LocaleClass localeClass;
    RelativeLayout rl_no_data_found;
    ProgressDialog pd;
    TextView tv_ok;

    public void onResume() {
        super.onResume();
        try {
            if (Common.dialog.isShowing()) {
                Common.dialog.dismiss();
            }
            if (pd.isShowing()) {
                pd.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_discover, container, false);

        radioGroup = (RadioGroup) view.findViewById(R.id.discoverRadioGroup);
        radioCourse = (RadioButton) view.findViewById(R.id.radio_course);
        radioJob = (RadioButton) view.findViewById(R.id.radio_job);
        interest_spinner = (Spinner) view.findViewById(R.id.prefered_interest_spinner);
        industry_spinner = (Spinner) view.findViewById(R.id.prefered_industry);
        rvDiscover = (RecyclerView) view.findViewById(R.id.rc_discover_content);
        tvRecommendation = (TextView) view.findViewById(R.id.recommendation);
        tvbestCourse = (TextView) view.findViewById(R.id.tvbestCourse);
        rl_no_data_found = (RelativeLayout) view.findViewById(R.id.rl_no_data_found);

        tv_ok=(TextView)view.findViewById(R.id.tv_ok);
        pd = new ProgressDialog(getActivity());

        discoverList = new ArrayList<>();
        tvRecommendation.setVisibility(View.GONE);

        tvRecommendation.setText(R.string.recommendations);
        setAdapter();
        setRecyclerViewAdapter();

        tv_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(),MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });

        radioCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectedCourse = "Course";
                industry_id = 0;
                interest_id = 0;
                discoverList.clear();
                tvRecommendation.setVisibility(View.GONE);
                rl_no_data_found.setVisibility(View.GONE);
                rvDiscover.setVisibility(View.GONE);
                setRecyclerViewAdapter();
                new HttpAsyncTask_Interest().execute(CommonUrl.drop_down + "GetDropdownList");
                new HttpAsyncTask_Industry().execute(CommonUrl.drop_down + "GetDropdownList");
                industry_spinner.setVisibility(View.GONE);
                interest_spinner.setVisibility(View.VISIBLE);
                PrefUtils.setSelected_Course(getActivity(), SelectedCourse);
            }
        });

        radioJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                industry_id = 0;
                interest_id = 0;
                discoverList.clear();
                setRecyclerViewAdapter();
                tvRecommendation.setVisibility(View.GONE);
                rl_no_data_found.setVisibility(View.GONE);
                rvDiscover.setVisibility(View.GONE);

                new HttpAsyncTask_Interest().execute(CommonUrl.drop_down + "GetDropdownList");
                new HttpAsyncTask_Industry().execute(CommonUrl.drop_down + "GetDropdownList");
                SelectedCourse = "Job";
                industry_spinner.setVisibility(View.GONE);
                interest_spinner.setVisibility(View.VISIBLE);

                PrefUtils.setSelected_Course(getActivity(), SelectedCourse);
            }
        });

        interest_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                interest_id = Integer.parseInt(interestList.get(i).getID());
                interest_name = interestList.get(i).getName();

                if (industryPosition > 0) {
//                    industry_id = industryPosition;
                } else {
                    industry_id = 0;
                }
                //   String text = industry_spinner.getSelectedItem().toString();
//                industry_id = Integer.parseInt(industryList.get(i).getID());
                Utilities.showLog("intrestId" + String.valueOf(interest_id) + "industryId=" + industry_id + "industry_id=" + industry_id);
                if (SelectedCourse.equals("Job")) {

                    if (industry_id != 0 || interest_id != 0) {
                        new HttpAsyncTaskCourse().execute(CommonUrl.getDashboardData + "GetCourseAndJobDiscoverByKey");
                    }
                    /*if (SelectedCourse.equals("Select Interest")) {
                        new HttpAsyncTaskCourse().execute(CommonUrl.getDashboardData + "GetCourseAndJobDiscoverByKey");
                    }else {
                        new HttpAsyncTaskCourse().execute(CommonUrl.getDashboardData + "GetCourseAndJobDiscoverByKey");
                    }*/
                } else {
                    if (interest_id != 0) {
                        new HttpAsyncTaskCourse().execute(CommonUrl.getDashboardData + "GetCourseAndJobDiscoverByKey");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            interest_spinner.setPopupBackgroundResource(R.drawable.border_search_text);
        }

        industry_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // interest_id = Integer.parseInt(interestList.get(i).getID());
                industry_id = Integer.parseInt(industryList.get(i).getID());
                industry_name = industryList.get(i).getName();
                industryPosition = i;
                Utilities.showLog("intrestId" + String.valueOf(interest_id) + "industryId=" + industry_id);
                if (SelectedCourse.equals("Job")) {
                    if (industry_id != 0 || interest_id != 0) {
                        new HttpAsyncTaskCourse().execute(CommonUrl.getDashboardData + "GetCourseAndJobDiscoverByKey");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            industry_spinner.setPopupBackgroundResource(R.drawable.border_search_text);
        }

        localeClass = new LocaleClass();
        localeClass.loadLocale(getActivity(), "DiscoverFragment");

        return view;
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvDiscover.setLayoutManager(mLayoutManager);
        rvDiscover.setItemAnimator(new DefaultItemAnimator());
    }
    private void setRecyclerViewAdapter() {
        discoverAdapter = new DiscoverAdapter(getActivity(), discoverList);
        rvDiscover.setAdapter(discoverAdapter);
        discoverAdapter.notifyDataSetChanged();
    }

    class HttpAsyncTask_Interest extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchInterest(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                interest_adapter = new Interest_Adapter(getActivity(), interestList);
                interest_spinner.setAdapter(interest_adapter);
                interest_adapter.notifyDataSetChanged();
            }
        }
    }

    public String fetchInterest(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "PRIMARYSKILLS");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                interestList = new ArrayList<>();
                interestModel = new InterestModel();
                interestModel.setName("Select Interest");
                interestModel.setID("0");
                interestList.add(interestModel);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        interestModel = new InterestModel();

                        interestModel.setID(catObj.getString("ID"));
                        interestModel.setCode(catObj.getString("Code"));
                        interestModel.setName(catObj.getString("Name"));
                        interestList.add(interestModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Industry extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchIndustry(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {

                industry_adapter = new Industry_Adapter(getActivity(), industryList);
                industry_spinner.setAdapter(industry_adapter);
                industry_adapter.notifyDataSetChanged();
            }
        }
    }

    public String fetchIndustry(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "INDUSTRY");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                industryList = new ArrayList<>();
                industryModel = new IndustryModel();
                industryModel.setName("Select Industry");
                industryModel.setID("0");
                industryList.add(industryModel);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        industryModel = new IndustryModel();

                        industryModel.setID(catObj.getString("ID"));
                        industryModel.setCode(catObj.getString("Code"));
                        industryModel.setName(catObj.getString("Name"));
                        industryList.add(industryModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private class HttpAsyncTaskCourse extends AsyncTask<String, Void, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
            discoverList = new ArrayList<>();
        }

        protected String doInBackground(String... urls) {

            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (discoverList.size() == 0) {
                tvRecommendation.setVisibility(View.GONE);
                rl_no_data_found.setVisibility(View.VISIBLE);
                rvDiscover.setVisibility(View.GONE);
            } else {
                tvRecommendation.setVisibility(View.VISIBLE);
                rl_no_data_found.setVisibility(View.GONE);
                rvDiscover.setVisibility(View.VISIBLE);
            }

            setRecyclerViewAdapter();
            pd.dismiss();
        }
    }

    public String getCourse(String url) {
        String result = "";
        try {
            JSONObject params = new JSONObject();
            params.put("PrimarySkillsId", interest_id);
            params.put("IndustryID", industry_id);
            params.put("Type", SelectedCourse);

            Log.e("DISCOVERCOURSE",params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.writeBytes("");
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {

                    Log.e("DiscoverOBJECT", response.toString());

                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        discoverModel = new DiscoverModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        discoverModel.setId(jsonObject.getString("Id"));
                        discoverModel.setPrimarySkillsName(jsonObject.getString("PrimarySkillsName"));
                        discoverModel.setSecondarySkillsName(jsonObject.getString("SecondarySkillsName"));
                        discoverModel.setName(jsonObject.getString("Name"));
                        discoverModel.setDiscoverCreater(jsonObject.getString("Creator"));

                        discoverList.add(discoverModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

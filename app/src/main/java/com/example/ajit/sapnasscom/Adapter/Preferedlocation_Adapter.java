package com.example.ajit.sapnasscom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Model.Education;
import com.example.ajit.sapnasscom.Model.PreferedLocation;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

/**
 * Created by DTP-110 on 1/27/2017.
 */

public class Preferedlocation_Adapter extends BaseAdapter
{

    Activity mActivity;
    ArrayList<PreferedLocation> preferedLocationList;

    public Preferedlocation_Adapter(Activity a, ArrayList<PreferedLocation> data) {
        this.mActivity = a;
        this.preferedLocationList = data;
    }



    @Override
    public int getCount() {
        return preferedLocationList.size();
    }

    @Override
    public Object getItem(int position) {
        return preferedLocationList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.preferedlocation_text, null);

            viewHolder.prefered_locationtext = (TextView) convertView
                    .findViewById(R.id.preferedlocation_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (preferedLocationList != null) {
            try {
                viewHolder.prefered_locationtext.setText("" + preferedLocationList.get(position).getName());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView prefered_locationtext;
    }

}



package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.asksira.loopingviewpager.LoopingPagerAdapter;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

public class DemoInfiniteAdapter extends LoopingPagerAdapter<Integer> {

    private static final int VIEW_TYPE_NORMAL = 100;
    private static final int VIEW_TYPE_SPECIAL = 101;

    public DemoInfiniteAdapter(Context context, ArrayList<Integer> itemList, boolean isInfinite) {
        super(context, itemList, isInfinite);
    }

    @Override
    protected int getItemViewType(int listPosition) {
//        if (itemList.get(listPosition) == 0) return VIEW_TYPE_SPECIAL;
        return VIEW_TYPE_NORMAL;
    }

    @Override
    protected View inflateView(int viewType, int listPosition) {
//        if (viewType == VIEW_TYPE_SPECIAL)
//            return LayoutInflater.from(context).inflate(R.layout.item_special, null);
        return LayoutInflater.from(context).inflate(R.layout.row_mover, null);
    }

    @Override
    protected void bindView(View convertView, int listPosition, int viewType) {
        if (viewType == VIEW_TYPE_SPECIAL) return;
      //  convertView.findViewById(R.id.image).setBackgroundColor(context.getResources().getColor(getBackgroundColor(listPosition)));
        ImageView ivLogo=convertView.findViewById(R.id.iv_logo);
        ivLogo.setBackgroundColor(context.getResources().getColor(getBackgroundColor(listPosition)));
        ivLogo.setImageResource(getImages(listPosition));
        TextView tvRelated = convertView.findViewById(R.id.tv_related_jobs);
        TextView tvDescription = convertView.findViewById(R.id.tv_description);
        tvRelated.setText(getJobs(listPosition));
        tvDescription.setText(getDescription(listPosition));
      //  tvDescription.setText(String.valueOf(itemList.get(listPosition)));
    }


    private int getBackgroundColor (int number) {
        switch (number) {
            case 0:
                return android.R.color.holo_red_light;
            case 1:
                return android.R.color.holo_orange_light;
            case 2:
                return android.R.color.holo_green_light;


            default:
                return android.R.color.black;
        }
    }
    private int getImages (int number) {
        switch (number) {
            case 0:
                return R.drawable.job_icons;
            case 1:
                return R.drawable.course_all;
            case 2:
                return R.drawable.descover_anything;

            default:
                return android.R.color.black;
        }
    }
    private String getJobs (int number) {
        switch (number) {
            case 0:
                return "Related Jobs";
            case 1:
                return"Courses for all";
            case 2:
                return "Discover anything";
            default:
                return "";
        }
    }
    private String getDescription(int number) {
        switch (number) {
            case 0:
                return "Find the Matching Job Opportunities available with your desired course!";
            case 1:
                return"  Now you're ready to start with your desired career option!";
            case 2:
                return "Find the Matching opportunities available with your desired course!";
            default:
                return "";
        }
    }
}

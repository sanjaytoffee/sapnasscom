package com.example.ajit.sapnasscom.Activity;

import android.app.Activity;
import android.os.Bundle;

import com.example.ajit.sapnasscom.R;

/**
 * Created by HSTPL on 2/28/2018.
 */

public class NoInternetActivity extends Activity {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.no_internet_activity);
    }
}

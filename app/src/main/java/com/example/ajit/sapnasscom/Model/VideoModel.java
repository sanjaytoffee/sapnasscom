package com.example.ajit.sapnasscom.Model;

/**
 * Created by pratik on 2/26/2018.
 */

public class VideoModel {
    String ID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    String Code;
    String Name;
}

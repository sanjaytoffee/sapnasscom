package com.example.ajit.sapnasscom.Activity;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Adapter.MyJobsAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.JobModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class MyJobsActivity extends AppCompatActivity {

    public MyJobsActivity() {
    }

    private String userId, status;
    private RecyclerView rvCourse;
    private SessionManager sessionManager;
    private ArrayList<JobModel> arrJobs;
    private MyJobsAdapter jobsAdapter;
    private double jobAppliedId;
    private boolean AppliedStatus = false;
    private RelativeLayout rlNoDatafound;
    private TextView tvOk;
    Toolbar toolbar;
    ProgressDialog pd;
    public static TextView tvWithdraw;
    LocaleClass localeClass;
    Parcelable recyclerViewState;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_job_activity);

        pd = new ProgressDialog(MyJobsActivity.this);
        init();
        getApplicationContext().registerReceiver(appliedJobApiCall, new IntentFilter("hitApplyjobsssApi"));

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyJobsActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "MyJobsActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Common.dialog.isShowing()) {
                Common.dialog.dismiss();
            }
            if (pd.isShowing()) {
                pd.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Common.isInternet(getApplicationContext())) {
            new MyJobsActivity.HttpAsyncTaskgetJobs().execute(CommonUrl.getAppliedJobs);
        } else {
            noInternet(MyJobsActivity.this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getApplicationContext().unregisterReceiver(appliedJobApiCall);
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.my_jobs);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        rvCourse = (RecyclerView) findViewById(R.id.rc_course);
        rlNoDatafound = (RelativeLayout) findViewById(R.id.rl_no_data_found);
        tvOk = (TextView) findViewById(R.id.tv_ok);
        arrJobs = new ArrayList<>();
        sessionManager = new SessionManager(getApplicationContext());
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        Utilities.showLog("userid=" + userId);
        setAdapter();
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvCourse.setLayoutManager(mLayoutManager);
        rvCourse.setItemAnimator(new DefaultItemAnimator());
    }

    private class HttpAsyncTaskgetJobs extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskgetJobs() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
            arrJobs = new ArrayList<>();
            jobsAdapter = new MyJobsAdapter(getApplicationContext(), arrJobs);
            recyclerViewState = rvCourse.getLayoutManager().onSaveInstanceState();
        }

        protected String doInBackground(String... urls) {

            return getJobs(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pd.dismiss();
            if (arrJobs.size() == 0) {
                rlNoDatafound.setVisibility(View.VISIBLE);
                rvCourse.setVisibility(View.GONE);
            } else {
                rlNoDatafound.setVisibility(View.GONE);
                rvCourse.setVisibility(View.VISIBLE);
            }
            rvCourse.setAdapter(jobsAdapter);
            rvCourse.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            Utilities.showLog(result);
        }
    }

    public String getJobs(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Id", userId);
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JobModel jobModel = new JobModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        jobModel.setId(jsonObject.getDouble("Id"));
                        jobModel.setProfile(jsonObject.getString("Profile"));
                        jobModel.setComapnyName(jsonObject.getString("ComapnyName"));
                        jobModel.setExperinceMin(jsonObject.getInt("ExperinceMin"));
                        jobModel.setExperinceMax(jsonObject.getInt("ExperinceMax"));
                        jobModel.setPrimarySkillsName(jsonObject.getString("PrimarySkillsName"));
                        jobModel.setCityName(jsonObject.getString("CityName"));
                        jobModel.setCreatedOn(jsonObject.getString("CreatedOn"));//Applied
                        jobModel.setApplied(jsonObject.getBoolean("Applied"));//Applied
                        jobModel.setLanguageName(jsonObject.getString("LanguageName"));
                        jobModel.setDescription(jsonObject.getString("Description"));
                        jobModel.setPackageMin(jsonObject.getString("PackageMin"));
                        jobModel.setPackageMax(jsonObject.getString("PackageMax"));//IndustryName
                        Utilities.showLog("salary=" + jsonObject.getInt("PackageMin") + "-" + jsonObject.getInt("PackageMax"));
                        jobModel.setIndustryName(jsonObject.getString("IndustryName"));
                        jobModel.setStatus(jsonObject.getString("JobStatus"));
                        jobModel.setSkillId(jsonObject.getInt("PrimarySkills"));

                        arrJobs.add(jobModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    BroadcastReceiver appliedJobApiCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                jobAppliedId = intent.getDoubleExtra("jobApplied_id", 00);
                Utilities.showLog(String.valueOf(jobAppliedId));
                if (Common.isInternet(getApplicationContext())) {
                    askOption(MyJobsActivity.this);
                }
            }
        }
    };

    public void askOption(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_dialog);
        tvWithdraw = (TextView) dialog.findViewById(R.id.tvMessage);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvBtnYes);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Common.isInternet(MyJobsActivity.this)) {
                    dialog.dismiss();
                    new HttpAsyncTaskJobApplied().execute(CommonUrl.setJobApplied);
                } else {
                    noInternet(MyJobsActivity.this);
                }
            }
        });

        tvWithdraw.setText(R.string.withdraw_message);
        TextView cancel = (TextView) dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private class HttpAsyncTaskJobApplied extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskJobApplied() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {
            return setJobApplied(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Utilities.showLog(result);
            new MyJobsActivity.HttpAsyncTaskgetJobs().execute(CommonUrl.getAppliedJobs);
        }
    }

    public String setJobApplied(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            Double d = new Double(jobAppliedId);
            params.put("JobId", String.valueOf(d.intValue()));
            params.put("Applied", "false");
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

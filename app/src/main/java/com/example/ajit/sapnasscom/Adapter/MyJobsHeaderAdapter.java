package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.example.ajit.sapnasscom.Activity.JobDescriptionActivity;
import com.example.ajit.sapnasscom.Activity.MainActivity;
import com.example.ajit.sapnasscom.Activity.MyJobsActivity;
import com.example.ajit.sapnasscom.Activity.ProfileDetailsActivity;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Fragment.FeedbackBottomSheetFragment;
import com.example.ajit.sapnasscom.Model.JobModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static com.example.ajit.sapnasscom.Adapter.MyViewHeaderAdapter.course;

public class MyJobsHeaderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    //Type of data in recycler view
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private List<JobModel> arrJobs;
    LocaleClass localeClass;
    public static String job,JOB;
    SessionManager sessionManager;
    int count = 0;

    public MyJobsHeaderAdapter(Context context, ArrayList<JobModel> arrJobs) {
        this.context = context;
        this.arrJobs = arrJobs;
        sessionManager = new SessionManager(context);
    }

    @Override
    public int getItemCount() {
        return (null != arrJobs ? arrJobs.size() + 1 : 0);//Add 2 more size to array list for Header and Footer
    }

    @Override
    public int getItemViewType(int position) {
        //Return item type according to requirement
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    //if position is 0 then type is header
    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        //If instance is DemoViewHolder the type is Item do your stuff over here
        if (holder instanceof MyViewHolder) {

            Utilities.showLog(String.valueOf("Size=" + position));
            final JobModel jobModel = arrJobs.get(position - 1);

            ((MyJobsHeaderAdapter.MyViewHolder) holder).tvProfile.setText(jobModel.getProfile());
            ((MyJobsHeaderAdapter.MyViewHolder) holder).tvCompanyName.setText(jobModel.getComapnyName());
            ((MyJobsHeaderAdapter.MyViewHolder) holder).tvSkills.setText(jobModel.getPrimarySkillsName());
            ((MyJobsHeaderAdapter.MyViewHolder) holder).tvLocation.setText(jobModel.getCityName());
            ((MyJobsHeaderAdapter.MyViewHolder) holder).tvExperience.setText(jobModel.getExperinceMin() + "-" + jobModel.getExperinceMax() + " year");
            ((MyJobsHeaderAdapter.MyViewHolder) holder).tvDate.setText(jobModel.getCreatedOn());

            if (jobModel.isApplied()) {
                ((MyJobsHeaderAdapter.MyViewHolder) holder).tvApplyNow.setText(R.string.Applied);
                sessionManager.createApplied("" + R.string.Applied);
            } else {
                ((MyJobsHeaderAdapter.MyViewHolder) holder).tvApplyNow.setText(R.string.Apply_Now);
                sessionManager.createApplied("" + R.string.Apply_Now);
            }

            count = 0;
            ((MyJobsHeaderAdapter.MyViewHolder) holder).tvApplyNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    count++;
                    if (count == 1) {
                        if (jobModel.isApplied()) {
//                        Common.showApplyDialog(context, context.getString(R.string.applyalready));
                            Intent intent = new Intent(context, MyJobsActivity.class);
                            context.startActivity(intent);
                        } else {
                            Intent localImageintent = new Intent();
                            localImageintent.setAction("hitApplyjobApi");
                            localImageintent.putExtra("jobApplied_id", jobModel.getId());
                            context.sendBroadcast(localImageintent);
                        }
                    }
                }
            });

            ((MyJobsHeaderAdapter.MyViewHolder) holder).tvJobDescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, JobDescriptionActivity.class);
                    intent.putExtra("jobid", jobModel.getId());
                    intent.putExtra("company_name", jobModel.getComapnyName());
                    intent.putExtra("profile_name", jobModel.getProfile());
                    intent.putExtra("location", jobModel.getCityName());
                    intent.putExtra("applystatus", jobModel.isApplied());
                    intent.putExtra("description", jobModel.getDescription());
                    intent.putExtra("experience", jobModel.getExperinceMin() + "-" + jobModel.getExperinceMax() + " year");
                    intent.putExtra("salary", jobModel.getPackageMin() + " - " + jobModel.getPackageMax() + " LPA");
                    intent.putExtra("industury", jobModel.getIndustryName());
                    intent.putExtra("myJobClick", "false");
                    intent.putExtra("SkillId", jobModel.getSkillId());
                    context.startActivity(intent);
                }
            });

            try {
                if (jobModel.getFeadBack().equalsIgnoreCase("true")) {
                    ((MyJobsHeaderAdapter.MyViewHolder) holder).cardView.setBackgroundColor(Color.parseColor("#fdd1c8"));
                    ((MyJobsHeaderAdapter.MyViewHolder) holder).JobRelative.setBackgroundColor(Color.parseColor("#fdd1c8"));
                } else {
                    ((MyJobsHeaderAdapter.MyViewHolder) holder).cardView.setBackgroundColor(Color.parseColor("#ffffff"));
                    ((MyJobsHeaderAdapter.MyViewHolder) holder).JobRelative.setBackgroundColor(Color.parseColor("#ffffff"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            ((MyJobsHeaderAdapter.MyViewHolder) holder).dot_click.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (jobModel.getFeadBack().equals("false")) {
                        FeedbackBottomSheetFragment bottomSheetDialogFragment = new FeedbackBottomSheetFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("Skill_list", (Serializable) Common.fetch_Skill(context));
                        bundle.putString("name", "From Adapter");
                        bundle.putString("COURSE_FEEDBACK", "JOB_FEEDBACK");
                        sessionManager.CreateFeedback("JOB_FEEDBACK");
                        bottomSheetDialogFragment.setArguments(bundle);
                        bottomSheetDialogFragment.show(((MainActivity) context).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        job = String.valueOf(jobModel.getId());
                        course = "JOB";
//                    crossText = "Give feedbackon this job";
                    } else {
                        Toast.makeText(context, "You have already submited your feedback", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            try {
                if (jobModel.getFeadBack().equalsIgnoreCase("true")) {
                    ((MyViewHolder) holder).cardView.setBackgroundColor(Color.parseColor("#fdd1c8"));
                    ((MyViewHolder) holder).JobRelative.setBackgroundColor(Color.parseColor("#fdd1c8"));
                } else {
                    ((MyViewHolder) holder).cardView.setBackgroundColor(Color.parseColor("#ffffff"));
                    ((MyViewHolder) holder).JobRelative.setBackgroundColor(Color.parseColor("#ffffff"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (holder instanceof RecyclerView_HeaderFooter_Holder) {
            //Else the type is header
            if (position == 0)
                ((RecyclerView_HeaderFooter_Holder) holder).title.setText(context.getResources().getString(R.string.recommend_texts));//if position is 0 set title to header view
            ((RecyclerView_HeaderFooter_Holder) holder).title.setTextColor(Color.parseColor("#1E90FF"));

            ((RecyclerView_HeaderFooter_Holder) holder).title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, ProfileDetailsActivity.class);
                    intent.putExtra("editProfile", "editProfile");
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(
            ViewGroup viewGroup, int viewType) {

        //inflate the item according to item type
        View itemView;

        //Since we are using same holder for both header and footer so we can return same holder
        if (viewType == TYPE_HEADER) {
            itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.header_footer_view, viewGroup, false);
            try {
                localeClass = new LocaleClass();
                localeClass.loadLocale(context, "MyJobsHeaderAdapter");
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return new RecyclerView_HeaderFooter_Holder(itemView);
        } else if (viewType == TYPE_ITEM) {
            //inflate your layout and pass it to view holder
            itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.row_job, viewGroup, false);
            try {
                localeClass = new LocaleClass();
                localeClass.loadLocale(context, "MyJobsHeaderAdapter");
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return new MyViewHolder(itemView);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");//Some error occurs then exception occurs
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tvProfile, tv_experience, tvCompanyName, tvSkills, tvExperience, tvDate, tvCreatedby, tvDuration, tvLocation, tvLanguage, tv__cource_location, tv_duration, tvApplyNow, tvJobDescription;
        private ImageView ivSite;
        LinearLayout dot_click;
        public CardView cardView;
        public RelativeLayout JobRelative;

        public MyViewHolder(View view) {
            super(view);
            tv_experience = (TextView) view.findViewById(R.id.tv_experience);
            tvProfile = (TextView) view.findViewById(R.id.tv_department);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvCompanyName = (TextView) view.findViewById(R.id.tv_company_name);
            tvExperience = (TextView) view.findViewById(R.id.tv_experience_number);
            tvLocation = (TextView) view.findViewById(R.id.tv_location_name);
            tvSkills = (TextView) view.findViewById(R.id.tv_skills);
            tvJobDescription = (TextView) view.findViewById(R.id.tv_job_description);
            tvApplyNow = (TextView) view.findViewById(R.id.tv_apply_job);
            tv_duration = (TextView) view.findViewById(R.id.tv_duration);
            tv__cource_location = (TextView) view.findViewById(R.id.tv__cource_location);
            dot_click = (LinearLayout) view.findViewById(R.id.dot_click);
            cardView = (CardView) view.findViewById(R.id.cardView);
            JobRelative = (RelativeLayout) view.findViewById(R.id.JobRelative);
        }
    }

    public static class RecyclerView_HeaderFooter_Holder extends RecyclerView.ViewHolder {
        public static TextView title,title_text;

        public RecyclerView_HeaderFooter_Holder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.header_footer_title);
            title_text = (TextView) itemView.findViewById(R.id.header_title);
        }
    }

}

package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.AndroidRuntimeException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Activity.JobDescriptionActivity;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Model.JobModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Utilities;
import java.util.List;

/**
 * Created by pratik on 12/7/2017.
 */

public class MyJobsAdapter extends RecyclerView.Adapter<MyJobsAdapter.MyViewHolder> {
    private List<JobModel> arrJobs;
    private Context context;
    LocaleClass localeClass;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tvProfile,tv_experience, tvCompanyName,tv_myjobs_status, tvSkills, tvExperience, tvDate, tvCreatedby, tvDuration, tvLocation, tvLanguage, tv__cource_status, tv__cource_location, tv_duration, tvApplyNow, tvJobDescription;
        private ImageView ivSite;

        public MyViewHolder(View view) {
            super(view);
            tv_experience = (TextView) view.findViewById(R.id.tv_experience);
            tv_myjobs_status = (TextView) view.findViewById(R.id.tv_myjobs_status);
            tvProfile = (TextView) view.findViewById(R.id.tv_department);
            tvDate = (TextView) view.findViewById(R.id.tv_date);
            tvCompanyName = (TextView) view.findViewById(R.id.tv_company_name);
            tvExperience = (TextView) view.findViewById(R.id.tv_experience_number);
            tvLocation = (TextView) view.findViewById(R.id.tv_location_name);
            tvSkills = (TextView) view.findViewById(R.id.tv_skills);
            tvJobDescription = (TextView) view.findViewById(R.id.tv_job_description);
            tvApplyNow = (TextView) view.findViewById(R.id.tv_apply_job1);
            tv_duration = (TextView) view.findViewById(R.id.tv_duration);
            tv__cource_location = (TextView) view.findViewById(R.id.tv__cource_location);
            tv__cource_status = (TextView) view.findViewById(R.id.tv__cource_status);
        }
    }

    public MyJobsAdapter(Context context, List<JobModel> arrJobs) {
        this.context = context;
        this.arrJobs = arrJobs;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.my_row_job, parent, false);
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(context, "MyJobsAdapter");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Utilities.showLog(String.valueOf("Size=" + position));
        final JobModel jobModel = arrJobs.get(position);
        holder.tvProfile.setText(jobModel.getProfile());
        holder.tvCompanyName.setText(jobModel.getComapnyName());
        holder.tvSkills.setText(jobModel.getPrimarySkillsName());
        holder.tvLocation.setText(jobModel.getCityName());
        holder.tv__cource_status.setText(jobModel.getStatus());
        holder.tvExperience.setText(jobModel.getExperinceMin() + "-" + jobModel.getExperinceMax() + " year");
        holder.tvDate.setText(jobModel.getCreatedOn());

        if (jobModel.isApplied()) {
            holder.tvApplyNow.setText(R.string.withdraw_application);
        }

        holder.tvApplyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jobModel.isApplied()) {
                    Intent localImageintent = new Intent();
                    localImageintent.setAction("hitApplyjobsssApi");
                    localImageintent.putExtra("jobApplied_id", jobModel.getId());
                    context.sendBroadcast(localImageintent);
                }
            }
        });

        holder.tvJobDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(context, JobDescriptionActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("jobid", jobModel.getId());
                    intent.putExtra("company_name", jobModel.getComapnyName());
                    intent.putExtra("profile_name", jobModel.getProfile());
                    intent.putExtra("location", jobModel.getCityName());
                    intent.putExtra("applystatus", jobModel.isApplied());
                    intent.putExtra("description", jobModel.getDescription());
                    intent.putExtra("experience", jobModel.getExperinceMin() + "-" + jobModel.getExperinceMax() + " year");
                    intent.putExtra("salary", jobModel.getPackageMin() + " - " + jobModel.getPackageMax() + " LPA");
                    intent.putExtra("industury", jobModel.getIndustryName());
                    intent.putExtra("jobStatus", jobModel.getStatus());
                    intent.putExtra("myJobClick", "true");
                    intent.putExtra("SkillId", jobModel.getSkillId());
                    context.startActivity(intent);
                } catch (AndroidRuntimeException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arrJobs.size();
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

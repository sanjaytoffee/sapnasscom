package com.example.ajit.sapnasscom.Fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Activity.MainActivity;
import com.example.ajit.sapnasscom.Activity.ProfileDetailsActivity;
import com.example.ajit.sapnasscom.Adapter.JobsAdapter;
import com.example.ajit.sapnasscom.Adapter.MyJobsHeaderAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.JobModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class JobsFragment extends Fragment {

    public JobsFragment() {
    }

    private String userId, status;
    private RecyclerView rvCourse;
    private SessionManager sessionManager;
    private ArrayList<JobModel> arrJobs;
    private JobsAdapter jobsAdapter;
    private double jobAppliedId;
    private boolean AppliedStatus = false;
    private RelativeLayout rlNoDatafound;
    public static TextView  title_text,title,tvOk;
    ProgressDialog pd;
    Parcelable recyclerViewState;
    LocaleClass localeClass;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(appliedJobApiCall, new IntentFilter("hitApplyjobApi"));
        getActivity().registerReceiver(callJobApiCall, new IntentFilter("hitjpbFeedbachapi"));
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Common.dialog.isShowing()) {
                Common.dialog.dismiss();
            }
            if (pd.isShowing()) {
                pd.dismiss();
            }
            if (AppliedStatus) {
                Common.hideApplyDialog();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (Common.isInternet(getActivity())) {
            new JobsFragment.HttpAsyncTaskgetJobs().execute(CommonUrl.getJobs);
        } else {
            noInternet(getActivity());
        }
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(appliedJobApiCall);
        getActivity().unregisterReceiver(callJobApiCall);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_job, container, false);
        init(v);
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getActivity(), "CourseFragment");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        pd = new ProgressDialog(getActivity());

        return v;
    }

    private void init(View v) {
        rvCourse = (RecyclerView) v.findViewById(R.id.rc_course);
        rlNoDatafound = (RelativeLayout) v.findViewById(R.id.rl_no_data_found);
        tvOk = (TextView) v.findViewById(R.id.tv_ok);
        title = (TextView) v.findViewById(R.id.header_footer_title);
        title_text = (TextView) v.findViewById(R.id.header_title);
        title.setText(getActivity().getResources().getString(R.string.recommend_texts));//if position is 0 set title to header view
        title.setTextColor(Color.parseColor("#1E90FF"));

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfileDetailsActivity.class);
                intent.putExtra("editProfile", "editProfile");
                getActivity().startActivity(intent);
            }
        });
        sessionManager = new SessionManager(getActivity());
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        Utilities.showLog("userid=" + userId);
        setAdapter();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setTab();
            }
        });
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCourse.setLayoutManager(mLayoutManager);
        rvCourse.setItemAnimator(new DefaultItemAnimator());
    }

    private class HttpAsyncTaskgetJobs extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskgetJobs() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
            arrJobs = new ArrayList<>();
           // arrJobs.clear();
            jobsAdapter = new JobsAdapter(getActivity(), arrJobs);
            recyclerViewState = rvCourse.getLayoutManager().onSaveInstanceState();
        }

        protected String doInBackground(String... urls) {
            return getJobs(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (arrJobs.size() == 0) {
                rlNoDatafound.setVisibility(View.VISIBLE);
                rvCourse.setVisibility(View.GONE);
            } else {
                rlNoDatafound.setVisibility(View.GONE);
                rvCourse.setVisibility(View.VISIBLE);
            }
            rvCourse.setAdapter(jobsAdapter);
            rvCourse.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            Utilities.showLog(result);

            pd.dismiss();
        }
    }

    public String getJobs(String url) {
        String result = "";
        Log.e("JobsUrl : ", url);
        try {
            JSONObject params = new JSONObject();
            params.put("id", userId);

            Log.e("JOBSPARAMS",params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JobModel jobModel = new JobModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        jobModel.setId(jsonObject.getDouble("Id"));
                        jobModel.setProfile(jsonObject.getString("Profile"));
                        jobModel.setComapnyName(jsonObject.getString("ComapnyName"));
                        jobModel.setExperinceMin(jsonObject.getInt("ExperinceMin"));
                        jobModel.setExperinceMax(jsonObject.getInt("ExperinceMax"));
                        jobModel.setPrimarySkillsName(jsonObject.getString("PrimarySkillsName"));
                        jobModel.setCityName(jsonObject.getString("CityName"));
                        jobModel.setCreatedOn(jsonObject.getString("CreatedOn"));
                        jobModel.setApplied(jsonObject.getBoolean("Applied"));
                        jobModel.setLanguageName(jsonObject.getString("LanguageName"));
                        jobModel.setDescription(jsonObject.getString("Description"));
                        jobModel.setPackageMin(jsonObject.getString("PackageMin"));
                        jobModel.setPackageMax(jsonObject.getString("PackageMax"));
                        Utilities.showLog("salary=" + jsonObject.getInt("PackageMin") + "-" + jsonObject.getInt("PackageMax"));
                        jobModel.setIndustryName(jsonObject.getString("IndustryName"));
                        jobModel.setFeadBack(jsonObject.getString("IsFeedback"));
                        jobModel.setSkillId(jsonObject.getInt("PrimarySkills"));

                        arrJobs.add(jobModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    BroadcastReceiver appliedJobApiCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                jobAppliedId = intent.getDoubleExtra("jobApplied_id", 00);
                Utilities.showLog(String.valueOf(jobAppliedId));
                if (Common.isInternet(getActivity())) {
                    new JobsFragment.HttpAsyncTaskJobApplied().execute(CommonUrl.setJobApplied);
                } else {
                    noInternet(getActivity());
                }
            }
        }
    };

    BroadcastReceiver callJobApiCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (Common.isInternet(getActivity())) {
                    new JobsFragment.HttpAsyncTaskgetJobs().execute(CommonUrl.getJobs);
                } else {
                    noInternet(getActivity());
                }
            }
        }
    };

    private class HttpAsyncTaskJobApplied extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskJobApplied() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setCancelable(false);
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {

            return setJobApplied(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Utilities.showLog(result);
            if (AppliedStatus) {
                new JobsFragment.HttpAsyncTaskgetJobs().execute(CommonUrl.getJobs);
                Common.showApplyDialog(getActivity(), getActivity().getString(R.string.apply));
            }
        }
    }

    public String setJobApplied(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            Double d = new Double(jobAppliedId);
            params.put("JobId", String.valueOf(d.intValue()));
            params.put("Applied", "true");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                   /* JSONObject jsonoObject = new JSONObject(response.toString());
                    //status = jsonoObject.getString("Result");*/
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        AppliedStatus = true;
                    }
//                    rvCourse.setAdapter(courseAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

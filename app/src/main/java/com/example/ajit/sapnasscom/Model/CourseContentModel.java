package com.example.ajit.sapnasscom.Model;

/**
 * Created by pratik on 2/19/2018.
 */

public class CourseContentModel {
    String courseTitle;
    String courseTagname;
    String courseContent;
    String courseCreater;

    public String getCourseTitle() {
        return courseTitle;
    }

    public void setCourseTitle(String courseTitle) {
        this.courseTitle = courseTitle;
    }

    public String getCourseTagname() {
        return courseTagname;
    }

    public void setCourseTagname(String courseTagname) {
        this.courseTagname = courseTagname;
    }

    public String getCourseContent() {
        return courseContent;
    }

    public void setCourseContent(String courseContent) {
        this.courseContent = courseContent;
    }

    public String getCourseCreater() {
        return courseCreater;
    }

    public void setCourseCreater(String courseCreater) {
        this.courseCreater = courseCreater;
    }
}


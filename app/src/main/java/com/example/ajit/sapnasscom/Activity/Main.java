package com.example.ajit.sapnasscom.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.example.ajit.sapnasscom.Utils.Utilities;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;


public class Main extends AppCompatActivity {

    private static final int RC_SIGN_IN = 234;
    private static final String TAG = "sap";
    GoogleSignInClient mGoogleSignInClient;
    String responseId, status;
    SessionManager sessionManager;
    FirebaseAuth mAuth;
    public static TextView tvterms, textViewNotRegister, tvprivacy, textViewByUsing, textViewSignup;
    SpannableString termsContent, policyContent;
    String userId = "";
    String userFullName = "", userFirstName, UserLastNAme, languageStatus;
    String userEmail = "";
    String photoUrl = "";
    public static Button buttonSignup, buttonLoginEmail, buttonConnectGoogle;
    LocaleClass localeClass;
    public static final int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 0;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.main_activity);
        sessionManager = new SessionManager(getApplicationContext());
//        HashMap<String, String> getmail = sessionManager.getEmail();
        //userId = getmail.get(SessionManager.KEY_USER_ID);
        tvterms = (TextView) findViewById(R.id.tv_terms);
        tvprivacy = (TextView) findViewById(R.id.tvPrivacy);
        textViewNotRegister = (TextView) findViewById(R.id.not_register);

        textViewSignup = (TextView) findViewById(R.id.signup_txt);
        textViewByUsing = (TextView) findViewById(R.id.bu_using);
        buttonLoginEmail = (Button) findViewById(R.id.login_email);
        buttonConnectGoogle = (Button) findViewById(R.id.sign_in_button);
        buttonSignup = (Button) findViewById(R.id.sign_up);
        pd = new ProgressDialog(Main.this);

        checkPermission();

        termsContent = new SpannableString("Terms of use");
        termsContent.setSpan(new UnderlineSpan(), 0, termsContent.length(), 0);
        tvterms.setText(termsContent);

        policyContent = new SpannableString("Privacy Policy");
        policyContent.setSpan(new UnderlineSpan(), 0, policyContent.length(), 0);
        tvprivacy.setText(policyContent);

        tvterms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TermsActivity.class);
                startActivity(intent);
            }
        });

        tvprivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PolicyActivity.class);
                startActivity(intent);
            }
        });


        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.isInternet(Main.this)) {
                    signIn();
                } else {
                    noInternet(Main.this);
                }

            }
        });

        findViewById(R.id.login_email).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.sign_up).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(intent);
            }
        });

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "Main");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //if the requestCode is the Google Sign In code that we defined at starting
        if (requestCode == RC_SIGN_IN) {

            //Getting the GoogleSignIn Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);

                //authenticating with firebase
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            userId = user.getUid();
                            userFullName = user.getDisplayName();
                            userEmail = user.getEmail();
                            photoUrl = String.valueOf(user.getPhotoUrl());
                            Utilities.showLog("name=" + userFullName + "\nuserId=" + userId + "\nuserEmail" + userEmail + "\nphotoUrl=" + photoUrl);
                            new HttpAsyncTaskSignup().execute(CommonUrl.Web_login + "SaveUserDetails");

                            Toast.makeText(Main.this, "User Signed In", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(Main.this, "Authentication failed.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private class HttpAsyncTaskSignup extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskSignup() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {

            return signUp(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            pd.dismiss();
            Utilities.showLog("response==" + result);
            if (status.equalsIgnoreCase("Success")) {
                sessionManager.createEmail("", responseId, "");
                PrefUtils.setUserLoginStatus(true);
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getApplicationContext(), ProfileDetailsActivity.class);
                intent.putExtra("editProfile", "signup");
                startActivity(intent);

            } else if (status.equalsIgnoreCase("Email Id Already Exist.")) {
                Utilities.showLog("Log=" + PrefUtils.getUserFullName(Main.this));
                sessionManager.createEmail("", responseId, "");
                PrefUtils.setUserLoginStatus(true);
                if (languageStatus.equalsIgnoreCase("0")) {
                    Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(getApplicationContext(), ProfileDetailsActivity.class);
                    intent.putExtra("editProfile", "signup");
                    startActivity(intent);
                } else {
                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            } else {
                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String signUp(String url) {
        String result = "";
        Log.e("GOOGLESIGN", url);

        try {
            JSONObject params = new JSONObject();
            params.put("Id", "0");
            params.put("Email", userEmail);
            params.put("FirstName", userFullName);
            params.put("LastName", "");
            params.put("Password", "");
            params.put("LoginType", "USER");
            params.put("ClientID", "0");
            params.put("UserFrom", "");
            params.put("PhoneNo", "");
            params.put("WebSite", "");
            params.put("FaceBook", "");
            params.put("Twitter", "");
            params.put("LinkedIn", "");
            params.put("Gender", "");

            Log.e("GOOGLEPARAMS", params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog("GOOGLERESPONSE" + response.toString());
                    JSONObject jsonoObject = new JSONObject(response.toString());
                    status = jsonoObject.getString("Response");
                    responseId = jsonoObject.getString("Id");
                    userEmail = jsonoObject.getString("Email");
                    languageStatus = jsonoObject.getString("LaguageId");
                    PrefUtils.setUserID(Main.this, responseId);
                    PrefUtils.setUserEmail(Main.this, userEmail);
                    PrefUtils.setUserFullName(Main.this, jsonoObject.getString("UserFullName"));
                    PrefUtils.setUserFirstName(Main.this, jsonoObject.getString("FirstName"));
                    PrefUtils.setUserLastName(Main.this, jsonoObject.getString("LastName"));
//                    PrefUtils.setUser_Profileurl(Main.this, jsonoObject.getString("Profilepath"));
                    PrefUtils.setUser_Profileurl(Main.this, CommonUrl.ImageUserProfileUrl + jsonoObject.getString("Profilepath"));
                    PrefUtils.setGender(Main.this, jsonoObject.getString("Gender"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @SuppressLint("NewApi")
    public void checkPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_FINE_LOCATION))
//            permissionsNeeded.add("GPS");
//        if (!addPermission(permissionsList, Manifest.permission.ACCESS_COARSE_LOCATION))
//            permissionsNeeded.add("Network location");
        if (!addPermission(permissionsList, Manifest.permission.CAMERA))
            permissionsNeeded.add("Camera");
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE))
            permissionsNeeded.add("Storage");
        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
            permissionsNeeded.add("Read Sms");
        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message,
                        new DialogInterface.OnClickListener() {
                            @SuppressLint("NewApi")
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                                        REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                            }
                        });
                return;
            }
            requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                permissionsList.add(permission);
                if (!shouldShowRequestPermissionRationale(permission))
                    return false;
            }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
//                perms.put(Manifest.permission.ACCESS_COARSE_LOCATION, PackageManager.PERMISSION_GRANTED);
//                perms.put(Manifest.permission.ACCESS_FINE_LOCATION, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (
//                        perms.get(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
//                        && perms.get(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
//                        &&
                        perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                                && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                }
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(Main.this, R.style.MyDialogTheme)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();

    }

    @Override
    protected void onResume() {
        //  android.os.Process.killProcess(android.os.Process.myPid());
        sessionManager.logoutUser();
        sessionManager.clearPreference(Main.this);
        trimCache(this);
        super.onResume();
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);

            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }


    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}

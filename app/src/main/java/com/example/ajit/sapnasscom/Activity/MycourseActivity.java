package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Adapter.MyCourseAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.CourseModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

/**
 * Created by HSTPL on 2/19/2018.
 */

public class MycourseActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private double courseEnrolledId;
    private String userId, status;
    private RecyclerView rvCourse;
    private ArrayList<CourseModel> arrCourse;
    private SessionManager sessionManager;
    private CourseModel courseModel;
    private MyCourseAdapter courseAdapter;
    private boolean enrolledStatus = false;
    private static String courseNmae;
    RelativeLayout rlNoDatafound;
    TextView tvOk;
    ProgressDialog pd;
    Parcelable recyclerViewState;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycourse);
        pd = new ProgressDialog(MycourseActivity.this);

        init();
        setDataMyCourses();

        MycourseActivity.this.registerReceiver(hitMyCourseApiCalllllll, new IntentFilter("hitMycourseEnrolledApiiiiii"));

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MycourseActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    BroadcastReceiver hitMyCourseApiCalllllll = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                new HttpAsyncTaskCourse().execute(CommonUrl.getMyCourse);
            }
        }
    };

    public void onDestroy() {
        super.onDestroy();
        MycourseActivity.this.unregisterReceiver(hitMyCourseApiCalllllll);
    }

    private void setDataMyCourses() {
        if (Common.isInternet(MycourseActivity.this)) {
            new HttpAsyncTaskCourse().execute(CommonUrl.getMyCourse);
        } else {
            noInternet(MycourseActivity.this);
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.My_Courses);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        tvOk = (TextView) findViewById(R.id.tv_ok);
        rlNoDatafound = (RelativeLayout) findViewById(R.id.nodataRelative);
        rvCourse = (RecyclerView) findViewById(R.id.rc_course);

        sessionManager = new SessionManager(MycourseActivity.this);
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        Utilities.showLog("userid=" + userId);
        setAdapter();
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MycourseActivity.this);
        rvCourse.setLayoutManager(mLayoutManager);
        rvCourse.setItemAnimator(new DefaultItemAnimator());
    }

    private class HttpAsyncTaskCourse extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourse() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
            arrCourse = new ArrayList<>();
            courseAdapter = new MyCourseAdapter(MycourseActivity.this, arrCourse);
            recyclerViewState = rvCourse.getLayoutManager().onSaveInstanceState();
        }

        protected String doInBackground(String... urls) {
            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (arrCourse.size() == 0) {
                rlNoDatafound.setVisibility(View.VISIBLE);
                rvCourse.setVisibility(View.GONE);
            } else {
                rlNoDatafound.setVisibility(View.GONE);
                rvCourse.setVisibility(View.VISIBLE);
            }

            rvCourse.setAdapter(courseAdapter);
            rvCourse.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            Utilities.showLog(result);
            pd.dismiss();
        }
    }

    public String getCourse(String url) {
        String result = "";
        Log.e("MycourseAPI : ", "" + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", userId);
            Log.e("UserID : ", userId);
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    enrolledStatus = false;
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        CourseModel courseModel = new CourseModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        courseModel.setIdValue(jsonObject.getString("Id"));
                        courseModel.setCourseName(jsonObject.getString("CourseName"));
                        courseModel.setDuration(jsonObject.getString("Duration"));
                        courseModel.setCityName(jsonObject.getString("CityName"));
                        courseModel.setCreator(jsonObject.getString("Creator"));
                        courseModel.setCreatedOn(jsonObject.getString("CreatedOn"));
                        courseModel.setLanguageName(jsonObject.getString("LanguageName"));
                        courseModel.setEnrolled(jsonObject.getBoolean("Enrolled"));
                        courseModel.setKeyFeature(jsonObject.getString("KeyFeature"));
                        courseModel.setDescription(jsonObject.getString("Description"));
                        courseModel.setEnrolledDate(jsonObject.getString("EnrolledDate"));
                        courseModel.setLearStatus(jsonObject.getString("LearnStatus"));
                        courseModel.setFeadBack(jsonObject.getString("IsFeedback"));
                        courseModel.setCourseCount(jsonObject.getInt("TotalCourseEnrolled"));
                        courseModel.setSkillId(jsonObject.getInt("PrimarySkills"));
                        arrCourse.add(courseModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
package com.example.ajit.sapnasscom.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;

import java.io.File;

public class LanguageScreenActivity extends Activity implements View.OnClickListener {
    TextView tv_continue, english_radio_button, hindi_radio_button, kannada_radio_button;
    String selectedLanguage = "", selectedItem = "", lang = "";
    LocaleClass localeClass;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_screen);
        sessionManager = new SessionManager(LanguageScreenActivity.this);

        getField();

        english_radio_button.setOnClickListener(this);
        hindi_radio_button.setOnClickListener(this);
        kannada_radio_button.setOnClickListener(this);
        tv_continue.setOnClickListener(this);

        localeClass = new LocaleClass();
        localeClass.loadLocale(getApplicationContext(), "");
    }

    private void getField() {
        english_radio_button = (TextView) findViewById(R.id.english_radio_button);
        hindi_radio_button = (TextView) findViewById(R.id.hindi_radio_button);
        kannada_radio_button = (TextView) findViewById(R.id.kannada_radio_button);
        tv_continue = (TextView) findViewById(R.id.tv_continue);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.english_radio_button:

//                english_radio_button.setBackgroundResource(R.drawable.color_primary_background);
//                hindi_radio_button.setBackgroundResource(R.drawable.tab_unselected);
//                kannada_radio_button.setBackgroundResource(R.drawable.tab_unselected);
                selectedItem = "English";
                lang = "en";
                sessionManager.createLanguage(selectedItem);
                onOptionLanguage();
                Intent intent1 = new Intent(getApplicationContext(), SignUpDescoverActivity.class);
                startActivity(intent1);
                finish();
                break;

            case R.id.hindi_radio_button:
//                english_radio_button.setBackgroundResource(R.drawable.tab_unselected);
//                hindi_radio_button.setBackgroundResource(R.drawable.color_primary_background);
//                kannada_radio_button.setBackgroundResource(R.drawable.tab_unselected);
                selectedItem = "Hindi";
                lang = "hi";
                sessionManager.createLanguage(selectedItem);
                saveLocale(lang,getApplicationContext());
                onOptionLanguage();
                Intent intent2 = new Intent(getApplicationContext(), SignUpDescoverActivity.class);
                startActivity(intent2);
                finish();
                break;

            case R.id.kannada_radio_button:
//                english_radio_button.setBackgroundResource(R.drawable.tab_unselected);
//                hindi_radio_button.setBackgroundResource(R.drawable.tab_unselected);
//                kannada_radio_button.setBackgroundResource(R.drawable.color_primary_background);
                selectedItem = "Kannada";
                lang = "ka";
                sessionManager.createLanguage(selectedItem);
                onOptionLanguage();
                Intent intent3 = new Intent(getApplicationContext(), SignUpDescoverActivity.class);
                startActivity(intent3);
                finish();
                break;

            case R.id.tv_continue:
//                onOptionLanguage();
//                Intent intent3 = new Intent(getApplicationContext(), SignUpDescoverActivity.class);
//                startActivity(intent3);
//                finish();
                break;
        }
    }
    public void saveLocale(String lang, Context context) {
        String langPref = "Language";
        SharedPreferences prefs = context.getSharedPreferences("CommonPrefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(langPref, lang);
        editor.commit();
    }
    private void onOptionLanguage() {
        if (selectedLanguage != null) {
            if (selectedItem.equals("English")) {
                lang = "en";
                localeClass.changeLang(lang, getApplicationContext());
            } else if (selectedItem.equals("Hindi")) {
                lang = "hi";
                localeClass.changeLang(lang, getApplicationContext());
            } else if (selectedItem.equals("Kannada")) {
                lang = "ka";
                localeClass.changeLang(lang, getApplicationContext());
            }
        }
        selectedLanguage = selectedItem;
    }
    @Override
    protected void onResume() {
        //  android.os.Process.killProcess(android.os.Process.myPid());
        sessionManager.logoutUser();
        sessionManager.clearPreference(LanguageScreenActivity.this);
        trimCache(this);
        super.onResume();
    }

    public static void trimCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);

            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }


    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}

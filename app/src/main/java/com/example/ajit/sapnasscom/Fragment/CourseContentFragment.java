package com.example.ajit.sapnasscom.Fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.ajit.sapnasscom.Activity.NoInternetActivity;
import com.example.ajit.sapnasscom.Adapter.CourseContentAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.CourseContentModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;


public class CourseContentFragment extends Fragment {

    private RecyclerView rvCourse;
    private SessionManager sessionManager;
    private CourseContentModel courseModel;
    private ArrayList<CourseContentModel> arrCourse;
    private CourseContentAdapter courseAdapter;

    ProgressDialog pd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_course_content, container, false);

        rvCourse = (RecyclerView) v.findViewById(R.id.rc_course_content);

        pd = new ProgressDialog(getActivity());
        sessionManager = new SessionManager(getActivity());

        setAdapter();

        if (Common.isInternet(getActivity())) {
            new HttpAsyncTaskCourse().execute(CommonUrl.getCoursewithoutLogin);
        } else {
            noInternet(getActivity());
        }

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if (Common.isInternet(getActivity())) {
            new HttpAsyncTaskCourse().execute(CommonUrl.getCoursewithoutLogin);
        } else {
            Intent intent = new Intent(getActivity(), NoInternetActivity.class);
            startActivity(intent);
        }*/
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCourse.setLayoutManager(mLayoutManager);
        rvCourse.setItemAnimator(new DefaultItemAnimator());
    }

    private class HttpAsyncTaskCourse extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourse() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();

            arrCourse = new ArrayList<>();
            courseAdapter = new CourseContentAdapter(getActivity(), arrCourse);
        }

        protected String doInBackground(String... urls) {

            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            rvCourse.setAdapter(courseAdapter);
            Utilities.showLog(result);
            pd.dismiss();
        }
    }

    public String getCourse(String url) {
        String result = "";
        Log.e("Without login : ", url);
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes("");
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Log.e("COURSERESPONSE", response.toString());
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        courseModel = new CourseContentModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        courseModel.setCourseTitle(jsonObject.getString("CourseName"));
                        courseModel.setCourseTagname(jsonObject.getString("PrimarySkillsName"));
                        courseModel.setCourseContent(jsonObject.getString("CourseName"));
                        courseModel.setCourseCreater(jsonObject.getString("Creator"));

                        arrCourse.add(courseModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

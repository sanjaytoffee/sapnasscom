package com.example.ajit.sapnasscom.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.NetworkClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.example.ajit.sapnasscom.Utils.Utilities;
import org.json.JSONException;
import org.json.JSONObject;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class LoginActivity extends Activity implements View.OnClickListener {

    EditText editEmail, editPassword;
    public static Button loginButton;
    String mdHashing;
    String userId, userEmail, UserFullName, FirstName, LastName;
    SessionManager sessionManager;
    LinearLayout forgotPasswor;
    NetworkClass networkClass;
    public static TextView forgot_passward, textViewPasswordLabel, textViewEmailAddress, textViewEmailLogin;
    LocaleClass localeClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login_activity);

        networkClass = new NetworkClass(getApplicationContext());

        textFieldValidation();
        loginButton.setOnClickListener(this);
        forgotPasswor.setOnClickListener(this);
        mdHashing = computeMD5Hash(editPassword.getText().toString()).trim();

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "Login");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void textFieldValidation() {
        textViewEmailLogin = (TextView) findViewById(R.id.email_login);
        textViewEmailAddress = (TextView) findViewById(R.id.email_lbl);
        textViewPasswordLabel = (TextView) findViewById(R.id.password_lbl);
        editEmail = (EditText) findViewById(R.id.user_email);
        editPassword = (EditText) findViewById(R.id.user_password);
        loginButton = (Button) findViewById(R.id.login_button);
        forgot_passward = (TextView) findViewById(R.id.forgot_passward);
        forgotPasswor = (LinearLayout) findViewById(R.id.forgot_password);
        sessionManager = new SessionManager(getApplicationContext());
        editPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    submitLogin();
                }
                return false;
            }
        });
    }

    private void submitLogin() {
        if (editEmail.getText().toString().length() == 0) {
            Toast.makeText(this, "Please Enter Email", Toast.LENGTH_SHORT).show();
        } else if (editPassword.getText().toString().length() == 0) {
            Toast.makeText(this, "Please enter password.", Toast.LENGTH_SHORT).show();
        } else {
            if (Common.isInternet(LoginActivity.this)) {
                loginRequest();
            } else {
                noInternet(LoginActivity.this);
            }
        }
    }

    private void loginRequest() {
        RequestQueue queue = Volley.newRequestQueue(LoginActivity.this);
        String response = null;
        final String finalResponse = response;
        MyProgressDialog.showDialog(this);
        Utilities.showLog("Login=" + CommonUrl.Web_login + "ValidateRegisterUser?EmailID" + "=" + editEmail.getText().toString() + "&" + "Password" + "=" + computeMD5Hash(editPassword.getText().toString()).trim());
        StringRequest postRequest = new StringRequest(Request.Method.POST, CommonUrl.Web_login + "ValidateRegisterUser?EmailID" + "=" + editEmail.getText().toString() + "&" + "Password" + "=" + computeMD5Hash(editPassword.getText().toString()).trim(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            MyProgressDialog.hideDialog();
                            JSONObject jsonObject = new JSONObject(response);
                            Utilities.showLog("login=" + response);
                            Utilities.showLog("Response =" + jsonObject.getString("Response"));
                            Toast.makeText(LoginActivity.this, "" + jsonObject.getString("Response"), Toast.LENGTH_SHORT).show();
                            if (jsonObject.getString("Response").equals("Success")) {
                                userId = jsonObject.getString("Id");
                                userEmail = jsonObject.getString("Email");
                                UserFullName = jsonObject.getString("UserFullName");
                                FirstName = jsonObject.getString("FirstName");
                                LastName = jsonObject.getString("LastName");
                                PrefUtils.setUserID(LoginActivity.this, userId);
                                PrefUtils.setUserEmail(LoginActivity.this, userEmail);
                                PrefUtils.setUserLoginStatus(true);
                                PrefUtils.setUserFullName(LoginActivity.this, UserFullName);
                                PrefUtils.setUserFirstName(LoginActivity.this, FirstName);
                                PrefUtils.setUserLastName(LoginActivity.this, LastName);
                                PrefUtils.setUser_Profileurl(LoginActivity.this, CommonUrl.ImageUserProfileUrl + jsonObject.getString("Profilepath"));
                                String Name = FirstName + " " + LastName;
                                sessionManager.createEmail(jsonObject.getString("Email"), userId, Name);
                                if (jsonObject.getInt("LaguageId") == 0) {
                                    Intent intent = new Intent(LoginActivity.this, ProfileDetailsActivity.class);
                                    intent.putExtra("editProfile", "login");
                                    startActivity(intent);
                                } else {
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            } else {
                                Toast.makeText(LoginActivity.this, "" + jsonObject.getString("Response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                    }
                }
        ) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String credentials = "unnati" + ":" + "unnati@123";
                String base64EncodedCredentials = Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Authorization", "Basic " + base64EncodedCredentials);
                Log.e("Authorization : ", "Basic " + base64EncodedCredentials);
                return headers;
            }
        };
        postRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(postRequest);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                submitLogin();
                break;

            case R.id.forgot_password:
                Intent intent = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                startActivity(intent);
                break;
        }
    }

    public final String computeMD5Hash(final String pass) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(pass.getBytes());
            byte messageDigest[] = digest.digest();
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            System.out.println(hexString.toString());
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}

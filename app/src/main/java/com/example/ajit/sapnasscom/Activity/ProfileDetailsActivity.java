package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Adapter.DropDownListAdapter;
import com.example.ajit.sapnasscom.Adapter.Education_Adapter;
import com.example.ajit.sapnasscom.Adapter.Language_Adapter;
import com.example.ajit.sapnasscom.Adapter.Location_Adapter;
import com.example.ajit.sapnasscom.Adapter.Month_Adapter;
import com.example.ajit.sapnasscom.Adapter.PrefLocationDropDownListAdapter;
import com.example.ajit.sapnasscom.Adapter.Preferedlocation_Adapter;
import com.example.ajit.sapnasscom.Adapter.Skills_Adapter;
import com.example.ajit.sapnasscom.Adapter.Specialization_Adapter;
import com.example.ajit.sapnasscom.Adapter.Year_Adapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.CurrentLocation;
import com.example.ajit.sapnasscom.Model.Education;
import com.example.ajit.sapnasscom.Model.Language;
import com.example.ajit.sapnasscom.Model.MonthModel;
import com.example.ajit.sapnasscom.Model.PreferedLocation;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.Model.Specilization;
import com.example.ajit.sapnasscom.Model.YearModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.FileDownloader;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class ProfileDetailsActivity extends AppCompatActivity {

    Toolbar toolbar;
    public static Button save;
    public static TextView tv_tool_right, textViewExprence, textViewPreferredLocation, textViewCurrentLocation, textViewResumeLabel, textViewResumeName, textViewSkill, textViewLanguage, textViewEduation, tv_tool_left;
    Spinner education_spinner, specilization_spinner, language_spinner;
    Spinner skill_spinner, preferred_Location;
    LinearLayout dynamic_skill_layout, dynamic_location_layout;
    JSONArray skill_json_array, currentlocation_json_array, location_json_array;
    private ArrayList<Education> educationList;
    private ArrayList<Specilization> specilizationList;
    Education_Adapter education_adapter;
    Specialization_Adapter specialization_adapter;
    String getClicked = "gs";
    Education education;
    Specilization specilization;
    String file_size = "", specialization_id, month_name, Profile_path = "", Profile_name = "", SpecializationId, status, month_id, year_name, year_id, specialization_name, selectedLanguage, userId, skillName, education_name = "", education_id, skill_id, skill_name, language_id, language_name, PreferedLocationId, currentLocationId, strResult, userPass, userEmail, lastName, firstName;
    LinearLayout buttonUpload;
    Language language;
    ArrayList<Language> languageList;
    Language_Adapter language_adapter;

    ProgressDialog pd;
    Skills_Adapter skills_adapter;
    ArrayList<Skills> skillsArrayList;
    Skills skills;

    public static ArrayList<String> skillList;//Creating arraylist
    public static ArrayList<String> preferedList;//Creating arraylist

    CurrentLocation currentLocation;
    Spinner spinnerCurrentLocation, yearSpinner, monthSpinner;
    String location_id, location_name, prefered_location_id, prefered_location_name;

    ArrayList<CurrentLocation> locationArrayList;
    Location_Adapter location_adapter;
    SessionManager sessionManager;

    ArrayList<PreferedLocation> preferedLocationList;
    Preferedlocation_Adapter preferedlocation_adapter;
    PreferedLocation preferedLocation;

    ArrayList<YearModel> yearList;
    Year_Adapter year_adapter;
    YearModel yearModel;
    JSONArray jsonArray_pre, jsonArray_skill;
    ArrayList<MonthModel> monthList;
    Month_Adapter month_adapter;
    MonthModel monthModel;

    private PopupWindow pw, pw2;
    private boolean expanded, expanded2;        //to  store information whether the selected values are displayed completely or in shortened representatn
    public static boolean[] checkSelected;    // store select/unselect information about the values in the list
    public static boolean[] checkSelected2;    // store select/unselect information about the values in the list

    TextView tv, tv2;
    RelativeLayout spinnerPreferedL;
    LinearLayout layout, layout2;
    RelativeLayout layout1;
    Intent intent;
    LocaleClass localeClass;
    String lang = "en";
    private static final int REQUEST_WRITE_PERMISSION = 786;
    public static int REQUEST_CODE_DOC = 100;
    String encodedFile = "", lastVal = "";
    public static String fileName = "";
    String encodedImage, docFilePath;
    Bitmap pdf;
    StringBuffer profileResponse = null;
    String selectedSkill = "", selectedSkill2 = "";
    String selectedPrefLoc = "", selectedPrefLoc2 = "", selected = "";
    ImageView imPDF, imResume;
    private int PICK_PDF_REQUEST = 1;
    private static final int STORAGE_PERMISSION_CODE = 123;
    private Uri filePath;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signuptwo_activity);
        intent = getIntent();

        pd = new ProgressDialog(ProfileDetailsActivity.this);

        DropDownListAdapter.selectedCount = 0;
        PrefLocationDropDownListAdapter.selectedCount2 = 0;
        if (intent != null) {
            getClicked = intent.getStringExtra("editProfile");
        }
        jsonArray_pre = new JSONArray();
        jsonArray_skill = new JSONArray();
        layout1 = (RelativeLayout) findViewById(R.id.relativeLayout1);
        spinnerPreferedL = (RelativeLayout) findViewById(R.id.relativeLayout2);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.profile_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        skill_json_array = new JSONArray();
        location_json_array = new JSONArray();
        currentlocation_json_array = new JSONArray();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                onBackPressed();
            }
        });

        sessionManager = new SessionManager(ProfileDetailsActivity.this);
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "Profile Details");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        imResume = (ImageView) findViewById(R.id.imResume);
        imResume.setVisibility(View.GONE);
        imPDF = (ImageView) findViewById(R.id.imPDF);


        textViewEduation = (TextView) findViewById(R.id.txt_Eduation);
        textViewLanguage = (TextView) findViewById(R.id.txt_language);
        textViewSkill = (TextView) findViewById(R.id.txt_skill);
        textViewResumeName = (TextView) findViewById(R.id.resume_name);
        textViewResumeLabel = (TextView) findViewById(R.id.resume_label);
        textViewCurrentLocation = (TextView) findViewById(R.id.txt_current_location);
        textViewPreferredLocation = (TextView) findViewById(R.id.txt_prefred_location);
        textViewExprence = (TextView) findViewById(R.id.txt_exprence);
        buttonUpload = (LinearLayout) findViewById(R.id.upload_resume);
//        tv_tool_left = (TextView) findViewById(R.id.tv_tool_left);
        tv_tool_right = (TextView) findViewById(R.id.tv_tool_right);

        save = (Button) findViewById(R.id.save_data);
        spinnerCurrentLocation = (Spinner) findViewById(R.id.current_lication_spinner);
        //preferred_Location = (Spinner) findViewById(R.id.preferreg_loc);
        education_spinner = (Spinner) findViewById(R.id.education_spinner);
        specilization_spinner = (Spinner) findViewById(R.id.specilization_spinner);
        language_spinner = (Spinner) findViewById(R.id.language_spinner);
        //skill_spinner = (Spinner) findViewById(R.id.skill);
        yearSpinner = (Spinner) findViewById(R.id.year_spinner);
        dynamic_skill_layout = (LinearLayout) findViewById(R.id.dynamic_skill_layout);
        dynamic_location_layout = (LinearLayout) findViewById(R.id.dynamic_location_layout);
        monthSpinner = (Spinner) findViewById(R.id.month_sinner);

        educationList = new ArrayList<>();
        specilizationList = new ArrayList<>();
        skillList = new ArrayList<String>();
        preferedList = new ArrayList<String>();

        skillList.clear();
        preferedList.clear();

        requestPermission();

        HashMap<String, String> getid = sessionManager.getEmail();
        userId = getid.get(SessionManager.KEY_USER_ID);

        HashMap<String, String> langh = sessionManager.getLanguage();
        selectedLanguage = langh.get(SessionManager.KEY_LANGUAGE);

        if (getClicked.equalsIgnoreCase("editProfile")) {
            if (textViewResumeName.getText().length() == 0) {
                imPDF.setVisibility(View.GONE);
            } else {
                imPDF.setVisibility(View.VISIBLE);
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        } else {
            imPDF.setVisibility(View.GONE);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setHomeButtonEnabled(false);
        }

        if (Common.isInternet(ProfileDetailsActivity.this)) {
            new HttpAsyncTask_Education().execute(CommonUrl.drop_down + "GetDropdownList");
        } else {
            Toast.makeText(getApplicationContext(), "Please check your internet connection.", Toast.LENGTH_SHORT).show();
        }

        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                year_id = yearList.get(i).getID();
                year_name = yearList.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            yearSpinner.setPopupBackgroundResource(R.drawable.border_search_text);
        }

        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                month_id = monthList.get(i).getID();
                month_name = monthList.get(i).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            monthSpinner.setPopupBackgroundResource(R.drawable.border_search_text);
        }

        education_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                education_id = educationList.get(i).getID();
                education_name = educationList.get(i).getName();
                new HttpAsyncTask_Specilization().execute(CommonUrl.drop_down + "GetDropdownList", education_id);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            education_spinner.setPopupBackgroundResource(R.drawable.border_search_text);
        }

        specilization_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                specialization_name = specilizationList.get(i).getName();
                specialization_id = specilizationList.get(i).getID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            specilization_spinner.setPopupBackgroundResource(R.drawable.border_search_text);
        }

        language_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                language_name = languageList.get(i).getName();
                language_id = languageList.get(i).getID();
                if (language_name.equals("English")) {
                    lang = "en";
                    localeClass.changeLang(lang, getApplicationContext());
                } else if (language_name.equals("Hindi")) {
                    lang = "hi";
                    localeClass.changeLang(lang, getApplicationContext());
                } else if (language_name.equals("Kannada")) {
                    lang = "ka";
                    localeClass.changeLang(lang, getApplicationContext());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            language_spinner.setPopupBackgroundResource(R.drawable.border_search_text);
        }

        spinnerCurrentLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                location_name = locationArrayList.get(i).getName();
                location_id = locationArrayList.get(i).getID();

                if (!location_name.equalsIgnoreCase("Select Location")) {
                    LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                   /* TextView tv = new TextView(getApplicationContext());
                    tv.setLayoutParams(lparams);
                    location_name = location_name + ",";
                    tv.setText(location_name);
                    tv.setTextColor(Color.BLACK);
                    dynamic_currentlocation_layout.addView(tv);*/
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("CityId", location_id);
                        currentlocation_json_array.put(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            spinnerCurrentLocation.setPopupBackgroundResource(R.drawable.border_search_text);
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (education_name.equalsIgnoreCase("Select Degree")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Education", Toast.LENGTH_SHORT).show();
                } else if (specialization_name.equalsIgnoreCase("Specialization")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Specialization", Toast.LENGTH_SHORT).show();
                } else if (language_name.equalsIgnoreCase("Select")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Language", Toast.LENGTH_SHORT).show();
                } else if (tv.getText().toString().equalsIgnoreCase("Select Skill")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Skill", Toast.LENGTH_SHORT).show();
                } else if (tv.getText().toString().equalsIgnoreCase("")) {
                    if (selectedSkill2.equalsIgnoreCase("")) {
                        Toast.makeText(ProfileDetailsActivity.this, "Please Select Skill", Toast.LENGTH_SHORT).show();
                    }

                } else if (tv.getText().toString().equalsIgnoreCase("Select")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Skill", Toast.LENGTH_SHORT).show();
                } else if (location_name.equalsIgnoreCase("Select Location")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Location", Toast.LENGTH_SHORT).show();
                } else if (tv2.getText().toString().equalsIgnoreCase("Select Preferred Location")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Preferred Location", Toast.LENGTH_SHORT).show();
                } else if (tv2.getText().toString().equalsIgnoreCase("")) {
                    if (selectedPrefLoc2.equalsIgnoreCase("")) {
                        Toast.makeText(ProfileDetailsActivity.this, "Please Select Preferred Location", Toast.LENGTH_SHORT).show();
                    }
                } else if (tv2.getText().toString().equalsIgnoreCase("Select")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Preferred Location", Toast.LENGTH_SHORT).show();
                } else if (year_name.equalsIgnoreCase("Year")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Year", Toast.LENGTH_SHORT).show();
                } else if (month_name.equalsIgnoreCase("Month")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Please Select Month", Toast.LENGTH_SHORT).show();
                } else {
                    if (Common.isInternet(ProfileDetailsActivity.this)) {
                        try {
                            if (file_size.contains("MB")) {
                                String[] str = file_size.split("MB");

                                if (Double.valueOf(str[0]) <= 2.00) {
                                    new HttpAsyncTaskSaveProfile().execute(CommonUrl.Web_login + "SaveUserProfileDetails");
                                } else {
                                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.file_validation), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                new HttpAsyncTaskSaveProfile().execute(CommonUrl.Web_login + "SaveUserProfileDetails");
                            }
                        } catch (Exception e) {
                            new HttpAsyncTaskSaveProfile().execute(CommonUrl.Web_login + "SaveUserProfileDetails");
                        }
                    } else {
                        noInternet(ProfileDetailsActivity.this);
                    }
                }

                // } else if (getName(fileName)) {
//                    if (Common.isInternet(ProfileDetailsActivity.this)) {
//                        new HttpAsyncTaskSaveProfile().execute(CommonUrl.Web_login + "SaveUserProfileDetails");
//                    } else {
//                        noInternet(ProfileDetailsActivity.this);
//                    }
//                } else {
//                    Toast.makeText(ProfileDetailsActivity.this,"please upload pdf or doc file",Toast.LENGTH_SHORT).show();
//                }
            }
        });

        imResume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new HttpAsyncTaskDeleteResume().execute(CommonUrl.deleteresume);
            }
        });

        imPDF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Uri uri = Uri.parse(Profile_path);
                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(getApplicationContext(), "Address is not found.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        textViewResumeName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.isInternet(ProfileDetailsActivity.this)) {
                    if (!Profile_path.equalsIgnoreCase("")) {
                        new DownloadFile().execute(Profile_path);
                    }
                } else {
                    noInternet(ProfileDetailsActivity.this);
                }
            }
        });

        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDocument();
            }
        });
    }

    public boolean getName(String fileName) {
        String filePathInLowerCase = fileName.toLowerCase();
        if (filePathInLowerCase.endsWith(".pdf")) {
            return true;
        } else if (filePathInLowerCase.endsWith(".PDF")) {
            return true;
        } else if (filePathInLowerCase.endsWith(".doc")) {
            return true;
        } else if (filePathInLowerCase.endsWith(".docx")) {
            return true;
        } else return false;
    }

    private void initialize() {
        checkSelected = new boolean[skillsArrayList.size()];
        //initialize all values of list to 'unselected' initially
        for (int i = 0; i < checkSelected.length; i++) {
            checkSelected[i] = false;
        }

        tv = (TextView) findViewById(R.id.SelectBox);
        tv.setText("Select Skill");
        tv.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if (!expanded) {
                    //display all selected values

                    int flag = 0;
                    selectedSkill = "";
                    for (int i = 0; i < skillsArrayList.size(); i++) {
                        if (checkSelected[i] == true) {

                            selectedSkill += skillsArrayList.get(i).getName();
                            selectedSkill += ",";
                            flag = 1;
                        }
                    }
                    try {
                        StringBuilder sb = new StringBuilder(selectedSkill);
                        // System.out.println(sb.deleteCharAt(sb.length()-1));
                        selectedSkill = sb.deleteCharAt(sb.length() - 1).toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (flag == 1)
                        tv.setText(selectedSkill);
                    expanded = true;
                } else {
                    //display shortened representation of selected values
                    if (jsonArray_skill.length() != 0) {
                        selectedSkill2 = "";
                        for (int i = 0; i < skillsArrayList.size(); i++) {
                            for (int j = 0; j < jsonArray_skill.length(); j++) {
                                try {

                                    if (skillsArrayList.get(i).getID().equalsIgnoreCase(jsonArray_skill.get(j).toString())) {
                                        selectedSkill2 += skillsArrayList.get(i).getName();
                                        selectedSkill2 += ",";
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                        try {
                            StringBuilder sb = new StringBuilder(selectedSkill2);
                            // System.out.println(sb.deleteCharAt(sb.length()-1));
                            selectedSkill2 = sb.deleteCharAt(sb.length() - 1).toString();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        tv.setText(selectedSkill2);
                    } else {
                        tv.setText(DropDownListAdapter.getSelected());
                    }

                    expanded = false;
                }
            }
        });

        //onClickListener to initiate the dropDown list
        LinearLayout createButton = (LinearLayout) findViewById(R.id.create);

        layout1.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                initiatePopUp(skillsArrayList, tv, jsonArray_skill);
            }
        });
    }

    private void setPreferredLocationinitialize() {
        checkSelected2 = new boolean[preferedLocationList.size()];
        //initialize all values of list to 'unselected' initially
        for (int i = 0; i < checkSelected2.length; i++) {
            checkSelected2[i] = false;
        }

        tv2 = (TextView) findViewById(R.id.SelectBo2);
        tv2.setText("Select Preferred Location");
        tv2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!expanded2) {
                    //display all selected values
                    int flag = 0;
                    selectedPrefLoc2 = "";
                    for (int i = 0; i < preferedLocationList.size(); i++) {
                        if (checkSelected2[i] == true) {
                            selectedPrefLoc2 += preferedLocationList.get(i).getName();
                            selectedPrefLoc2 += ",";
                            flag = 1;
                        }
                    }
                    try {
                        StringBuilder sb = new StringBuilder(selectedPrefLoc2);
                        // System.out.println(sb.deleteCharAt(sb.length()-1));
                        selectedPrefLoc2 = sb.deleteCharAt(sb.length() - 1).toString();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (flag == 1)
                        tv2.setText(selectedPrefLoc2);
                    expanded2 = true;
                } else {
                    //display shortened representation of selected values

                    if (jsonArray_pre.length() != 0) {
                        selectedPrefLoc2 = "";
                        for (int i = 0; i < preferedLocationList.size(); i++) {
                            for (int j = 0; j < jsonArray_pre.length(); j++) {
                                try {

                                    if (preferedLocationList.get(i).getId().equalsIgnoreCase(jsonArray_pre.get(j).toString())) {
                                        selectedPrefLoc2 += preferedLocationList.get(i).getName();
                                        selectedPrefLoc2 += ",";
                                    }


                                } catch (JSONException e) {

                                }
                            }
                        }
                        try {
                            StringBuilder sb = new StringBuilder(selectedPrefLoc2);
                            // System.out.println(sb.deleteCharAt(sb.length()-1));
                            selectedPrefLoc2 = sb.deleteCharAt(sb.length() - 1).toString();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        tv2.setText(selectedPrefLoc2);
                    } else {
                        tv2.setText(PrefLocationDropDownListAdapter.getSelected());
                    }
                    expanded2 = false;
                }
            }
        });

        //onClickListener to initiate the dropDown list
        LinearLayout createButton2 = (LinearLayout) findViewById(R.id.create2);
        spinnerPreferedL.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub

                setPreferredLocationinitiatePopUp(preferedLocationList, tv2, jsonArray_pre);
            }
        });
    }

    private void initiatePopUp(ArrayList<Skills> skillsArrayList, TextView tv, JSONArray jsonArray_skill) {
        LayoutInflater inflater = (LayoutInflater) ProfileDetailsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //get the pop-up window i.e.  drop-down layout
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.pop_up_window, (ViewGroup) findViewById(R.id.PopUpView));

        //get the view to which drop-down layout is to be anchored

        pw = new PopupWindow(layout, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        //Pop-up window background cannot be null if we want the pop-up to listen touch events outside its window
        pw.setBackgroundDrawable(new BitmapDrawable());
        pw.setTouchable(true);

        //let pop-up be informed about touch events outside its window. This  should be done before setting the content of pop-up
        pw.setOutsideTouchable(true);
        pw.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        //dismiss the pop-up i.e. drop-down when touched anywhere outside the pop-up
        pw.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    pw.dismiss();
                    return true;
                }
                return false;
            }
        });

        //provide the source layout for drop-down
        pw.setContentView(layout);

        //anchor the drop-down to bottom-left corner of 'layout1'
        pw.showAsDropDown(layout1);

        //populate the drop-down list
        final ListView list = (ListView) layout.findViewById(R.id.dropDownList);
        DropDownListAdapter adapter = new DropDownListAdapter(this, skillsArrayList, tv, jsonArray_skill);
        list.setAdapter(adapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            list.setBackgroundResource(R.drawable.border_search_text);
        }
    }

    private void setPreferredLocationinitiatePopUp(ArrayList<PreferedLocation> preferedLocationList, TextView tv, JSONArray jsonArray_pre) {
        LayoutInflater inflater = (LayoutInflater) ProfileDetailsActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //get the pop-up window i.e.  drop-down layout
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.prelocation_pop_up_window, (ViewGroup) findViewById(R.id.PopUpView));

        //get the view to which drop-down layout is to be anchored

        pw2 = new PopupWindow(layout, ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        //Pop-up window background cannot be null if we want the pop-up to listen touch events outside its window
        pw2.setBackgroundDrawable(new BitmapDrawable());
        pw2.setTouchable(true);

        //let pop-up be informed about touch events outside its window. This  should be done before setting the content of pop-up
        pw2.setOutsideTouchable(true);
        pw2.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        //dismiss the pop-up i.e. drop-down when touched anywhere outside the pop-up
        pw2.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    pw2.dismiss();
                    return true;
                }
                return false;
            }
        });

        //provide the source layout for drop-down
        pw2.setContentView(layout);

        //anchor the drop-down to bottom-left corner of 'layout1'
        pw2.showAsDropDown(spinnerPreferedL);

        //populate the drop-down list
        final ListView list = (ListView) layout.findViewById(R.id.dropDownList2);
        PrefLocationDropDownListAdapter adapter = new PrefLocationDropDownListAdapter(this, preferedLocationList, tv, jsonArray_pre);
        list.setAdapter(adapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            list.setBackgroundResource(R.drawable.border_search_text);
        }
    }


    private class HttpAsyncGetProfile extends AsyncTask<String, Void, String> {
        private HttpAsyncGetProfile() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {

            return getchProfileData(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            MyProgressDialog.hideDialog();
            selectedPrefLoc = "";
            selected = "";
            try {

                JSONObject jsonoObject = new JSONObject(profileResponse.toString());

                SpecializationId = jsonoObject.getString("SpecializationId");

                if (!jsonoObject.getString("CV").equalsIgnoreCase("null")) {
                    Profile_path = CommonUrl.IMAGE + jsonoObject.getString("CV");
                    Profile_name = Profile_path.substring(Profile_path.lastIndexOf("\\") + 1);
                    textViewResumeName.setText("" + Profile_name);
                    textViewResumeName.setTextColor(Color.BLUE);
                    textViewResumeName.setPaintFlags(textViewResumeName.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                    Log.e("Profile_name", "" + Profile_name);

                    if (!encodedFile.equalsIgnoreCase("")) {
                        imResume.setVisibility(View.GONE);
                        imPDF.setVisibility(View.GONE);
                    } else {
                        imResume.setVisibility(View.VISIBLE);
                        imPDF.setVisibility(View.VISIBLE);
                    }
                }

                new HttpAsyncTask_Specilization().execute(CommonUrl.drop_down + "GetDropdownList", jsonoObject.getString("DegreeId"));

                for (int i = 0; i < educationList.size(); i++) {
                    if (educationList.get(i).getID().equalsIgnoreCase(jsonoObject.getString("DegreeId"))) {
                        education_spinner.setSelection(i);
                    }
                }

                for (int i = 0; i < languageList.size(); i++) {
                    if (languageList.get(i).getID().equalsIgnoreCase(jsonoObject.getString("LaguageId"))) {
                        language_spinner.setSelection(i);
                    }
                }

                for (int i = 0; i < yearList.size(); i++) {
                    if (yearList.get(i).getID().equalsIgnoreCase(jsonoObject.getString("Year"))) {
                        yearSpinner.setSelection(i);
                    }
                }

                for (int i = 0; i < monthList.size(); i++) {
                    if (monthList.get(i).getID().equalsIgnoreCase(jsonoObject.getString("Month"))) {
                        monthSpinner.setSelection(i);
                    }
                }

                JSONArray jsonArray = jsonoObject.getJSONArray("CurrentLocId");
                String location = jsonArray.get(0).toString();
                for (int i = 0; i < locationArrayList.size(); i++) {
                    if (locationArrayList.get(i).getID().equalsIgnoreCase(location)) {
                        spinnerCurrentLocation.setSelection(i);
                    }
                }
                jsonArray_pre = jsonoObject.getJSONArray("PreferredLocId");
                jsonArray_skill = jsonoObject.getJSONArray("PrimarySkills");

                for (int i = 0; i < jsonArray_skill.length(); i++) {
                    String string = jsonArray_skill.get(i).toString();
                    for (int k = 0; k < skillsArrayList.size(); k++) {
                        if (skillsArrayList.get(k).getID().equalsIgnoreCase(string)) {
                            selected += skillsArrayList.get(k).getName();
                            selected += ",";
                        }
                    }
                }
                try {
                    StringBuilder sb = new StringBuilder(selected);
                    // System.out.println(sb.deleteCharAt(sb.length()-1));
                    selected = sb.deleteCharAt(sb.length() - 1).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tv.setText(selected);

                for (int i = 0; i < skillsArrayList.size(); i++) {
                    for (int j = 0; j < jsonArray_skill.length(); j++) {
                        if (skillsArrayList.get(i).getID().equalsIgnoreCase(jsonArray_skill.get(j).toString())) {
                            skillList.add(jsonArray_skill.get(j).toString());
                        }
                    }
                }

                for (int i = 0; i < preferedLocationList.size(); i++) {
                    for (int j = 0; j < jsonArray_pre.length(); j++) {
                        if (preferedLocationList.get(i).getId().equalsIgnoreCase(jsonArray_pre.get(j).toString())) {
                            preferedList.add(jsonArray_pre.get(j).toString());
                        }
                    }
                }

                for (int i = 0; i < jsonArray_pre.length(); i++) {
                    String string = jsonArray_pre.get(i).toString();
                    for (int k = 0; k < preferedLocationList.size(); k++) {
                        if (preferedLocationList.get(k).getId().equalsIgnoreCase(string)) {
                            selectedPrefLoc += preferedLocationList.get(k).getName();
                            selectedPrefLoc += ",";
                        }
                    }
                }
                try {
                    StringBuilder sb2 = new StringBuilder(selectedPrefLoc);
                    // System.out.println(sb.deleteCharAt(sb.length()-1));
                    selectedPrefLoc = sb2.deleteCharAt(sb2.length() - 1).toString();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tv2.setText(selectedPrefLoc);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getchProfileData(String url) {
        String result = "";

        try {
            JSONObject params = new JSONObject();
            params.put("Id", userId);

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";

            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                profileResponse = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            profileResponse.append(line);
            profileResponse.append(TokenParser.CR);
            if (profileResponse != null) {

                Log.e("GETPROFILE", profileResponse.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private class HttpAsyncTaskSaveProfile extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskSaveProfile() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(ProfileDetailsActivity.this);
            skillList = new ArrayList<>();
            if (jsonArray_skill.length() != 0) {
                for (int j = 0; j < jsonArray_skill.length(); j++) {
                    try {
                        skillList.add(jsonArray_skill.get(j).toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            for (int i = 0; i < skillsArrayList.size(); i++) {
                if (checkSelected[i] == true) {
                    skillList.add(skillsArrayList.get(i).getID());
                }
            }

            preferedList = new ArrayList<>();
            if (jsonArray_pre.length() != 0) {
                for (int j = 0; j < jsonArray_pre.length(); j++) {
                    try {
                        preferedList.add(jsonArray_pre.get(j).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            for (int i = 0; i < preferedLocationList.size(); i++) {
                if (checkSelected2[i] == true) {
                    preferedList.add(preferedLocationList.get(i).getId());
                }
            }

        }

        public String doInBackground(String... urls) {

            return fetchData(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MyProgressDialog.hideDialog();
            file_size = "";
            if (status.equalsIgnoreCase("Success")) {
                if (getClicked.equalsIgnoreCase("editProfile")) {
                    Toast.makeText(ProfileDetailsActivity.this, "Profile updated successfully", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), SelectInterestActivity.class);
                    intent.putExtra("editProfile", "editProfile");
                    startActivity(intent);
//                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                    Toast.makeText(ProfileDetailsActivity.this, "Profile saved successfully", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), SelectInterestActivity.class);
                    intent.putExtra("editProfile", "login");
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
////                    finish();
                }

            } else {

                Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
            }
        }
    }

    public String fetchData(String url) {
        String result = "";

        try {
            JSONObject params = new JSONObject();
            params.put("Id", "");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("UserId", userId);
            jsonObject.put("DegreeId", education_id);
            jsonObject.put("SpecializationId", specialization_id);
            jsonObject.put("LaguageId", language_id);
            jsonObject.put("Year", year_id);
            jsonObject.put("Month", month_id);
            params.put("UserProfile", jsonObject);
            JSONObject pdfObj = new JSONObject();
            pdfObj.put("DocumentName", fileName);
            pdfObj.put("Document", encodedFile);
            params.put("clsDocument", pdfObj);

            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < skillList.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("PrimarySkills", skillList.get(i));
                jsonArray.put(jsonObject1);
            }
            params.put("UserSkillsList", jsonArray);

            JSONArray locationArray = new JSONArray();
            JSONObject locationObject = new JSONObject();
            {
                locationObject.put("CityId", location_id);
                locationArray.put(locationObject);
            }
            params.put("UserCurrentLocationList", locationArray);

            JSONArray preferedLocationArray = new JSONArray();

            for (int i = 0; i < preferedList.size(); i++) {
                JSONObject preferedLocationObject = new JSONObject();
                preferedLocationObject.put("CityId", preferedList.get(i));
                preferedLocationArray.put(preferedLocationObject);
            }
            params.put("UserPreferredLocationList", preferedLocationArray);

            Log.e("SAVEDATA", String.valueOf(params));
            Log.e("SAVEDATAURL", url);

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONObject jsonoObject = new JSONObject(response.toString());
                    status = jsonoObject.getString("Result");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Skils extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchSkill(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                initialize();
                new HttpAsyncTask_CurrentLocation().execute(CommonUrl.drop_down + "GetDropdownList");
            }
        }
    }

    public String fetchSkill(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "PRIMARYSKILLS");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                skillsArrayList = new ArrayList<>();
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {
                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        skills = new Skills();
                        skills.setID(catObj.getString("ID"));
                        skills.setCode(catObj.getString("Code"));
                        skills.setName(catObj.getString("Name"));
                        skillsArrayList.add(skills);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Education extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(ProfileDetailsActivity.this);
        }

        protected String doInBackground(String... urls) {
            return fetchEducation(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                education_adapter = new Education_Adapter(ProfileDetailsActivity.this, educationList);
                education_spinner.setAdapter(education_adapter);
                new HttpAsyncTask_Language().execute(CommonUrl.drop_down + "GetAllLanguage");
                new HttpAsyncTask_Specilization().execute(CommonUrl.drop_down + "GetDropdownList", education_id);
            }
        }
    }

    public String fetchEducation(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "DEGREE");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                educationList = new ArrayList<>();
                education = new Education();
                education.setName("Select Degree");
                education.setID("0");
                educationList.add(education);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {
                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        education = new Education();
                        education.setID(catObj.getString("ID"));
                        education.setCode(catObj.getString("Code"));
                        education.setName(catObj.getString("Name"));
                        educationList.add(education);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Specilization extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchSpecilization(urls[0], urls[1]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                specialization_adapter = new Specialization_Adapter(ProfileDetailsActivity.this, specilizationList);
                specilization_spinner.setAdapter(specialization_adapter);

                if (specilizationList.size() != 0) {
                    for (int i = 0; i < specilizationList.size(); i++) {
                        if (specilizationList.get(i).getID().equalsIgnoreCase(SpecializationId)) {
                            specilization_spinner.setSelection(i);
                        }
                    }
                }
            }
        }
    }

    public String fetchSpecilization(String url, String education_id) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "SPECIALIZATION");
            params.put("KeyOne", education_id);
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                specilizationList = new ArrayList<>();
                specilization = new Specilization();
                specilization.setName("Specialization");
                specilization.setID("0");
                specilizationList.add(specilization);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {
                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        specilization = new Specilization();
                        specilization.setID(catObj.getString("ID"));
                        specilization.setCode(catObj.getString("Code"));
                        specilization.setName(catObj.getString("Name"));

                        specilizationList.add(specilization);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Language extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchLanguage(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                language_adapter = new Language_Adapter(ProfileDetailsActivity.this, languageList);
                language_spinner.setAdapter(language_adapter);
                new HttpAsyncTask_Skils().execute(CommonUrl.drop_down + "GetDropdownList");

                if (languageList.size() != 0 && selectedLanguage != null) {
                    for (int i = 0; i < languageList.size(); i++) {
                        if (languageList.get(i).getName().equalsIgnoreCase(selectedLanguage)) {
                            language_spinner.setSelection(i);
                        }
                    }
                }
            }
        }
    }

    public String fetchLanguage(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "LANGUAGE");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                languageList = new ArrayList<>();
                language = new Language();
                language.setName("Select");
                language.setID("0");
                languageList.add(language);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        language = new Language();

                        language.setID(catObj.getString("ID"));
                        language.setCode(catObj.getString("Code"));
                        language.setName(catObj.getString("Name"));
                        languageList.add(language);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_CurrentLocation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchCurrentLocation(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                new HttpAsyncTask_PreferedLocation().execute(CommonUrl.drop_down + "GetDropdownList");
                location_adapter = new Location_Adapter(ProfileDetailsActivity.this, locationArrayList);
                spinnerCurrentLocation.setAdapter(location_adapter);
            }
        }
    }

    public String fetchCurrentLocation(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "INDIACITY");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                locationArrayList = new ArrayList<>();
                currentLocation = new CurrentLocation();
                currentLocation.setName("Select Location");
                currentLocation.setID("0");
                locationArrayList.add(currentLocation);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        currentLocation = new CurrentLocation();
                        currentLocation.setID(catObj.getString("ID"));
                        currentLocation.setCode(catObj.getString("Code"));
                        currentLocation.setName(catObj.getString("Name"));
                        locationArrayList.add(currentLocation);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_PreferedLocation extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchPreferedLocation(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                new HttpAsyncTask_Year().execute(CommonUrl.drop_down + "GetDropdownList");
                setPreferredLocationinitialize();
            }
        }
    }

    public String fetchPreferedLocation(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "INDIACITY");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                preferedLocationList = new ArrayList<>();

                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }


            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        preferedLocation = new PreferedLocation();
                        preferedLocation.setId(catObj.getString("ID"));
                        preferedLocation.setCode(catObj.getString("Code"));
                        preferedLocation.setName(catObj.getString("Name"));

                        preferedLocationList.add(preferedLocation);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Month extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchMonth(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                if (getClicked.equalsIgnoreCase("editProfile")) {
                    new HttpAsyncGetProfile().execute(CommonUrl.editProfileDataById);
                } else {
                    MyProgressDialog.hideDialog();
                }
                month_adapter = new Month_Adapter(ProfileDetailsActivity.this, monthList);
                monthSpinner.setAdapter(month_adapter);
            }
        }
    }

    public String fetchMonth(String url) {
        String result = "";
        Log.e("MONTHURL : ", "" + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "MONTH");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            Log.e("MONTHPARAM : ", "" + params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                monthList = new ArrayList<>();
                monthModel = new MonthModel();
                monthModel.setName("Month");
                monthModel.setID("0");
                monthList.add(monthModel);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        monthModel = new MonthModel();

                        monthModel.setID(catObj.getString("ID"));
                        monthModel.setCode(catObj.getString("Code"));
                        monthModel.setName(catObj.getString("Name"));
                        monthList.add(monthModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class HttpAsyncTask_Year extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchYear(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                new HttpAsyncTask_Month().execute(CommonUrl.drop_down + "GetDropdownList");

                year_adapter = new Year_Adapter(ProfileDetailsActivity.this, yearList);
                yearSpinner.setAdapter(year_adapter);
            }
        }
    }

    public String fetchYear(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "YEAR");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                yearList = new ArrayList<>();
                yearModel = new YearModel();
                yearModel.setName("Year");
                yearModel.setID("0");
                yearList.add(yearModel);
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject catObj = jsonArray.getJSONObject(i);
                        yearModel = new YearModel();

                        yearModel.setID(catObj.getString("ID"));
                        yearModel.setCode(catObj.getString("Code"));
                        yearModel.setName(catObj.getString("Name"));

                        yearList.add(yearModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private void getDocument() {
        try {
            Intent intent = new Intent();
            intent.setType("application/pdf");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PICK_PDF_REQUEST);
        } catch (Exception e) {
            Toast.makeText(this, "You can't select", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int req, int result, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(req, result, data);
        if (result == RESULT_OK) {
            Uri fileuri = data.getData();
            docFilePath = getFileNameByUri(this, fileuri);
            getFileName(data.getData());

            try {
                pdf = getBitmapFromUri(fileuri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor =
                getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap image = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return image;
    }

    private String getFileNameByUri(Context context, Uri uri) {
        String filepath = "";//default fileName
        File file;
        if (uri.getScheme().toString().compareTo("content") == 0) {
            Cursor cursor = context.getContentResolver().query(uri, new String[]{MediaStore.Images.ImageColumns.DATA, MediaStore.Images.Media.ORIENTATION}, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String mImagePath = cursor.getString(column_index);
            cursor.close();
            filepath = mImagePath;
        } else if (uri.getScheme().compareTo("filefile") == 0) {
            try {
                file = new File(new URI(uri.toString()));
                if (file.exists())
                    filepath = file.getAbsolutePath();
                getStringFile(file);
            } catch (URISyntaxException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            filepath = uri.getPath();
        }
        return filepath;
    }

    public void getFileName(Uri uri) {
        try {
            Log.e("URI", "" + uri.toString());

            if (uri.toString().contains("/storage")) {
                String fileNames = uri.getLastPathSegment();
                String path = fileNames;//it contain your path of image..im using a temp string..
                fileName = path.substring(path.lastIndexOf("/") + 1);

                if (getName(fileName)) {
                    textViewResumeName.setText("" + fileName);
                    try {
                        File file = new File(new URI(uri.toString()));
                        file_size = getFileSize(file);
                        getStringFile(file);
                    } catch (URISyntaxException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please select only .pdf,.doc and .docx file.", Toast.LENGTH_SHORT).show();
                }

                Log.e("FileName & Path", "" + fileName + "," + path + "," + file_size);
            } else {
                Cursor returnCursor =
                        getContentResolver().query(uri, null, null, null, null);

                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);


                returnCursor.moveToFirst();
                file_size = String.valueOf(Long.toString(returnCursor.getLong(sizeIndex)));
                fileName = returnCursor.getString(nameIndex);

                textViewResumeName.setText(returnCursor.getString(nameIndex));
                docFilePath = getPath(getApplicationContext(), uri);
                //docFilePath = GoogleDrivepath( uri);
                Log.e("FileName & Path", "" + fileName + "," + docFilePath + "," + file_size);

                if (getName(fileName)) {
                    textViewResumeName.setText(returnCursor.getString(nameIndex));
                    try {
                        File file = new File(docFilePath);
                        file_size = getFileSize(file);
                        getStringFile(file);
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        docFilePath = GoogleDrivepath(uri);
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please select only .pdf,.doc and .docx file", Toast.LENGTH_SHORT).show();
                }
                Log.e("FileName & Path", "" + fileName + "," + docFilePath + "," + file_size);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static final DecimalFormat format = new DecimalFormat("#.##");
    private static final long MB = 1024 * 1024;
    private static final long KB = 1024;

    public String getFileSize(File file) {

        if (!file.isFile()) {
            throw new IllegalArgumentException("Expected a file");
        }
        final double length = file.length();

        if (length > MB) {
            return format.format(length / MB) + " MB";
        }
        if (length > KB) {
            return format.format(length / KB) + " KB";
        }
        return format.format(length) + " B";
    }

    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    if ("primary".equalsIgnoreCase(type)) {
                        return Environment.getExternalStorageDirectory() + "/" + split[1];
                    }

                    // TODO handle non-primary volumes
                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {

                    String id = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        id = DocumentsContract.getDocumentId(uri);
                    }
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                } else if (isDriveDocument(uri)) {

                    String id = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        id = "" + 237;
                    }
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://Android/data/com.google.android.apps.docs/files"), Long.valueOf(id));

                    return getDataColumn(context, contentUri, null, null);
                }

                // MediaProvider
                else if (isMediaDocument(uri)) {
                    String docId = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        docId = DocumentsContract.getDocumentId(uri);
                    }
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {

                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isDriveDocument(Uri uri) {
        return "com.google.android.apps.docs.storage".equals(uri.getAuthority());
    }

    public String getStringFile(File f) {
        InputStream inputStream = null;
        encodedFile = "";
        try {
            inputStream = new FileInputStream(f.getAbsolutePath());
            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile = output.toString();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        return lastVal;
    }

    private String GoogleDrivepath(Uri _uri) {

        String filePath = null;

        Log.d("", "URI = " + _uri);
        if (_uri != null && "content".equals(_uri.getScheme())) {
            Cursor cursor = this.getContentResolver().query(_uri, new String[]{MediaStore.Files.FileColumns.DATA}, null, null, null);
            cursor.moveToFirst();
            filePath = cursor.getString(0);
            cursor.close();
        } else {
            filePath = _uri.getPath();
        }
        Log.d("", "Chosen path = " + filePath);
        return filePath;
    }

    private Boolean checkOtherFileType(String filePath) {
        String filePathInLowerCase = filePath.toLowerCase();
        if (filePathInLowerCase.endsWith(".pdf")) {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

        }
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
        }
    }

    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf

            Utilities.showLog("wrightid=" + Profile_name);
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "CodeUnnatiPdf");
            folder.mkdir();
            File pdfFile = new File(folder, Profile_name);

            try {
                pdfFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Common.showApplyDialog(ProfileDetailsActivity.this, "Resume downloaded successfully.");
        }
    }

    @Override
    protected void onResume() {
        DropDownListAdapter.selectedCount = 0;
        PrefLocationDropDownListAdapter.selectedCount2 = 0;
        super.onResume();
    }

    class HttpAsyncTaskDeleteResume extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {
            return getResumeData(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                fileName = "";
                encodedFile = "";
                Profile_path = "";
                textViewResumeName.setText("");
                imPDF.setVisibility(View.GONE);
                imResume.setVisibility(View.GONE);
            }
            pd.dismiss();
        }
    }

    public String getResumeData(String url) {
        String result = "";
        Log.e("DELETECV", url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", userId);

            Log.e("DELETEPARAMS", params.toString());
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {
                Log.e("DELETERESULTS", response.toString());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
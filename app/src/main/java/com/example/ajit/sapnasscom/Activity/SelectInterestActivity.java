package com.example.ajit.sapnasscom.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Adapter.CustomAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Model.Skills;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

public class SelectInterestActivity extends AppCompatActivity {

    GridView gridview;
    ArrayList<Skills> skillsArrayList, skillarray;
    Skills skills;
    public static TextView tv_tool_skip, tv_tool_right;
    JSONObject catObj;
    Toolbar toolbar;
    String getResponse;
    final Context context = this;
    CustomAdapter adapter;
    Intent intent;
    String getClicked;
    public static JSONArray jsonArray,jsonArray2;
    TextView tv_tool_left;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.grid_activity);
        intent = getIntent();
        if (intent != null) {
            getClicked = intent.getStringExtra("editProfile");
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.interesr_title));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        jsonArray = new JSONArray();

        pd = new ProgressDialog(SelectInterestActivity.this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                onBackPressed();
            }
        });

        gridview = (GridView) findViewById(R.id.customgrid);
        tv_tool_right = (TextView) findViewById(R.id.tv_tool_right);
        tv_tool_skip = (TextView) findViewById(R.id.tv_tool_skip);
        tv_tool_left = (TextView) findViewById(R.id.tv_tool_left);

        tv_tool_skip.setVisibility(View.VISIBLE);
        tv_tool_right.setVisibility(View.VISIBLE);

        tv_tool_right.setText(getResources().getString(R.string.Save));
        tv_tool_skip.setText(getResources().getString(R.string.skip));

        new HttpAsyncTask_Skils().execute(CommonUrl.getSelectIntrest);

        tv_tool_right.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (jsonArray.length() < 1) {
                    Toast.makeText(SelectInterestActivity.this, "Please select at least 1 interest.", Toast.LENGTH_SHORT).show();
                } else {
                    new SaveInterest().execute(CommonUrl.setSelectIntrest);
                }
            }
        });

        tv_tool_skip.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (getClicked.equalsIgnoreCase("editProfile")) {
                    Intent intent = new Intent(SelectInterestActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    PrefUtils.setUserLoginStatus(true);
                    Intent intent = new Intent(SelectInterestActivity.this, MainActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    class HttpAsyncTask_Skils extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {
            return fetchSkill(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                adapter = new CustomAdapter(getApplicationContext(), skillsArrayList);
                gridview.setAdapter(adapter);
                pd.dismiss();
            }
        }
    }

    public String fetchSkill(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("ID", PrefUtils.getUserID(SelectInterestActivity.this));
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                skillsArrayList = new ArrayList<>();
                skillarray = new ArrayList<>();
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {
                try {
                    final JSONArray jsonArrays = new JSONArray(response.toString());

                    for (int i = 0; i < jsonArrays.length(); i++) {
                        catObj = jsonArrays.getJSONObject(i);
                        skills = new Skills();

                        skills.setName(catObj.getString("PrimarySkillsName"));
                        skills.setPrimarySkills(catObj.getString("PrimarySkills"));

                        JSONArray jArray = catObj.getJSONArray("UserSecondarySkillsLists");
                        skillarray = new ArrayList<>();
                        if (jArray.length() != 0) {
                            for (int j = 0; j < jArray.length(); j++) {
                                JSONObject catObj2 = jArray.getJSONObject(j);
                                Skills skills = new Skills();
                                skills.setName(catObj2.getString("SkillsName"));
                                skills.setID(catObj2.getString("Id"));
                                skills.setPreSelected(catObj2.getString("isPreferred"));
                                skills.setPrimarySkillsId(catObj2.getString("MainSkillsId"));
                                skillarray.add(skills);
                                if (catObj2.getString("isPreferred").equalsIgnoreCase("1")) {
                                    try {
                                        JSONObject json = new JSONObject();
                                        json.put("Id", "0");
                                        json.put("UserId", PrefUtils.getUserID(context));
                                        json.put("PrimarySkills", catObj2.getString("MainSkillsId"));
                                        json.put("SecondarySkills", catObj2.getString("Id"));
                                        jsonArray.put(json);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                        skills.setSecondryList(skillarray);
                        skillsArrayList.add(skills);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    class SaveInterest extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {
            return SaveInterest(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {
                pd.dismiss();
                if (getResponse.equals("Success")) {
                    Utilities.showLog("response" + getResponse);
                    if (getClicked.equalsIgnoreCase("editProfile")) {
                     //   Common.showUpdatedProfileDialog(SelectInterestActivity.this);
                        Intent intent = new Intent(SelectInterestActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        PrefUtils.setUserLoginStatus(true);
                        Intent intent = new Intent(SelectInterestActivity.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                }
            }
        }
    }

    public String SaveInterest(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserSkillsLists", jsonArray);
            Log.e("SAVESELECT", "" + jsonArray.toString());
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                skillsArrayList = new ArrayList<>();
                skillarray = new ArrayList<>();
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {

                try {
                    JSONObject json = new JSONObject(line);
                    getResponse = json.getString("Result");

                    // Log.d("Resonse",""+json.getString("Result"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


}
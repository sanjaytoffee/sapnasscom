package com.example.ajit.sapnasscom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Model.Language;
import com.example.ajit.sapnasscom.R;
import java.util.ArrayList;

/**
 * Created by DTP-110 on 1/27/2017.
 */

public class Language_Adapter extends BaseAdapter
{

    Activity mActivity;
    ArrayList<Language> languageList;

    public Language_Adapter(Activity a, ArrayList<Language> data) {
        this.mActivity = a;
        this.languageList = data;
    }

    @Override
    public int getCount() {
        return languageList.size();
    }

    @Override
    public Object getItem(int position) {
        return languageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.language_text, null);

            viewHolder.language_text = (TextView) convertView
                    .findViewById(R.id.language_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (languageList != null) {
            try {
                viewHolder.language_text.setText("" + languageList.get(position).getName());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView language_text;
    }

}



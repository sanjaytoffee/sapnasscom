package com.example.ajit.sapnasscom.Model;

/**
 * Created by pratik on 2/20/2018.
 */

public class DiscoverJobModel {
    public boolean isApplied() {
        return Applied;
    }

    public void setApplied(boolean applied) {
        Applied = applied;
    }

    public boolean Applied;
    double Id;
    String Profile;

    public double getId() {
        return Id;
    }

    public void setId(double id) {
        Id = id;
    }

    public String getProfile() {
        return Profile;
    }

    public void setProfile(String profile) {
        Profile = profile;
    }

    public String getComapnyName() {
        return ComapnyName;
    }

    public void setComapnyName(String comapnyName) {
        ComapnyName = comapnyName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public double getCountryId() {
        return CountryId;
    }

    public void setCountryId(double countryId) {
        CountryId = countryId;
    }

    public double getStateId() {
        return StateId;
    }

    public void setStateId(double stateId) {
        StateId = stateId;
    }

    public double getCityId() {
        return CityId;
    }

    public void setCityId(double cityId) {
        CityId = cityId;
    }



    public double getPrimarySkills() {
        return PrimarySkills;
    }

    public void setPrimarySkills(double primarySkills) {
        PrimarySkills = primarySkills;
    }

    public int getExperinceMin() {
        return ExperinceMin;
    }

    public void setExperinceMin(int experinceMin) {
        ExperinceMin = experinceMin;
    }

    public int getExperinceMax() {
        return ExperinceMax;
    }

    public void setExperinceMax(int experinceMax) {
        ExperinceMax = experinceMax;
    }

    public double getIndustryId() {
        return IndustryId;
    }

    public void setIndustryId(double industryId) {
        IndustryId = industryId;
    }

    public String getFuncationalArea() {
        return FuncationalArea;
    }

    public void setFuncationalArea(String funcationalArea) {
        FuncationalArea = funcationalArea;
    }

    public String getRoleCatorgory() {
        return RoleCatorgory;
    }

    public void setRoleCatorgory(String roleCatorgory) {
        RoleCatorgory = roleCatorgory;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public String getEmployerType() {
        return EmployerType;
    }

    public void setEmployerType(String employerType) {
        EmployerType = employerType;
    }

    public boolean isActive() {
        return IsActive;
    }

    public void setActive(boolean active) {
        IsActive = active;
    }

    public String getLstJobsKillsEntity() {
        return lstJobsKillsEntity;
    }

    public void setLstJobsKillsEntity(String lstJobsKillsEntity) {
        this.lstJobsKillsEntity = lstJobsKillsEntity;
    }

    public String getPrimarySkillsName() {
        return PrimarySkillsName;
    }

    public void setPrimarySkillsName(String primarySkillsName) {
        PrimarySkillsName = primarySkillsName;
    }

    public String getSecondarySkillsName() {
        return SecondarySkillsName;
    }

    public void setSecondarySkillsName(String secondarySkillsName) {
        SecondarySkillsName = secondarySkillsName;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getIndustryName() {
        return IndustryName;
    }

    public void setIndustryName(String industryName) {
        IndustryName = industryName;
    }

    public String getLanguageName() {
        return LanguageName;
    }

    public void setLanguageName(String languageName) {
        LanguageName = languageName;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    String ComapnyName;
    String Description;
    double CountryId;
    double StateId;
    double CityId;

    public String getPackageMin() {
        return PackageMin;
    }

    public void setPackageMin(String packageMin) {
        PackageMin = packageMin;
    }

    public String getPackageMax() {
        return PackageMax;
    }

    public void setPackageMax(String packageMax) {
        PackageMax = packageMax;
    }

    String PackageMin;
    String PackageMax;
    double PrimarySkills;
    int ExperinceMin;
    int ExperinceMax;
    double IndustryId;
    String FuncationalArea;
    String RoleCatorgory;
    String Role;
    String EmployerType;
    boolean IsActive;
    String lstJobsKillsEntity;
    String PrimarySkillsName;
    String SecondarySkillsName;
    String CountryName;
    String StateName;
    String CityName;
    String IndustryName;
    String LanguageName;
    String CreatedOn;
}

package com.example.ajit.sapnasscom.Model;

/**
 * Created by HSTPL on 2/23/2018.
 */

public class InterviewModel {
    String InterviewQues;
    String InterviewAns;

    public String getInterviewQues() {
        return InterviewQues;
    }

    public void setInterviewQues(String interviewQues) {
        InterviewQues = interviewQues;
    }

    public String getInterviewAns() {
        return InterviewAns;
    }

    public void setInterviewAns(String interviewAns) {
        InterviewAns = interviewAns;
    }
}

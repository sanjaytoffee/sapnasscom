package com.example.ajit.sapnasscom.Common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

public class SessionManager {

    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "SAPNasscom";

    public static final String KEY_USERID = "userid";
    public static final String KEY_APPLIED = "applied";

    public static final String KEY_USER_ID = "userid_id";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_LANGUAGE = "language";
    public static final String KEY_FIRSRNAME = "first_name";
    public static final String KEY_LASTNAME = "last_name";
    public static final String KEY_USER_EMAIL = "user_email";
    public static final String KEY_USER_PASSWORD = "password";
    public static final String KEY_CURRENT_LOCATION_ID = "location_id";
    public static final String KEY_PREFERRED_LOCATION_ID = "preferred_id";

    public static final String KEY_USERNAME = "user_name";
    public static final String KEY_NAME = "name";
    public static final String KEY_ENROLL_STATUS = "enrollstatus";
    public static final String KEY_LEARN_STATUS = "LEARN_STATUS";
    public static final String KEY_JOB_LEARN_STATUS = "JOB_LEARN_STATUS";
    public static final String KEY_DESCRIPTION_STATUS = "description_activity";
    public static final String KEY_GET_FEEDBACK = "feedback";
    public static final String KEY_JOB_APPLIED = "applieds";
    public static final String KEY_COURSE_JOB_APPLIED = "CourseRelatedJobapplieds";
    public static final String KEY_ACTIVE = "active";
    public static final String KEY_DOWNLOAD = "download";
    public static final String KEY_COURSE_STATUS = "course_status";

    public void createEmail(String id, String userid, String userName) {
        editor.putString(KEY_EMAIL, id);
        editor.putString(KEY_USER_ID, userid);
        editor.putString(KEY_USERNAME, userName);
        editor.commit();
    }

    public void createLanguage(String language) {
        editor.putString(KEY_LANGUAGE, language);
        editor.commit();
    }

    public void CreateFeedback(String feedback) {
        editor.putString(KEY_GET_FEEDBACK, feedback);
        editor.commit();
    }

    public void createDownload(String download) {
        editor.putString(KEY_DOWNLOAD, download);
        editor.commit();
    }

    public HashMap<String, String> getDownload() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_DOWNLOAD, pref.getString(KEY_DOWNLOAD, null));
        return user;
    }

    public void createActive(String active) {
        editor.putString(KEY_ACTIVE, active);
        editor.commit();
    }

    public HashMap<String, String> getActive() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ACTIVE, pref.getString(KEY_ACTIVE, null));
        return user;
    }

    public HashMap<String, String> getFeedback() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_GET_FEEDBACK, pref.getString(KEY_GET_FEEDBACK, null));
        return user;
    }

    public void createEnroll(String status) {
        editor.putString(KEY_ENROLL_STATUS, status);
        editor.commit();
    }

    public void createLearnStatus(String status) {
        editor.putString(KEY_LEARN_STATUS, status);
        editor.commit();
    }

    public void createRelatedCourseStatus(String status) {
        editor.putString(KEY_JOB_LEARN_STATUS, status);
        editor.commit();
    }

    public HashMap<String, String> getRelatedCourseStatus() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_JOB_LEARN_STATUS, pref.getString(KEY_JOB_LEARN_STATUS, null));
        return user;
    }

    public void createDesriiptionStatus(String learnStatus) {
        editor.putString(KEY_DESCRIPTION_STATUS, learnStatus);
        editor.commit();
    }

    public HashMap<String, String> getDescriptionStatus() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_DESCRIPTION_STATUS, pref.getString(KEY_DESCRIPTION_STATUS, null));
        return user;
    }

  public HashMap<String, String> getlLearnStatus() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_LEARN_STATUS, pref.getString(KEY_LEARN_STATUS, null));
        return user;
    }

    public void createJobApplied(String status) {
        editor.putString(KEY_JOB_APPLIED, status);
        editor.commit();
    }

    public void createCourseStatus(String status) {
        editor.putString(KEY_COURSE_STATUS, status);
        editor.commit();
    }

    public HashMap<String, String> getCourseStatus() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_COURSE_STATUS, pref.getString(KEY_COURSE_STATUS, null));
        return user;
    }
  public HashMap<String, String> getJobApplied() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_JOB_APPLIED, pref.getString(KEY_JOB_APPLIED, null));
        return user;
    }

    public HashMap<String, String> getEnrollStatus() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_ENROLL_STATUS, pref.getString(KEY_ENROLL_STATUS, null));
        return user;
    }

    public HashMap<String, String> getLanguage() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_LANGUAGE, pref.getString(KEY_LANGUAGE, null));
        return user;
    }

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void clearPreference(Context context) {
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getValue() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_FIRSRNAME, pref.getString(KEY_FIRSRNAME, null));
        user.put(KEY_LASTNAME, pref.getString(KEY_LASTNAME, null));
        user.put(KEY_USER_EMAIL, pref.getString(KEY_USER_EMAIL, null));
        user.put(KEY_USER_PASSWORD, pref.getString(KEY_USER_PASSWORD, null));
        user.put(KEY_CURRENT_LOCATION_ID, pref.getString(KEY_CURRENT_LOCATION_ID, null));
        user.put(KEY_PREFERRED_LOCATION_ID, pref.getString(KEY_PREFERRED_LOCATION_ID, null));
        return user;
    }

    public void logoutUser() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getEmail() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, null));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        return user;
    }

  public void createApplied(String applied) {
        editor.putString(KEY_APPLIED, applied);
        editor.commit();
    }

    public HashMap<String, String> getApplied() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_APPLIED, pref.getString(KEY_APPLIED, null));
        return user;
    }


    public HashMap<String, String> getUserId() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_USERID, pref.getString(KEY_USERID, null));
        return user;
    }

    public void createCourseJobApplied(String applied) {
        editor.putString(KEY_COURSE_JOB_APPLIED, applied);
        editor.commit();
    }

    public HashMap<String, String> getCourseJobApplied() {
        HashMap<String, String> user = new HashMap<String, String>();
        user.put(KEY_COURSE_JOB_APPLIED, pref.getString(KEY_COURSE_JOB_APPLIED, null));
        return user;
    }

}

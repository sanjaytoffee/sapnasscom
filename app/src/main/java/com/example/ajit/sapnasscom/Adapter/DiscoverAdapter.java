package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Activity.CourseActivity;
import com.example.ajit.sapnasscom.Activity.JobActivity;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Model.DiscoverModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.example.ajit.sapnasscom.Utils.Utilities;
import java.util.List;

/**
 * Created by Dharmendar on 12/7/2017.
 */

public class DiscoverAdapter extends RecyclerView.Adapter<DiscoverAdapter.MyViewHolder> {

    private List<DiscoverModel> discoverList;
    private Context context;
    DiscoverModel discoverModel;
    LocaleClass localeClass;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tvName, tvPrimarySkill, tvSecondarySkill, tvCreater, tvDiscoverSubmit, tv_discover_primaryskill,tv_discover_secondary;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tv_edit_tittle);
            tvPrimarySkill = (TextView) view.findViewById(R.id.tv_discover_primary_skills);
            tvSecondarySkill = (TextView) view.findViewById(R.id.tv_discover_secondary_skill);
            tvCreater = (TextView) view.findViewById(R.id.tv_edit_creater);
            tvDiscoverSubmit = (TextView) view.findViewById(R.id.tv_discover_submit);
            tv_discover_primaryskill = (TextView) view.findViewById(R.id.tv_discover_primaryskill);
            tv_discover_secondary = (TextView) view.findViewById(R.id.tv_discover_secondary);
        }
    }

    public DiscoverAdapter(Context context, List<DiscoverModel> discoverList) {
        this.context = context;
        this.discoverList = discoverList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_discover, parent, false);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(context, "DiscoverAdapter");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Utilities.showLog(String.valueOf("Size=" + position));

        discoverModel = discoverList.get(position);

        holder.tvName.setText(discoverModel.getName());
        holder.tvPrimarySkill.setText(discoverModel.getPrimarySkillsName());
        holder.tvSecondarySkill.setText(discoverModel.getSecondarySkillsName());
        holder.tvCreater.setText("Created by : " + discoverModel.getDiscoverCreater());

        if (PrefUtils.get_SelectedCourse(context).equalsIgnoreCase("Course")) {
            holder.tvDiscoverSubmit.setText(R.string.Course_Details);
        } else {
            holder.tvDiscoverSubmit.setText(R.string.Job_Details);
        }

        holder.tvDiscoverSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PrefUtils.get_SelectedCourse(context).equalsIgnoreCase("Course")) {
                    Intent intent = new Intent(context, CourseActivity.class);
                    intent.putExtra("CourseId", discoverList.get(position).getId());
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, JobActivity.class);
                    intent.putExtra("JobId", discoverList.get(position).getId());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return discoverList.size();
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

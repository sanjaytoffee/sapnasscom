package com.example.ajit.sapnasscom.Model;

/**
 * Created by HSTPL on 2/27/2018.
 */

public class DiscoverModel {
    String id;
    String discoverCreater;
    String Name;
    String PrimarySkillsName;
    String SecondarySkillsName;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrimarySkillsName() {
        return PrimarySkillsName;
    }

    public void setPrimarySkillsName(String primarySkillsName) {
        PrimarySkillsName = primarySkillsName;
    }

    public String getSecondarySkillsName() {
        return SecondarySkillsName;
    }

    public void setSecondarySkillsName(String secondarySkillsName) {
        SecondarySkillsName = secondarySkillsName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getDiscoverCreater() {
        return discoverCreater;
    }

    public void setDiscoverCreater(String discoverCreater) {
        this.discoverCreater = discoverCreater;
    }
}

package com.example.ajit.sapnasscom.Activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Adapter.LetsLearnReledJobsAdapter;
import com.example.ajit.sapnasscom.Adapter.PdfAdapter;
import com.example.ajit.sapnasscom.Adapter.VideoAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.JobModel;
import com.example.ajit.sapnasscom.Model.PdfModel;
import com.example.ajit.sapnasscom.Model.VideoModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.FileDownloader;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

/**
 * Created by HSTPL on 2/19/2018.
 */

public class LetsLearnActivity extends AppCompatActivity {
    private String forRequest = "", enrolledValue = "", LearnStatus = "", courseName = "";
    private Toolbar toolbar;
    private String userId, status;
    private RecyclerView rvCourse, rcVideo;
    private SessionManager sessionManager;
    private RecyclerView rvPdf;
    private ArrayList<VideoModel> arrVideo;
    private ArrayList<PdfModel> arrPdf;
    private ArrayList<JobModel> arrJobs;
    private LetsLearnReledJobsAdapter letsLearnReledJobsAdapter;
    private PdfAdapter pdfAdapter;
    private VideoAdapter videoAdapter;
    private double jobAppliedId;
    private boolean AppliedStatus = false;
    private String pdfUrl;
    private String pdfName, pdfId, courseId = "";
    private TextView tv_heading;
    private Intent intent;
    public static TextView tvStartLearning, textMessage, tvReltedVideo, tvRelatedJob, tvLetsLearn, tvFinish, tvProgress, tvRelatedJobs;
    private View viewStartLearning, viewRelatedVideo, viewRealtedJob;
    Button buttonFinish;
    Bundle bundle;
    RadioButton btnLetsLearn, btnProgress, btnFinish;
    View viewProgress, viewFinish;
    LocaleClass localeClass;
    ProgressDialog pd;
    Parcelable recyclerViewState;
    public static FrameLayout customViewContainer;
    public static RelativeLayout lets_layout;
    int SkillId;

    public void onResume() {
        super.onResume();
        try {
            if (Common.dialog.isShowing()) {
                Common.dialog.dismiss();
            }
            if (pd.isShowing()) {
                pd.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

  /*  @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Intent intent1 = new Intent(LetsLearnActivity.this, MycourseActivity.class);
            startActivity(intent1);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }*/

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.lests_learn_activity);

        registerReceiver(pdfBroadCast, new IntentFilter("pdf"));
        registerReceiver(hitApplyjobApi, new IntentFilter("hitApplyjobApii"));

        pd = new ProgressDialog(LetsLearnActivity.this);
        getIntentId();

        init();
        setPdfList();

//        if (LearnStatus.equalsIgnoreCase("Let learn")) {
//            LearnStatus = "Progress";
//        }

        ColorStateList grayColorlist = new ColorStateList(
                new int[][]{ new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{getResources().getColor(R.color.grey)}
        );

        ColorStateList orangeColorlist = new ColorStateList(
                new int[][]{new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{getResources().getColor(R.color.orangeColor)}
        );

        ColorStateList greenColorlist = new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_enabled} //enabled
                },
                new int[]{getResources().getColor(R.color.greenColor)}
        );

        if (LearnStatus.equalsIgnoreCase("Let Learn")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnLetsLearn.setButtonTintList(grayColorlist.valueOf(ContextCompat.getColor(LetsLearnActivity.this, R.color.grey)));
            }
            btnLetsLearn.setHighlightColor(getResources().getColor(R.color.grey));
            btnLetsLearn.setChecked(true);
            buttonFinish.setVisibility(View.VISIBLE);
            viewProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.grey));
        } else if (LearnStatus.equalsIgnoreCase("Progress")) {
            btnLetsLearn.setChecked(true);
            btnProgress.setChecked(true);
            buttonFinish.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnLetsLearn.setButtonTintList(grayColorlist.valueOf(ContextCompat.getColor(LetsLearnActivity.this, R.color.grey)));
            }
            btnLetsLearn.setHighlightColor(getResources().getColor(R.color.grey));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnProgress.setButtonTintList(orangeColorlist.valueOf(ContextCompat.getColor(LetsLearnActivity.this, R.color.orangeColor)));
            }
            btnProgress.setHighlightColor(getResources().getColor(R.color.orangeColor));
            viewProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.grey));
        } else if (LearnStatus.equalsIgnoreCase("Finish")) {
            btnLetsLearn.setChecked(true);
            btnProgress.setChecked(true);
            btnFinish.setChecked(true);
            buttonFinish.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnLetsLearn.setButtonTintList(grayColorlist.valueOf(ContextCompat.getColor(LetsLearnActivity.this, R.color.grey)));
            }
            btnLetsLearn.setHighlightColor(getResources().getColor(R.color.grey));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnProgress.setButtonTintList(grayColorlist.valueOf(ContextCompat.getColor(LetsLearnActivity.this, R.color.grey)));
            }
            btnProgress.setHighlightColor(getResources().getColor(R.color.grey));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                btnFinish.setButtonTintList(greenColorlist.valueOf(ContextCompat.getColor(LetsLearnActivity.this, R.color.greenColor)));
            }
            btnFinish.setHighlightColor(getResources().getColor(R.color.greenColor));

            viewProgress.setBackgroundColor(ContextCompat.getColor(this, R.color.grey));
            viewFinish.setBackgroundColor(ContextCompat.getColor(this, R.color.grey));
        }

        buttonFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askOption(LetsLearnActivity.this);
            }
        });

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(LetsLearnActivity.this, "LetsLearnActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void askOption(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_dialog);
        textMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvBtnYes);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Common.isInternet(LetsLearnActivity.this)) {
                    new HttpAsyncTaskCourse().execute(CommonUrl.myCourseClick);
                } else {
                    noInternet(LetsLearnActivity.this);
                }
            }
        });
        textMessage.setText(R.string.finish_message);

        TextView cancel = (TextView) dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void setPdfList() {
        if (Common.isInternet(LetsLearnActivity.this)) {
            new HttpAsyncTaskgetPdf().execute(CommonUrl.getPdf);
        } else {
            noInternet(LetsLearnActivity.this);
        }
    }

    private void getIntentId() {
        intent = getIntent();
        if (intent != null) {
            enrolledValue = intent.getStringExtra("enrolledValue");
            LearnStatus = intent.getStringExtra("LearnStatus");
            courseId = intent.getStringExtra("courseid");
            courseName = intent.getStringExtra("courseName");
            SkillId = intent.getIntExtra("SkillId", 0);
        }

        Log.e("LearnSkillId", "" + SkillId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(pdfBroadCast);
        unregisterReceiver(hitApplyjobApi);
    }

    private void init() {

        btnLetsLearn = (RadioButton) findViewById(R.id.btnLetsLearn);
        btnProgress = (RadioButton) findViewById(R.id.btnProgress);
        btnFinish = (RadioButton) findViewById(R.id.btnFinish);
        customViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
        lets_layout = (RelativeLayout) findViewById(R.id.lets_layout);

        viewProgress = (View) findViewById(R.id.viewProgress);
        viewFinish = (View) findViewById(R.id.viewFinish);

        tvRelatedJobs = (TextView) findViewById(R.id.tv_related_job);
        tvFinish = (TextView) findViewById(R.id.tv_finish);
        tvProgress = (TextView) findViewById(R.id.tv_progress);
        tvLetsLearn = (TextView) findViewById(R.id.tvLet_learn);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvStartLearning = (TextView) findViewById(R.id.tv_start_learning);
        tvReltedVideo = (TextView) findViewById(R.id.tv_related_video);
        tvRelatedJob = (TextView) findViewById(R.id.tv_related_job);
        viewStartLearning = (View) findViewById(R.id.view_start_learning);
        viewRelatedVideo = (View) findViewById(R.id.view_related_video);
        viewRealtedJob = (View) findViewById(R.id.view_related_course);
        buttonFinish = (Button) findViewById(R.id.finish_Button);
        setSupportActionBar(toolbar);

        final Intent intent = getIntent();
        getSupportActionBar().setTitle(getResources().getString(R.string.My_Courses));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        rcVideo = (RecyclerView) findViewById(R.id.rc_video);
        rvPdf = (RecyclerView) findViewById(R.id.rc_pdf);//rc_pdf
        tv_heading = (TextView) findViewById(R.id.tv_heading);
        arrJobs = new ArrayList<>();
        rvCourse = (RecyclerView) findViewById(R.id.rc_course);//rc_pdf
        arrJobs = new ArrayList<>();
        sessionManager = new SessionManager(LetsLearnActivity.this);
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        Utilities.showLog("userid=" + userId);
        setPdfAdapter();
        setVideoAdapter();

        tv_heading.setText(courseName);

        setAdapter();
    }

    private void setVideoAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LetsLearnActivity.this);
        rcVideo.setLayoutManager(mLayoutManager);
        rcVideo.setItemAnimator(new DefaultItemAnimator());
    }

    private void setPdfAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LetsLearnActivity.this);
        rvPdf.setLayoutManager(mLayoutManager);
        rvPdf.setItemAnimator(new DefaultItemAnimator());
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LetsLearnActivity.this);
        rvCourse.setLayoutManager(mLayoutManager);
        rvCourse.setItemAnimator(new DefaultItemAnimator());
    }

    private class HttpAsyncTaskgetPdf extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskgetPdf() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            arrPdf = new ArrayList<>();
            arrPdf.clear();
            pdfAdapter = new PdfAdapter(LetsLearnActivity.this, arrPdf);
        }

        protected String doInBackground(String... urls) {
            return getPdf(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            rvPdf.setAdapter(pdfAdapter);
            Utilities.showLog(result);

            if (arrPdf.size() == 0) {
                tvStartLearning.setVisibility(View.GONE);
                viewStartLearning.setVisibility(View.GONE);
            } else {
                tvStartLearning.setVisibility(View.VISIBLE);
                viewStartLearning.setVisibility(View.VISIBLE);
            }
            new HttpAsyncTaskgetVideo().execute(CommonUrl.getVideo);
        }
    }

    public String getPdf(String url) {
        String result = "";
        System.out.print("PDFRESPONSE : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "COURSEDOCUMENT");
            params.put("KeyOne", courseId);
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        PdfModel pdfModel = new PdfModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        pdfModel.setID(jsonObject.getString("ID"));
                        pdfModel.setCode(jsonObject.getString("Code"));
                        pdfModel.setName(jsonObject.getString("Name"));
                        pdfModel.setActive(jsonObject.getBoolean("IsActive"));

                        arrPdf.add(pdfModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private class HttpAsyncTaskgetVideo extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskgetVideo() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            arrVideo = new ArrayList<>();
            arrVideo.clear();
            videoAdapter = new VideoAdapter(LetsLearnActivity.this, arrVideo);
        }

        protected String doInBackground(String... urls) {
            return getVideo(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (arrVideo.size() == 0) {
                tvReltedVideo.setVisibility(View.GONE);
                viewRelatedVideo.setVisibility(View.GONE);
            } else {
                tvReltedVideo.setVisibility(View.VISIBLE);
                viewRelatedVideo.setVisibility(View.VISIBLE);
            }
            rcVideo.setAdapter(videoAdapter);

            new HttpAsyncTaskgetJobs().execute(CommonUrl.getCourseRelatedJobs);
        }
    }

    public String getVideo(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "COURSEVIDEO");
            params.put("KeyOne", courseId);
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {

                    JSONArray jsonArray = new JSONArray(response.toString());
                    Log.e("VIDEO",""+jsonArray.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        VideoModel videoModel = new VideoModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        videoModel.setID(jsonObject.getString("ID"));
                        videoModel.setCode(jsonObject.getString("Code"));
                        videoModel.setName(jsonObject.getString("Name"));

                        arrVideo.add(videoModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private class HttpAsyncTaskgetJobs extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskgetJobs() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
            arrJobs = new ArrayList<>();
            letsLearnReledJobsAdapter = new LetsLearnReledJobsAdapter(LetsLearnActivity.this, arrJobs);
            recyclerViewState = rvCourse.getLayoutManager().onSaveInstanceState();
        }

        protected String doInBackground(String... urls) {

            return getJobs(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (arrJobs.size() == 0) {
                tvRelatedJob.setVisibility(View.GONE);
                viewRealtedJob.setVisibility(View.GONE);
            } else {
                tvRelatedJob.setVisibility(View.VISIBLE);
                viewRealtedJob.setVisibility(View.VISIBLE);
            }
            rvCourse.setAdapter(letsLearnReledJobsAdapter);
            rvCourse.getLayoutManager().onRestoreInstanceState(recyclerViewState);

            pd.dismiss();
        }
    }

    public String getJobs(String url) {
        String result = "";
        Log.e("GETJOBRELATEDCOURSE : ", url);
        try {
            JSONObject params = new JSONObject();
            params.put("id", userId);
            params.put("SkillsDetails", SkillId);

            Log.e("GETPARAMS :", params.toString());
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    JSONArray jsonArray = new JSONArray(response.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JobModel jobModel = new JobModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        jobModel.setId(jsonObject.getDouble("Id"));
                        jobModel.setProfile(jsonObject.getString("Profile"));
                        jobModel.setComapnyName(jsonObject.getString("ComapnyName"));
                        jobModel.setExperinceMin(jsonObject.getInt("ExperinceMin"));
                        jobModel.setExperinceMax(jsonObject.getInt("ExperinceMax"));
                        jobModel.setPrimarySkillsName(jsonObject.getString("PrimarySkillsName"));
                        jobModel.setCityName(jsonObject.getString("CityName"));
                        jobModel.setCreatedOn(jsonObject.getString("CreatedOn"));
                        jobModel.setApplied(jsonObject.getBoolean("Applied"));
                        jobModel.setLanguageName(jsonObject.getString("LanguageName"));
                        jobModel.setDescription(jsonObject.getString("Description"));
                        jobModel.setPackageMin(jsonObject.getString("PackageMin"));
                        jobModel.setPackageMax(jsonObject.getString("PackageMax"));
                        jobModel.setIndustryName(jsonObject.getString("IndustryName"));
                        jobModel.setSkillId(jsonObject.getInt("PrimarySkills"));
                        arrJobs.add(jobModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    BroadcastReceiver hitApplyjobApi = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                jobAppliedId = intent.getDoubleExtra("jobApplied_id", 00);

                new HttpAsyncTaskJobApplied().execute(CommonUrl.setJobApplied);
            }

        }
    };

    private class HttpAsyncTaskJobApplied extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskJobApplied() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {

            return setJobApplied(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (AppliedStatus) {
                new HttpAsyncTaskgetJobs().execute(CommonUrl.getCourseRelatedJobs);
                Common.showApplyDialog(LetsLearnActivity.this, getString(R.string.apply));
            }
        }
    }

    public String setJobApplied(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            Double d = new Double(jobAppliedId);
            params.put("JobId", String.valueOf(d.intValue()));
            params.put("Applied", "true");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                   /* JSONObject jsonoObject = new JSONObject(response.toString());
                    //status = jsonoObject.getString("Result");*/

                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        AppliedStatus = true;
                    }
//                    rvCourse.setAdapter(courseAdapter);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (forRequest.equalsIgnoreCase("download")) {
//                String  pdfUrl=intent.getStringExtra("pdfUrl");
//                String pdfNAme=intent.getStringExtra("pdfName");
                Utilities.showLog("pdfUrl=" + CommonUrl.LiveTURLImage + pdfUrl);
                new DownloadFile().execute(CommonUrl.LiveTURLImage + pdfUrl, pdfId + ".pdf");
                //   new DownloadFile().execute("http://www.toffeemail.com/DMS/MasterDocument/d1c9c4Soc.pdf", "ss.pdf");
            } else {
                read();
            }
        }
    }

    BroadcastReceiver pdfBroadCast = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                //  getPermission();
                forRequest = intent.getStringExtra("pdf");
                pdfUrl = intent.getStringExtra("pdfUrl");
                pdfName = intent.getStringExtra("pdfName");
                pdfId = intent.getStringExtra("pdfId");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            &&
                            checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        if (intent.getStringExtra("pdf").equalsIgnoreCase("download")) {
//                        String  pdfUrl=intent.getStringExtra("pdfUrl");
//                        String pdfNAme=intent.getStringExtra("pdfName");
                            Utilities.showLog("pdfUrl=" + CommonUrl.LiveTURLImage + pdfUrl);
                            new DownloadFile().execute(CommonUrl.LiveTURLImage + pdfUrl, pdfId + ".pdf");
                        } else {
                            read();
                        }
                    } else {
                        ActivityCompat.requestPermissions(LetsLearnActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    }
                } else {
                    if (intent.getStringExtra("pdf").equalsIgnoreCase("download")) {
                        Utilities.showLog("pdfUrl=" + CommonUrl.ImageUserProfileUrl + pdfUrl);
                        new DownloadFile().execute(CommonUrl.LiveTURLImage + pdfUrl, pdfId + ".pdf");
                        /*Utilities.showLog("pdfUrl=" + "http://192.168.1.241:787" + pdfUrl);
                        new DownloadFile().execute("http://192.168.1.241:787" + pdfUrl, "unnati.pdf");*/
                    } else {
                        read();
                    }
                    //   checkProgress();
                    // loadImageFromPhone();
//            cameraintentFire();
                }

            }

        }
    };

    public void view(View v) {
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/testthreepdf/" + "maven.pdf");  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(path, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(LetsLearnActivity.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
        }
    }

    /*    private class DownloadFile extends AsyncTask<String, Void, Void> {

            @Override
            protected Void doInBackground(String... strings) {
                String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
                String fileName = strings[1];  // -> maven.pdf
                String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
                File folder = new File(extStorageDirectory, "testthreepdf");
                folder.mkdir();

                File pdfFile = new File(folder, fileName);

                try {
                    pdfFile.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                FileDownloader.downloadFile(fileUrl, pdfFile);
                return null;
            }
        }*/
    private class DownloadFile extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... strings) {
            String fileUrl = strings[0];   // -> http://maven.apache.org/maven-1.x/maven.pdf
            String fileName = strings[1];  // -> maven.pdf
            Utilities.showLog("wrightid=" + fileName);
            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File folder = new File(extStorageDirectory, "CodeUnnatiPdf");
            folder.mkdir();
            File pdfFile = new File(folder, fileName);
            try {
                pdfFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileDownloader.downloadFile(fileUrl, pdfFile);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
           // PdfAdapter.MyViewHolder.tvRead.setText(R.string.read);
            Common.showApplyDialog(LetsLearnActivity.this, "PDF Downloaded Successfully");
            pdfAdapter.notifyDataSetChanged();

        }
    }

    public void read() {
        Utilities.showLog("readid=" + pdfId);
        /*
        String authorities = getApplicationContext().getPackageName() + ".fileprovider";
            Uri imageUri = FileProvider.getUriForFile(this, authorities, cameraFile);
            cameraNougatIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            startActivityForResult(cameraNougatIntent, RESULT_LOAD_CAMERA);
         */
        /*File pdfFile = new File(Environment.getExternalStorageDirectory() + "/testthreepdf/" + pdfId+".pdf");  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        //
        String authorities = getApplicationContext().getPackageName() + ".fileprovider";
        Uri imageUri = FileProvider.getUriForFile(this, authorities, pdfFile);
        //
        Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
        pdfIntent.setDataAndType(imageUri, "application/pdf");
        pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        try {
            startActivity(pdfIntent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(LetsLearnActivity.this, "Knidly download first", Toast.LENGTH_SHORT).show();
        }*/
        File pdfFile = new File(Environment.getExternalStorageDirectory() + "/CodeUnnatiPdf/" + pdfId + ".pdf");  // -> filename = maven.pdf
        Uri path = Uri.fromFile(pdfFile);
        ///
        if (pdfFile.exists()) {
            /* do something */
            String authorities = getApplicationContext().getPackageName() + ".fileprovider";
            Uri imageUri = FileProvider.getUriForFile(LetsLearnActivity.this, authorities, pdfFile);
            //
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(imageUri, "application/pdf");
            // pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            pdfIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            pdfIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            try {
                startActivity(pdfIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(LetsLearnActivity.this, "No Application available to view PDF", Toast.LENGTH_SHORT).show();
            }
        } else {
            Common.showApplyDialog(LetsLearnActivity.this, "Please download the PDF file first");
            //And your other stuffs goes here
        }
    }

    private class HttpAsyncTaskCourse extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourse() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();

            btnLetsLearn.setChecked(true);
            btnProgress.setChecked(true);
            btnFinish.setChecked(true);
            viewProgress.setBackgroundColor(ContextCompat.getColor(LetsLearnActivity.this, R.color.tabColor));
            viewFinish.setBackgroundColor(ContextCompat.getColor(LetsLearnActivity.this, R.color.tabColor));
        }

        protected String doInBackground(String... urls) {
            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Intent intent = new Intent(LetsLearnActivity.this, MycourseActivity.class);
            startActivity(intent);
            pd.dismiss();
            finish();
        }
    }

    public String getCourse(String url) {
        String result = "";
        Log.e("FINISHCLICK : ", url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", userId);
            params.put("CourseID", courseId);
            if (enrolledValue.equalsIgnoreCase("true")) {
                params.put("Enrolled", 1);
            } else {
                params.put("Enrolled", 0);
            }
            params.put("LearnStatus", "Finish");

            Log.e("HELLO", params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public void onBackPressed() {

        Intent intent1 = new Intent(LetsLearnActivity.this, MycourseActivity.class);
        startActivity(intent1);
        finish();
    }
}
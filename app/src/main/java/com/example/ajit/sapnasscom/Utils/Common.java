package com.example.ajit.sapnasscom.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.ajit.sapnasscom.Activity.MainActivity;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Model.Feedback;
import com.example.ajit.sapnasscom.R;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pratik on 2/23/2018.
 */

public class Common {
    public static Dialog dialog;
    public static ArrayList<Feedback> skillsArrayList = new ArrayList<>();
    public static ArrayList<Feedback> skillarray = new ArrayList<>();

    public static void hideApplyDialog() {
        dialog.dismiss();
    }

    public static void showApplyDialog(Context activity, String msg) {
        if (dialog != null) {
            hideApplyDialog();
        }
        try {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_apply);

            TextView text = (TextView) dialog.findViewById(R.id.tv_text);
            text.setText(msg);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showUpdatedProfileDialog(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.updated_profile);

        TextView text = (TextView) dialog.findViewById(R.id.tv_text);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tv_ok);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //   ((MainActivity)activity).setTab();
                dialog.dismiss();
                activity.finish();
            }
        });
        // text.setText(msg);

        /*Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/

        dialog.show();

    }

    public static void Dialog(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_dialog);
        TextView text = (TextView) dialog.findViewById(R.id.tvMessage);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tv_ok);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                activity.finish();
            }
        });
         text.setText("");

        TextView cancel = (TextView) dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }


    public static void showNoDataDialog(final Context activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.no_data_found);

        TextView text = (TextView) dialog.findViewById(R.id.tv_text);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tv_ok);
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) activity).setTab();
                dialog.dismiss();
            }
        });
        // text.setText(msg);

        /*Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });*/

        dialog.show();

    }

    public static void showAlertNoDataFound(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("No Data Found")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        context.finish();
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public static void showAlertDialogString(final Activity context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("No data found")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                        context.finish();
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static void noInternet(final Activity cotext) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(cotext);
        dialogBuilder.setCancelable(false);

        LayoutInflater inflater = cotext.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.no_internet_dialog, null);
        dialogBuilder.setView(dialogView);
        TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);
        final AlertDialog alertDialog = dialogBuilder.create();
        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    public static boolean isInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return (ni != null && ni.isConnected());
    }

    public static int calculateNoOfColumns(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        int noOfColumns = (int) (dpWidth / 150);
        return noOfColumns;
    }

    public static ArrayList<Feedback> fetch_Skill(Context context) {
        new HttpAsyncTask_Skils().execute(CommonUrl.getPdf);
        return skillarray;
    }

    public static class HttpAsyncTask_Skils extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {
            return fetchSkill(urls[0]);
        }

        protected void onPostExecute(String result) {
            if (result != null) {

            }
        }
    }

    public static String fetchSkill(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("Filter", "FEEDBACK");
            params.put("KeyOne", "0");
            params.put("Keytwo", "0");
            params.put("Keythree", "0");
            params.put("Keyfour", "0");
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                skillsArrayList = new ArrayList<>();
                skillarray = new ArrayList<>();
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (line != null) {
                try {
                    final JSONArray jsonArray = new JSONArray(response.toString());
                    skillarray = new ArrayList<>();
                    if (jsonArray.length() != 0) {
                        for (int j = 0; j < jsonArray.length(); j++) {
                            JSONObject catObj2 = jsonArray.getJSONObject(j);
                            Feedback skills = new Feedback();
                            skills.setID(catObj2.getString("ID"));
                            skills.setCode(catObj2.getString("Code"));
                            skills.setName(catObj2.getString("Name"));
                            skillarray.add(skills);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

package com.example.ajit.sapnasscom.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Fragment.JobDeatailFragment;
import com.example.ajit.sapnasscom.Fragment.JobRelatedCourseFragment;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Utilities;
import java.util.ArrayList;
import java.util.List;

public class JobDescriptionActivity extends AppCompatActivity {
    Toolbar toolbar;
    TabLayout tabs;
    ViewPager viewPager;
    TextView tvExperience, tvLocation, tvStatus, tvTitle;
    Intent intent;
    String jobId, title, experience, location, language, myJobClick, jobStatus = "", keyFeatures, description, companyName, profileNAme, salary = "", industry = "";
    boolean applyStatus;
    RelativeLayout statusRelative;
    LocaleClass localeClass;
    int SkillId;
    public static TextView tv_duration, tv__cource_location, tv_myjob_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_job_description);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(JobDescriptionActivity.this, "JobDescriptionActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        intent = getIntent();
        if (intent != null) {
            Double d = new Double(intent.getDoubleExtra("jobid", 00));
            jobId = String.valueOf(d.intValue());
            Utilities.showLog("jobid=" + jobId);
            companyName = intent.getStringExtra("company_name");
            profileNAme = intent.getStringExtra("profile_name");
            location = intent.getStringExtra("location");
            applyStatus = intent.getBooleanExtra("applystatus", false);
            description = intent.getStringExtra("description");
            experience = intent.getStringExtra("experience");
            salary = intent.getStringExtra("salary");
            industry = intent.getStringExtra("industury");
            jobStatus = intent.getStringExtra("jobStatus");
            myJobClick = intent.getStringExtra("myJobClick");
            SkillId = intent.getIntExtra("SkillId",0);

            Log.e("JOBSTATUS", "" + applyStatus);

            Utilities.showLog("ActivityShoeDescription=" + description);
            Utilities.showLog(String.valueOf(salary + "\n" + industry + "\n" + jobId + companyName + profileNAme + location + applyStatus + description));
        }
        init();
        setData();

    }

    private void setData() {
        tvTitle.setText(profileNAme);
        tvExperience.setText(experience);
        tvLocation.setText(location);

        try {
            if (myJobClick.equalsIgnoreCase("true")) {
                statusRelative.setVisibility(View.VISIBLE);
                tv_myjob_status.setText(R.string.status);
                tvStatus.setText(jobStatus);
            } else {
                statusRelative.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvExperience = (TextView) findViewById(R.id.tv_experience_number);
        tvLocation = (TextView) findViewById(R.id.tv_location_name);
        tvStatus = (TextView) findViewById(R.id.tv_job_status);
        tv_duration = (TextView) findViewById(R.id.tv_duration);
        tv__cource_location = (TextView) findViewById(R.id.tv__cource_location);
        tv_myjob_status = (TextView) findViewById(R.id.tv_myjob_status);

        statusRelative = (RelativeLayout) findViewById(R.id.statusRelative);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);
    }

    private void setupViewPager(final ViewPager viewPager) {

        final DescoverWithoutLogin.Adapter adapter = new DescoverWithoutLogin.Adapter(getSupportFragmentManager());
        Bundle bundle = new Bundle();
        bundle.putString("jobid", jobId);
        bundle.putBoolean("applyvalue", applyStatus);
        bundle.putString("description", description);
        bundle.putString("company_name", companyName);
        bundle.putString("profile_name", profileNAme);
        bundle.putString("salary", salary);
        bundle.putString("industury", industry);
        bundle.putInt("SkillId", SkillId);
        JobDeatailFragment jobDeatailFragment = new JobDeatailFragment();
        JobRelatedCourseFragment jobCourseRelatedFragment = new JobRelatedCourseFragment();
        jobDeatailFragment.setArguments(bundle);
        jobCourseRelatedFragment.setArguments(bundle);
        adapter.addFragment(jobDeatailFragment, getResources().getString(R.string.Job_Description));
        adapter.addFragment(jobCourseRelatedFragment, getResources().getString(R.string.Related_Course));
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

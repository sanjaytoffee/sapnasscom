package com.example.ajit.sapnasscom.Activity;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Fragment.CourseDeatailFragment;
import com.example.ajit.sapnasscom.Fragment.CourseRelatedJobFragment;
import com.example.ajit.sapnasscom.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CourseDescriptionActivity extends AppCompatActivity {
    Toolbar toolbar;
    TabLayout tabs;
    ViewPager viewPager;
    public static TextView tvDuration, tvLocation, tv_duration, tv_languages, tv__cource_location, tvLanguage, tvTitle;
    Intent intent;
    String courseId, title, duration, location, language, keyFeatures, courseName, learnStatus, description, selectedLanguage;
    boolean enrolled;
    SessionManager sessionManager;
    int skillId;
    LocaleClass localeClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_course_description);

        sessionManager = new SessionManager(CourseDescriptionActivity.this);
        HashMap<String, String> langh = sessionManager.getLanguage();
        selectedLanguage = langh.get(SessionManager.KEY_LANGUAGE);
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "CourseDescriptionActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        intent = getIntent();
        if (intent != null) {
            courseId = intent.getStringExtra("courseid");
            title = intent.getStringExtra("title");
            learnStatus = intent.getStringExtra("LearnStatus");
            duration = intent.getStringExtra("duration");
            location = intent.getStringExtra("location");
            language = intent.getStringExtra("language");
            enrolled = intent.getBooleanExtra("enrolledValue", false);
            keyFeatures = intent.getStringExtra("keyfeatures");
            description = intent.getStringExtra("description");
            courseName = intent.getStringExtra("courseName");
            skillId = intent.getIntExtra("SkillId",0);
        }
        init();
        setData();
    }

    private void setData() {
        tvTitle.setText(title);
        tvDuration.setText(duration);
        tvLocation.setText(location);
        tvLanguage.setText(language);
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) findViewById(R.id.tv_title);
        tvDuration = (TextView) findViewById(R.id.tv_experience_number);
        tvLocation = (TextView) findViewById(R.id.tv_location_namee);
        tvLanguage = (TextView) findViewById(R.id.tv_laguages_name);
        tv_duration = (TextView) findViewById(R.id.tv_duration);
        tv__cource_location = (TextView) findViewById(R.id.tv__cource_location);
        tv_languages = (TextView) findViewById(R.id.tv_languages);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                finish();
            }
        });
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tabs = (TabLayout) findViewById(R.id.tabs);
        setupViewPager(viewPager);

        tabs.setupWithViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        viewPager.setCurrentItem(1);
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "CourseDescriptionActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void setupViewPager(final ViewPager viewPager) {

        final DescoverWithoutLogin.Adapter adapter = new DescoverWithoutLogin.Adapter(getSupportFragmentManager());
        //  viewPager.setOffscreenPageLimit(1);
        Bundle bundle = new Bundle();
        bundle.putString("courseid", courseId);
        bundle.putBoolean("enrolledvalue", enrolled);
        bundle.putString("description", description);
        bundle.putString("keyfeatures", keyFeatures);
        bundle.putString("LearnStatus", learnStatus);
        bundle.putString("courseid", courseId);
        bundle.putString("courseName", courseName);
        bundle.putInt("Skillid", skillId);
        CourseDeatailFragment courseDeatailFragment = new CourseDeatailFragment();
        CourseRelatedJobFragment courseRelatedJobsFragment = new CourseRelatedJobFragment();
        courseDeatailFragment.setArguments(bundle);
        courseRelatedJobsFragment.setArguments(bundle);
        adapter.addFragment(courseDeatailFragment, getResources().getString(R.string.details));
        adapter.addFragment(courseRelatedJobsFragment, getResources().getString(R.string.related_jobs));
        viewPager.setAdapter(adapter);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

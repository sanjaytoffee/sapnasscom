package com.example.ajit.sapnasscom.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ajit.sapnasscom.Activity.CourseDescriptionActivity;
import com.example.ajit.sapnasscom.Activity.LetsLearnActivity;
import com.example.ajit.sapnasscom.Activity.MycourseActivity;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Fragment.FeedbackBottomSheetFragment;
import com.example.ajit.sapnasscom.Model.CourseModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

/**
 * Created by pratik on 12/7/2017.
 */

public class MyCourseAdapter extends RecyclerView.Adapter<MyCourseAdapter.MyViewHolder> {

    private List<CourseModel> arrCourse;
    private Context context;
    SessionManager sessionManager;
    private String userId;
    String id;
    boolean enrolledValue;
    LocaleClass localeClass;
    public static String job = "", course = "", mycourse = "";
    private int d;

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        public static TextView tvCourseName, tv_enrolledDate, tvDate, tvCreatedby, tvCourseCount, tvDuration, tvLocation, tvLanguage, tvEnrollNow, tvCourseDescription;
        private ImageView ivSite;
        RelativeLayout courseCardView;
        public CardView courseCard;
        public LinearLayout course_dot_click;
        public static TextView tv__enrolled_text, tv_duration, tv__cource_location, tv_languages;

        public MyViewHolder(View view) {
            super(view);
            tv_duration = (TextView) view.findViewById(R.id.tv_duration);
            tv__cource_location = (TextView) view.findViewById(R.id.tv__cource_location);
            tv_languages = (TextView) view.findViewById(R.id.tv_languages);
            tv__enrolled_text = (TextView) view.findViewById(R.id.tv__enrolled_text);
            tvCourseName = (TextView) view.findViewById(R.id.tv_course_department);
            tvDate = (TextView) view.findViewById(R.id.tv_course_date);
            tv_enrolledDate = (TextView) view.findViewById(R.id.tv_enrolledDate);
            tvCreatedby = (TextView) view.findViewById(R.id.tv_course_company_name);
            tvDuration = (TextView) view.findViewById(R.id.tv_course_experience_number);
            tvLocation = (TextView) view.findViewById(R.id.tv_course_location_name);
            tvLanguage = (TextView) view.findViewById(R.id.tv_course_laguages_name);
            tvCourseDescription = (TextView) view.findViewById(R.id.tv_mycourse_detail);
            tvCourseCount = (TextView) view.findViewById(R.id.tv_course_enrolled_no);
            tvEnrollNow = (TextView) view.findViewById(R.id.tv_mycourse_enroll_now);
            courseCardView = (RelativeLayout) view.findViewById(R.id.courseCardView);
            courseCard = (CardView) view.findViewById(R.id.courseCard);
            course_dot_click = (LinearLayout) view.findViewById(R.id.course_dot_click);
        }
    }

    public MyCourseAdapter(Context context, List<CourseModel> arrCourse) {
        this.context = context;
        this.arrCourse = arrCourse;
        sessionManager = new SessionManager(context);
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_my_course, parent, false);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(context, "MyCourseAdapter");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Utilities.showLog(String.valueOf("Size=" + position));
        final CourseModel courseModel = arrCourse.get(position);

        holder.tvCourseName.setText(courseModel.getCourseName());
        holder.tvCreatedby.setText("Created By: " + courseModel.getCreator());
        holder.tvDuration.setText(String.valueOf(courseModel.getDuration()) + " Weeks");
        holder.tvLocation.setText(courseModel.getCityName());
        holder.tvLanguage.setText(courseModel.getLanguageName());
        holder.tvDate.setText(courseModel.getCreatedOn());
        holder.tvCourseCount.setText("" + courseModel.getCourseCount());

        if (courseModel.getLearStatus().equalsIgnoreCase("Let learn")) {
            holder.tvEnrollNow.setText(R.string.start_course);
        } else if (courseModel.getLearStatus().equalsIgnoreCase("Finish")) {
            holder.tvEnrollNow.setText(R.string.finishh);
        } else {
            holder.tvEnrollNow.setText(R.string.continue_course);
        }

        sessionManager.createCourseStatus("" + courseModel.getLearStatus());

        holder.tvCourseDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double d = new Double(courseModel.getIdValue());
                Intent intent = new Intent(context, CourseDescriptionActivity.class);
                intent.putExtra("courseid", String.valueOf(d.intValue()));
                intent.putExtra("title", courseModel.getCourseName());
                intent.putExtra("duration", String.valueOf(courseModel.getDuration()) + " Weeks");
                intent.putExtra("location", courseModel.getCityName());
                intent.putExtra("language", courseModel.getLanguageName());
                intent.putExtra("enrolled", courseModel.isEnrolled());
                intent.putExtra("keyfeatures", courseModel.getKeyFeature());
                intent.putExtra("description", courseModel.getDescription());
                intent.putExtra("LearnStatus", courseModel.getLearStatus());
                intent.putExtra("enrolledValue", courseModel.isEnrolled());
                intent.putExtra("courseName", courseModel.getCourseName());
                intent.putExtra("SkillId", courseModel.getSkillId());
                context.startActivity(intent);
            }
        });

        holder.course_dot_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Coures", "" + courseModel.getFeadBack());
                try {
                    if (courseModel.getFeadBack().equalsIgnoreCase("false")) {
                        FeedbackBottomSheetFragment bottomSheetDialogFragment = new FeedbackBottomSheetFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("Skill_list", (Serializable) Common.fetch_Skill(context));
                        bundle.putString("COURSE_FEEDBACK", "COURSE_FEEDBACK");
                        sessionManager.CreateFeedback("COURSE_FEEDBACK");
                        bottomSheetDialogFragment.setArguments(bundle);
                        bottomSheetDialogFragment.show(((MycourseActivity) context).getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        job = String.valueOf(courseModel.getIdValue());
                        mycourse = "MYCOURSE";
                    } else {
                        Toast.makeText(context, "You have already submitted your feedback", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        try {
            if (courseModel.getFeadBack().equalsIgnoreCase("true")) {
                holder.courseCardView.setBackgroundColor(Color.parseColor("#fdd1c8"));
                holder.courseCard.setBackgroundColor(Color.parseColor("#fdd1c8"));
            } else {
                holder.courseCardView.setBackgroundColor(Color.parseColor("#ffffff"));
                holder.courseCard.setBackgroundColor(Color.parseColor("#ffffff"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.tvEnrollNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double d = new Double(courseModel.getIdValue());
                Utilities.showLog("courseid=" + String.valueOf(d.intValue()));
                Intent intent = new Intent(context, LetsLearnActivity.class);
                intent.putExtra("courseName", courseModel.getCourseName());
                intent.putExtra("courseid", String.valueOf(d.intValue()));
                intent.putExtra("LearnStatus", courseModel.getLearStatus());
                intent.putExtra("SkillId", courseModel.getSkillId());
                Log.e("COURSEADAPTER", String.valueOf(d.intValue()));

                id = arrCourse.get(position).getIdValue();
                enrolledValue = arrCourse.get(position).isEnrolled();
                if (Common.isInternet(context)) {
                    if (courseModel.getLearStatus().equalsIgnoreCase("Let Learn")) {
                        new HttpAsyncTaskCourse().execute(CommonUrl.myCourseClick);
                    }
                } else {
                    Toast.makeText(context, "Please Connect Internet", Toast.LENGTH_SHORT).show();
                }
                intent.putExtra("courseid", arrCourse.get(position).getIdValue());
                intent.putExtra("enrolledValue", "" + enrolledValue);
                context.startActivity(intent);
                ((MycourseActivity) context).finish();
            }
        });
    }

    private class HttpAsyncTaskCourse extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourse() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            MyProgressDialog.showDialog(context);
        }

        protected String doInBackground(String... urls) {
            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            MyProgressDialog.hideDialog();
        }
    }

    public String getCourse(String url) {
        String result = "";
        System.out.print("MYCOURSEACTIVITY_URL : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserID", userId);
            params.put("CourseID", id);
            if (enrolledValue == true) {
                params.put("Enrolled", 1);
            } else {
                params.put("Enrolled", 0);
            }

            params.put("LearnStatus", "Progress");

            Log.e("MYCOURSEACTIVITY",params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public int getItemCount() {
        return arrCourse.size();
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

}

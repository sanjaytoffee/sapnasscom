package com.example.ajit.sapnasscom.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.asksira.loopingviewpager.LoopingViewPager;
import com.example.ajit.sapnasscom.Adapter.DemoInfiniteAdapter;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.rd.PageIndicatorView;

import java.util.ArrayList;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class SignUpDescoverActivity extends AppCompatActivity implements View.OnClickListener {
    LoopingViewPager viewPager;
    DemoInfiniteAdapter adapter;
    PageIndicatorView indicatorView;
    private int currentDataSet = 1;
    public static TextView tvDescover, tvSignin;
    LocaleClass localeClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_descover);

        viewPager = findViewById(R.id.viewpager);
        indicatorView = findViewById(R.id.indicator);
        tvDescover = (TextView) findViewById(R.id.tv_descover);
        tvSignin = (TextView) findViewById(R.id.tv_signin);

        tvDescover.setOnClickListener(this);
        tvSignin.setOnClickListener(this);
        adapter = new DemoInfiniteAdapter(this, createDummyItems(), true);
        viewPager.setAdapter(adapter);

        //Custom bind indicator
        indicatorView.setCount(viewPager.getIndicatorCount());
        viewPager.setIndicatorPageChangeListener(new LoopingViewPager.IndicatorPageChangeListener() {
            @Override
            public void onIndicatorProgress(int selectingPosition, float progress) {
                indicatorView.setProgress(selectingPosition, progress);
            }

            @Override
            public void onIndicatorPageChange(int newIndicatorPosition) {
//                indicatorView.setSelection(newIndicatorPosition);
            }
        });

        View.OnClickListener pageSelector = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer number = Integer.valueOf(((Button) v).getText().toString());
                viewPager.setCurrentItem(adapter.isInfinite() ? number : number - 1);
            }
        };
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "SignUpDescoverActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        viewPager.resumeAutoScroll();
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "SignUpDescoverActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        viewPager.pauseAutoScroll();
        super.onPause();
    }

    private ArrayList<Integer> createDummyItems() {
        ArrayList<Integer> items = new ArrayList<>();
        items.add(0, 1);
        items.add(1, 2);
        items.add(2, 3);
        return items;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_descover:
                if (Common.isInternet(SignUpDescoverActivity.this)) {
                    Intent intent1 = new Intent(SignUpDescoverActivity.this, DescoverWithoutLogin.class);
                    startActivity(intent1);
                } else {
                    noInternet(SignUpDescoverActivity.this);
                }

//                Intent
                break;
            case R.id.tv_signin:
                Intent intent = new Intent(SignUpDescoverActivity.this, Main.class);
                startActivity(intent);
//                Intent
                break;
        }
    }
}

package com.example.ajit.sapnasscom.Activity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.Window;
import android.view.WindowManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;

public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_screen);

        Common.fetch_Skill(this);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        Thread background = new Thread() {
            public void run() {
                try {
                    sleep(3 * 1000);
                    if(PrefUtils.getUserLoginStatus()){
                        Intent intent=new Intent(SplashScreenActivity.this,MainActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Intent intent=new Intent(SplashScreenActivity.this,LanguageScreenActivity.class);
                        startActivity(intent);
                    }

                    finish();

                } catch (Exception e) {

                }
            }
        };
        background.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}

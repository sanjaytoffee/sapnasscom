package com.example.ajit.sapnasscom.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public final class PrefUtils {

    private static SharedPreferences pref;
    private static Editor editor;
    private static String PREF_NAME = "user_pref";
    private static String USER_EMAIL = "email";
    private static String USER_ID = "id";
    private static String USER_login = "login";
    private static String IMAGES = "images";
    private static String USER_FULL_NAME = "user_full_name";
    private static String USER_FIRST_NAME = "user_first_name";
    private static String USER_LAST_NAME = "user_last_name";
    private static String USER_gender = "gender";
    private static String USER_PROFILE_URL = "profile_url";
    private static String SELECTED_COURSE = "select_course";

    /**
     * @param ctx
     */
    public static void init(Context ctx) {
        pref = ctx.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.apply();
    }

    public static String get_SelectedCourse(Context context) {
        return pref.getString(SELECTED_COURSE, "");
    }

    public static void setSelected_Course(Context context, String select_course) {
        editor.putString(SELECTED_COURSE, select_course);
        editor.apply();
    }

    public static boolean getUserLoginStatus() {
        return pref.getBoolean(USER_login, false);
    }

    public static void setUserLoginStatus(Boolean loginstatus) {
        editor.putBoolean(USER_login, loginstatus);
        editor.apply();
    }

    public static String getUserID(Context context) {
        return pref.getString(USER_ID, "");
    }

    public static void setUserID(Context context, String user_id) {
        editor.putString(USER_ID, user_id);
        editor.apply();
    }

    public static String getUserEmail(Context context) {
        return pref.getString(USER_EMAIL, "");
    }

    public static void setUserEmail(Context context, String user_EMAIL) {
        editor.putString(USER_EMAIL, user_EMAIL);
        editor.apply();
    }

    public static void clearPreference(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }

    public static String getJson() {
        return pref.getString(IMAGES, "");
    }

    public static void setJson(String json) {
        editor.putString(IMAGES, json);
        editor.apply();
    }

    public static String getUserFullName(Context context) {
        return pref.getString(USER_FULL_NAME, "");
    }

    public static void setUserFullName(Context context, String fullNAme) {

        editor.putString(USER_FULL_NAME, fullNAme);
        editor.apply();
    }

    public static void setUserFirstName(Context context, String first_name) {
        editor.putString(USER_FIRST_NAME, first_name);
        editor.apply();
    }

    public static void setUserLastName(Context context, String last_name) {
        editor.putString(USER_LAST_NAME, last_name);
        editor.apply();
    }


    public static void setGender(Context context, String gender) {
        editor.putString(USER_gender, gender);
        editor.apply();
    }

    public static String getUser_Profileurl(Context context) {
        return pref.getString(USER_PROFILE_URL, "");
    }

    public static void setUser_Profileurl(Context context, String user_prfile_url) {
        editor.putString(USER_PROFILE_URL, user_prfile_url);
        editor.apply();
    }
}
package com.example.ajit.sapnasscom.Fragment;


import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.MainActivity;
import com.example.ajit.sapnasscom.Activity.ProfileDetailsActivity;
import com.example.ajit.sapnasscom.Adapter.CourseAdapter;
import com.example.ajit.sapnasscom.Adapter.MyViewHeaderAdapter;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Model.CourseModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class CourseFragment extends Fragment {

    public CourseFragment() {
    }

    private double courseEnrolledId;
    private String userId, status;
    private RecyclerView rvCourse;
    private SessionManager sessionManager;
    private CourseModel courseModel;
    private ArrayList<CourseModel> arrCourseList;
    private CourseAdapter courseAdapter;
    private boolean enrolledStatus = false;
    private RelativeLayout rlNoDatafound;
    public static TextView title_text, title, tvOk;
    LocaleClass localeClass;
    ProgressDialog pd;
    double d;
    Parcelable recyclerViewState;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().registerReceiver(enrolledApiCall, new IntentFilter("hitEnrolledApi"));
        getActivity().registerReceiver(courseApiCall, new IntentFilter("hitCoursedApi"));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(enrolledApiCall);
        getActivity().unregisterReceiver(courseApiCall);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_course, container, false);

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getActivity(), "CourseFragment");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        pd = new ProgressDialog(getActivity());

        init(v);

        return v;
    }

    private void init(View v) {
        rvCourse = (RecyclerView) v.findViewById(R.id.rc_course);
        rvCourse.setHasFixedSize(true);
        rlNoDatafound = (RelativeLayout) v.findViewById(R.id.rl_no_data_found);
        title = (TextView) v.findViewById(R.id.header_footer_title);
        title_text = (TextView) v.findViewById(R.id.header_title);
        title.setText(getActivity().getResources().getString(R.string.recommend_texts));
        title.setTextColor(Color.parseColor("#1E90FF"));

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ProfileDetailsActivity.class);
                intent.putExtra("editProfile", "editProfile");
                getActivity().startActivity(intent);
            }
        });
        tvOk = (TextView) v.findViewById(R.id.tv_ok);
        sessionManager = new SessionManager(getActivity());
        HashMap<String, String> getmail = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        Utilities.showLog("userid=" + userId);

        setAdapter();

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity) getActivity()).setTab();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (Common.dialog.isShowing()) {
                Common.dialog.dismiss();
            }
            if (pd.isShowing()) {
                pd.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Common.isInternet(getActivity())) {
            new HttpAsyncTaskCourse().execute(CommonUrl.getCourse);
        } else {
            noInternet(getActivity());
        }
    }

    private void setAdapter() {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvCourse.setLayoutManager(mLayoutManager);
        rvCourse.setItemAnimator(new DefaultItemAnimator());
    }

    public class HttpAsyncTaskCourse extends AsyncTask<String, Void, String> {
        public HttpAsyncTaskCourse() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();

            arrCourseList = new ArrayList<>();
            courseAdapter = new CourseAdapter(getActivity(), arrCourseList);
            recyclerViewState = rvCourse.getLayoutManager().onSaveInstanceState();
        }

        protected String doInBackground(String... urls) {
            return getCourse(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (arrCourseList.size() == 0) {
                rlNoDatafound.setVisibility(View.VISIBLE);
                rvCourse.setVisibility(View.GONE);
            } else {
                rlNoDatafound.setVisibility(View.GONE);
                rvCourse.setVisibility(View.VISIBLE);
            }
            rvCourse.setAdapter(courseAdapter);
            rvCourse.getLayoutManager().onRestoreInstanceState(recyclerViewState);
            Utilities.showLog(result);
            pd.dismiss();
        }
    }

    public String getCourse(String url) {
        String result = "";
        Log.e("CourseUrl : ", url);
        try {
            JSONObject params = new JSONObject();
            params.put("id", userId);

            Log.e("COURSEPARAMS", params.toString());
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }
            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    enrolledStatus = false;
                    JSONArray jsonArray = new JSONArray(response.toString());
                    Log.e("CourseLength", "" + jsonArray.length());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        CourseModel courseModel = new CourseModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        courseModel.setId(jsonObject.getDouble("Id"));
                        d = jsonObject.getDouble("Id");
                        Utilities.showLog("LogPrint" + String.valueOf(jsonObject.getDouble("Id")) + jsonObject.getString("CourseName"));
                        courseModel.setCourseName(jsonObject.getString("CourseName"));
                        courseModel.setEnrolledCount(jsonObject.getString("TotalCourseEnrolled"));
                        courseModel.setDuration(jsonObject.getString("Duration"));
                        courseModel.setCityName(jsonObject.getString("CityName"));
                        courseModel.setCreator(jsonObject.getString("Creator"));
                        courseModel.setCreatedOn(jsonObject.getString("CreatedOn"));
                        courseModel.setLanguageName(jsonObject.getString("LanguageName"));
                        courseModel.setEnrolled(jsonObject.getBoolean("Enrolled"));
                        courseModel.setKeyFeature(jsonObject.getString("KeyFeature"));
                        courseModel.setDescription(jsonObject.getString("Description"));
                        courseModel.setFeadBack(jsonObject.getString("IsFeedback"));
                        courseModel.setLearnStatus(jsonObject.getString("LearnStatus"));
                        courseModel.setSkillId(jsonObject.getInt("PrimarySkills"));

                        arrCourseList.add(courseModel);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    BroadcastReceiver enrolledApiCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                courseEnrolledId = intent.getDoubleExtra("course_enrolled_id", 00);
                Utilities.showLog(String.valueOf(courseEnrolledId));
                if (Common.isInternet(getActivity())) {
                    new CourseFragment.HttpAsyncTaskCourseEnrolled().execute(CommonUrl.setCourseEnrolled);
                } else {
                    noInternet(getActivity());
                }
            }
        }
    };

    BroadcastReceiver courseApiCall = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                if (Common.isInternet(getActivity())) {
                    new HttpAsyncTaskCourse().execute(CommonUrl.getCourse);
                } else {
                    noInternet(getActivity());
                }
            }
        }
    };

    private class HttpAsyncTaskCourseEnrolled extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourseEnrolled() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
            pd.setMessage("Please wait...");
            pd.setProgressStyle(0);
            pd.setIndeterminate(true);
            pd.setProgress(0);
            pd.setCancelable(false);
            pd.show();
        }

        protected String doInBackground(String... urls) {

            return setEnrolled(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Utilities.showLog(result);
            if (enrolledStatus) {
                Common.showApplyDialog(getActivity(), getString(R.string.enrolled));
                new HttpAsyncTaskCourse().execute(CommonUrl.getCourse);
            }
        }
    }

    public String setEnrolled(String url) {
        String result = "";
        Log.e("EnrollAPI : ", "" + url);

        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            Double d = new Double(courseEnrolledId);
            params.put("CourseID", String.valueOf(d.intValue()));
            params.put("Enrolled", "true");

            Log.e("ENROLLPARAm", params.toString());

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        enrolledStatus = true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}

package com.example.ajit.sapnasscom.Activity;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import com.example.ajit.sapnasscom.R;
import java.util.List;
import static android.text.Html.fromHtml;

/**
 * Created by HSTPL on 2/19/2018.
 */

public class SupportActivity extends AppCompatActivity implements View.OnClickListener {

    Toolbar toolbar;
    TextView myTextView, tvTextView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_support);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.Support));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        String s = "<b>Program objective</b><br>" +
                "<br>" +
                "Our mission is to foster digital literacy and develop digital competencies to contribute towards social and economic development among Adolescents, Youth, Citizens & Differently abled.\n" +
                "<br>" +
                "<br>" +
                "<b>Mobile App Objective</b><br>" +
                "<br>" +
                "The App is designed to help you explore career options, jobs near you and find the courses that can help you enhance your skills to get the jobs you desire.\n" +
                "<br>" +
                "<br>" +
                "For queries, write to us at";
        myTextView = (TextView) findViewById(R.id.myTextView);
        tvTextView = (TextView) findViewById(R.id.tvTextView);
        String mailText = "<u>appsupport@codeunnati.org</u>";

        tvTextView.setTextColor(Color.parseColor("#1E90FF"));

        myTextView.setText(fromHtml(s));
        tvTextView.setText(fromHtml(mailText));

        tvTextView.setOnClickListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvTextView:
                gmail();
                break;
        }
    }

    public void gmail() {
        Intent intent = new Intent(Intent.ACTION_SEND).setType("text/plain")
                .putExtra(Intent.EXTRA_EMAIL, new String[]{"appsupport@codeunnati.org"});
        List<ResolveInfo> matches = this.getPackageManager().queryIntentActivities(intent, 0);
        ResolveInfo best = null;
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail")) {
                best = info;
            }
        }
        if (best != null) {
            intent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
        }
        startActivity(intent);
    }
}
package com.example.ajit.sapnasscom.Model;

import java.util.ArrayList;

public class Feedback {



    private String ID;
    private String Code;
    private String Name;
    private String PrimarySkills;
    private String primarySkillsId;
    private String secondrySkills;
    private String userID;
    private boolean selected;

    public Feedback(String ID, String code, String name, String primarySkills, String primarySkillsId, String secondrySkills, String userID, boolean selected, ArrayList<Feedback> secondryList) {
        this.ID = ID;
        Code = code;
        Name = name;
        PrimarySkills = primarySkills;
        this.primarySkillsId = primarySkillsId;
        this.secondrySkills = secondrySkills;
        this.userID = userID;
        this.selected = selected;
        this.secondryList = secondryList;
    }



    public Feedback() {

    }


    public String getSecondrySkills() {
        return secondrySkills;
    }

    public void setSecondrySkills(String secondrySkills) {
        this.secondrySkills = secondrySkills;
    }



    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }



    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }



    public String getPrimarySkillsId() {
        return primarySkillsId;
    }

    public void setPrimarySkillsId(String primarySkillsId) {
        this.primarySkillsId = primarySkillsId;
    }




    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPrimarySkills() {
        return PrimarySkills;
    }

    public void setPrimarySkills(String primarySkills) {
        PrimarySkills = primarySkills;
    }







    private ArrayList<Feedback> secondryList;
    public void setSecondryList(ArrayList<Feedback> secondryList) {
        this.secondryList = secondryList;
    }
    public ArrayList<Feedback> getSecondryList() {
        return secondryList;
    }
}

package com.example.ajit.sapnasscom.Common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import com.example.ajit.sapnasscom.Activity.ForgotSecond;

public class IncomingSms extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    try {
                        if (senderNum.equals("MD-UNNATI")) {
                            ForgotSecond Sms = new ForgotSecond();
                            Sms.recivedSms(message);
                        }
                        ForgotSecond.mPinFirstDigitEditText.setText("" + ForgotSecond.j0);
                        ForgotSecond.mPinSecondDigitEditText.setText("" + ForgotSecond.j1);
                        ForgotSecond.mPinThirdDigitEditText.setText("" + ForgotSecond.j2);
                        ForgotSecond.mPinForthDigitEditText.setText("" + ForgotSecond.j3);

                    } catch (Exception e) {
                    }

                }
            }

        } catch (Exception e) {

        }
    }

}
package com.example.ajit.sapnasscom.Adapter;


import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Fragment.FeedbackBottomSheetFragment;
import com.example.ajit.sapnasscom.Model.Feedback;
import com.example.ajit.sapnasscom.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FeedBackAdapter extends RecyclerView.Adapter<FeedBackAdapter.MyViewHolder> {

    private Context mContext;
    private List<Feedback> mGridData = new ArrayList<Feedback>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView titleTextView, year, genre;

        public MyViewHolder(View view) {
            super(view);
            titleTextView = (TextView) view.findViewById(R.id.grid_item_title);
        }
    }

    public FeedBackAdapter(Context mContext, List<Feedback> mGridData) {
        this.mGridData = mGridData;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feedback_type_text_view, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Feedback item = mGridData.get(position);
        holder.titleTextView.setText(Html.fromHtml(item.getName()));
        holder.titleTextView.setTag(position);

        holder.titleTextView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                boolean isSelectedAfterClick = !v.isSelected();
                v.setSelected(isSelectedAfterClick);

                if (isSelectedAfterClick) {
                    holder.titleTextView.setBackgroundColor(mContext.getResources().getColor(R.color.splashColor));
                    mGridData.get(position).setSelected(true);
                    // Toast.makeText(mContext, "" + item.getID(), Toast.LENGTH_SHORT).show();
                    try {
                        JSONObject json = new JSONObject();
                        json.put("ID", item.getID());
                        json.put("CreatedBy", "0");
                        json.put("ModifiedBy", "0");
                        json.put("IsActive", "1");
                        FeedbackBottomSheetFragment.jsonArray_Value.put(json);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (!isSelectedAfterClick) {
                    holder.titleTextView.setBackgroundColor(mContext.getResources().getColor(R.color.label_color));
                }

            }
        });
//        for (int i = 0; i < FeedbackBottomSheetFragment.jsonArray_Value.length(); i++) {
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                //  FeedbackBottomSheetFragment.jsonArray_Value.remove(position);
//                // Log.e("Remove",""+SelectInterestActivity.jsonArray.remove(position));
//            } else {
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                    FeedbackBottomSheetFragment.jsonArray.remove(position);
//                }
//            }
//        }
    }

    @Override
    public int getItemCount() {
        return mGridData.size();
    }
}
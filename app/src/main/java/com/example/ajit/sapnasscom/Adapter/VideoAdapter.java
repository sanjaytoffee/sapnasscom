package com.example.ajit.sapnasscom.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.ajit.sapnasscom.Activity.LetsLearnActivity;
import com.example.ajit.sapnasscom.Model.VideoModel;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Utilities;

import java.util.List;

/**
 * Created by pratik on 12/7/2017.
 */

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.MyViewHolder> {

    private List<VideoModel> arrVideo;
    public Context context;
    String lastName = "", videpUrl = "", frameVideo = "", imagePath = "";
    myWebChromeClient mWebChromeClient;
    myWebViewClient mWebViewClient;
    VideoModel videoModel;
    private View mCustomView;
    private WebChromeClient.CustomViewCallback customViewCallback;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView, playImage;
        WebView webview;

        public MyViewHolder(View view) {
            super(view);
            playImage = (ImageView) view.findViewById(R.id.playImage);
            imageView = (ImageView) view.findViewById(R.id.videoview);
            webview = (WebView) view.findViewById(R.id.webView);
        }
    }

    public VideoAdapter(Context context, List<VideoModel> arrVideo) {
        this.context = context;
        this.arrVideo = arrVideo;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_vidieo, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Utilities.showLog(String.valueOf("Size=" + position));

        videoModel = arrVideo.get(position);

        if (videoModel.getCode().contains("https://www.youtube.com")) {
            lastName = videoModel.getCode().substring(videoModel.getCode().lastIndexOf('=') + 1);
            imagePath = "https://img.youtube.com/vi/" + lastName + "/0.jpg";
        } else if (videoModel.getCode().contains("https://youtu.be")) {
            lastName = videoModel.getCode().substring(videoModel.getCode().lastIndexOf('/') + 1);
            imagePath = "https://img.youtube.com/vi/" + lastName + "/0.jpg";
        }

        Glide.with(context)
                .load(imagePath)
                .apply(new RequestOptions().placeholder(R.drawable.youtube_thumbnell).error(R.drawable.youtube_thumbnell))
                .into(holder.imageView);

        holder.playImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                holder.playImage.setVisibility(View.GONE);
                holder.imageView.setVisibility(View.GONE);
                holder.webview.setVisibility(View.VISIBLE);

                videpUrl = videoModel.getCode();
                Log.e("URL=",""+videoModel.getCode());

                if (videpUrl.contains("https://www.youtube.com")) {
                    lastName = videpUrl.substring(videpUrl.lastIndexOf('=') + 1);
                } else if (videpUrl.contains("https://youtu.be")) {
                    lastName = videpUrl.substring(videpUrl.lastIndexOf('/') + 1);
                }

                Log.e("CODEVALUE0", "" + lastName);
                mWebViewClient = new myWebViewClient();
                holder.webview.setWebViewClient(mWebViewClient);
                mWebChromeClient = new myWebChromeClient(holder);
                holder.webview.setWebChromeClient(mWebChromeClient);

                holder.webview.getSettings().setJavaScriptEnabled(true);
                holder.webview.getSettings().setAppCacheEnabled(true);
                if(lastName.equalsIgnoreCase("youtu.be"))
                {
                    String str[]=videoModel.getCode().toString().split("=");
                    Log.e("URL",str[0]+","+str[1]);
                    String str_new[]=str[1].split("&");

                    frameVideo = "<html><body><iframe width=\"device-width\" height=\"240dp\" src=\"https://www.youtube.com/embed/Fem4gL0n4kk\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

                    frameVideo = frameVideo.replace("Fem4gL0n4kk", str_new[0]);
                }else {
                    frameVideo = "<html><body><iframe width=\"device-width\" height=\"240dp\" src=\"https://www.youtube.com/embed/Fem4gL0n4kk\" frameborder=\"0\" allowfullscreen></iframe></body></html>";

                    frameVideo = frameVideo.replace("Fem4gL0n4kk", lastName);
                }
                Log.e("MYFRAME", frameVideo);

                if (videpUrl.contains("https://www.youtube.com")) {
                    holder.webview.loadData(frameVideo, "text/html", "utf-8");
                } else if (videpUrl.contains("https://youtu.be")) {
                    holder.webview.loadData(frameVideo, "text/html", "utf-8");
                } else {
                    holder.webview.loadUrl(videpUrl);
                }
            }
        });
    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;
        MyViewHolder holder;

        public myWebChromeClient(MyViewHolder holder) {
            this.holder=holder;
        }


        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);    //To change body of overridden methods use File | Settings | File Templates.
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            LetsLearnActivity.customViewContainer.addView(view);
            LetsLearnActivity.lets_layout.setVisibility(View.GONE);
            LetsLearnActivity.customViewContainer.setVisibility(View.VISIBLE);

            customViewCallback = callback;
        }

        public void onProgressChanged(WebView view, int progress) {
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();    //To change body of overridden methods use File | Settings | File Templates.
            if (mCustomView == null)
                return;

            LetsLearnActivity.lets_layout.setVisibility(View.VISIBLE);
            LetsLearnActivity.customViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            LetsLearnActivity.customViewContainer.removeView(mCustomView);
            customViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
            //To change body of overridden methods use File | Settings | File Templates.
        }
    }
    @Override
    public int getItemCount() {
        return arrVideo.size();
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}

package com.example.ajit.sapnasscom.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.ajit.sapnasscom.Activity.SignupActivity;
import com.example.ajit.sapnasscom.Model.CurrentLocation;
import com.example.ajit.sapnasscom.Model.PreferredLocation;
import com.example.ajit.sapnasscom.R;

import java.util.ArrayList;

/**
 * Created by DTP-110 on 1/27/2017.
 */

public class PreferredLocation_Adapter extends BaseAdapter
{

    Activity mActivity;
    ArrayList<PreferredLocation> specialization;

    public PreferredLocation_Adapter(Activity a, ArrayList<PreferredLocation> data) {
        this.mActivity = a;
        this.specialization = data;
    }

    public PreferredLocation_Adapter(SignupActivity a, CurrentLocation currentLocation) {
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return specialization.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = ((LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.preferred_location_text, null);

            viewHolder.specilization_text = (TextView) convertView
                    .findViewById(R.id.preferred_locatio_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (specialization != null) {
            try {
                viewHolder.specilization_text.setText("" + specialization.get(position).getName());
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        return convertView;
    }

    class ViewHolder {
        TextView specilization_text;
    }

}



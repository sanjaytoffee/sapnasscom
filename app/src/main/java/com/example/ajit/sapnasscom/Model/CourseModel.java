package com.example.ajit.sapnasscom.Model;

/**
 * Created by pratik on 2/19/2018.
 */

public class CourseModel {

    String feadBack;
    String Duration;
    String Creator;
    String CityName;
    String Description;
    double Id;
    String IdValue;
    String CourseName;
    String CreatedOn;
    String LanguageName;
    String LearnStatus;
    int skillId;

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    public int getCourseCount() {
        return courseCount;
    }

    public void setCourseCount(int courseCount) {
        this.courseCount = courseCount;
    }

    int courseCount;

    public void setEnrolledCount(String enrolledCount) {
        this.enrolledCount = enrolledCount;
    }

    String enrolledCount;

    public String getEnrolledDate() {
        return EnrolledDate;
    }

    public void setEnrolledDate(String enrolledDate) {
        EnrolledDate = enrolledDate;
    }

    public String EnrolledDate;

    public String getKeyFeature() {
        return KeyFeature;
    }

    public void setKeyFeature(String keyFeature) {
        KeyFeature = keyFeature;
    }

    public String KeyFeature;

    public String getLearStatus() {
        return learStatus;
    }

    public void setLearStatus(String learStatus) {
        this.learStatus = learStatus;
    }

    public String learStatus;

    public boolean isEnrolled() {
        return Enrolled;
    }


    public void setEnrolled(boolean enrolled) {
        Enrolled = enrolled;
    }

    public boolean Enrolled;

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getLanguageName() {
        return LanguageName;
    }

    public void setLanguageName(String languageName) {
        LanguageName = languageName;
    }


    public String getLearnStatus() {
        return LearnStatus;
    }

    public void setLearnStatus(String learnStatus) {
        LearnStatus = learnStatus;
    }

    public String getFeadBack() {
        return feadBack;
    }

    public void setFeadBack(String feadBack) {
        this.feadBack = feadBack;
    }


    public String getIdValue() {
        return IdValue;
    }

    public void setIdValue(String idValue) {
        IdValue = idValue;
    }

    public double getId() {
        return Id;
    }

    public void setId(double id) {
        Id = id;
    }

    public String getCourseName() {
        return CourseName;
    }

    public void setCourseName(String courseName) {
        CourseName = courseName;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCreator() {
        return Creator;
    }

    public void setCreator(String creator) {
        Creator = creator;
    }

    public String getCityName() {
        return CityName;
    }

    public void setCityName(String cityName) {
        CityName = cityName;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getEnrolledCount() {
        return enrolledCount;
    }
}

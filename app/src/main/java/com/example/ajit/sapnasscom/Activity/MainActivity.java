package com.example.ajit.sapnasscom.Activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.MyProgressDialog;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.Fragment.CourseFragment;
import com.example.ajit.sapnasscom.Fragment.DiscoverFragment;
import com.example.ajit.sapnasscom.Fragment.JobsFragment;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.PrefUtils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.ajit.sapnasscom.Utils.Common.noInternet;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private TabLayout tabs;
    private ViewPager viewPager;
    private CircleImageView imgvw;
    private static final String TAG = MainActivity.class.getSimpleName();
    private boolean doubleBackToExitPressedOnce = false;
    private FragmentManager fragmentManager;
    private Fragment fragment = null;
    private SessionManager sessionManager;
    private String userId, userName, userEmail, resultValue;
    private Toolbar toolbar;
    private TextView profileName, profileEmail;
    public static Menu menu;
    public static NavigationView navigationView;
    LocaleClass localeClass;
    public static int Course = 0;
    String selectedLanguage = "", selectedItem = "", lang = "";
    DescoverWithoutLogin.Adapter adapter;
    ProgressDialog pd;
    public static TextView tvLogout, tvDeativate, textExitMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //intialization the Id
        init();

        pd = new ProgressDialog(MainActivity.this);
        menu = navigationView.getMenu();

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "MainActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.Dashboard);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

      /*  if (Common.isInternet(MainActivity.this)) {
         //   new CourseContentFragment.HttpAsyncTaskCourse().execute(CommonUrl.getCoursewithoutLogin);
        } else {
            Intent intent = new Intent(MainActivity.this, NoInternetActivity.class);
            startActivity(intent);
            finish();
        }*/
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        //tab
        tabs = (TabLayout) findViewById(R.id.tabs);
        /*if (CommenClass.isTablet(this)) {
            tabs.setTabMode(TabLayout.MODE_FIXED);
        } else {
            tabs.setTabMode(TabLayout.MODE_SCROLLABLE);
        }*/
        tabs.setupWithViewPager(viewPager);
        sessionManager = new SessionManager(getApplicationContext());

        HashMap<String, String> getmail = sessionManager.getEmail();
        HashMap<String, String> mail = sessionManager.getEmail();
        HashMap<String, String> name = sessionManager.getEmail();
        userId = getmail.get(SessionManager.KEY_USER_ID);
        userEmail = mail.get(SessionManager.KEY_EMAIL);
        userName = name.get(SessionManager.KEY_USERNAME);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);

        //View navView = navigationView.inflateHeaderView(R.layout.nav_header_main);
        imgvw = (CircleImageView) headerView.findViewById(R.id.profile_photo);

        profileName = (TextView) headerView.findViewById(R.id.navHeaderProfileName);
        profileEmail = (TextView) headerView.findViewById(R.id.navHeaderProfileEmail);

        profileName.setText(PrefUtils.getUserFullName(MainActivity.this));
        profileEmail.setText(PrefUtils.getUserEmail(MainActivity.this));

        imgvw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);
                Intent intent = new Intent(getApplicationContext(), EditProfileShowDataActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setupViewPager(final ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        viewPager.setOffscreenPageLimit(2);
        adapter.addFragment(new CourseFragment(), getResources().getString(R.string.Courses));
        adapter.addFragment(new JobsFragment(), getResources().getString(R.string.Jobs));
        adapter.addFragment(new DiscoverFragment(), getResources().getString(LocaleClass.Discover));
        viewPager.setAdapter(adapter);
        // viewPager.setCurrentItem(1);
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (position == 1) {
                    DiscoverFragment.discoverList.clear();
                    DiscoverFragment.rvDiscover.setVisibility(View.GONE);
                    DiscoverFragment.tvRecommendation.setVisibility(View.GONE);
                }
                if (position == 2) {
                    DiscoverFragment.radioGroup.clearCheck();
                    DiscoverFragment.industry_spinner.setVisibility(View.GONE);
                    DiscoverFragment.interest_spinner.setVisibility(View.GONE);
                    DiscoverFragment.discoverList.clear();
                    DiscoverFragment.rvDiscover.setVisibility(View.VISIBLE);
                    DiscoverFragment.tvRecommendation.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageSelected(int position) {
                if (position == 1) {
                    DiscoverFragment.discoverList.clear();
                    DiscoverFragment.rvDiscover.setVisibility(View.GONE);
                    DiscoverFragment.tvRecommendation.setVisibility(View.GONE);
                }
                if (position == 2) {
                    DiscoverFragment.radioGroup.clearCheck();
                    DiscoverFragment.rvDiscover.setVisibility(View.VISIBLE);
                    DiscoverFragment.industry_spinner.setVisibility(View.GONE);
                    DiscoverFragment.interest_spinner.setVisibility(View.GONE);
                    DiscoverFragment.discoverList.clear();
                    DiscoverFragment.tvRecommendation.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        if (doubleBackToExitPressedOnce) {

            openLogouut(MainActivity.this);
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click back again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setTab() {
        viewPager.setCurrentItem(2);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_dashboard) {
            viewPager.setCurrentItem(0);
        } else if (id == R.id.nav_support) {
            Intent intent = new Intent(getApplicationContext(), SupportActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_mycourse) {
            Intent intent = new Intent(getApplicationContext(), MycourseActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_interview) {
            if (Common.isInternet(MainActivity.this)) {
                Intent intent = new Intent(getApplicationContext(), InterviewActivity.class);
                startActivity(intent);
            } else {
                noInternet(MainActivity.this);
            }
        } else if (id == R.id.nav_myjob) {
            Intent intent = new Intent(MainActivity.this, MyJobsActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_signout) {
            askLogout(MainActivity.this);
        } else if (id == R.id.nav_rate) {
            Uri uri = Uri.parse("https://play.google.com/store?hl=en");
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        } else if (id == R.id.refer_friend) {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_TEXT, "Please Download  the App \nTo know about Related Jobs & Course. \nhttps://play.google.com/store?hl=en");
            startActivity(Intent.createChooser(i, "Share URL"));
        } else if (id == R.id.nav_exit) {
            askOption(MainActivity.this);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void openLogouut(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_dialog);
        textExitMessage = (TextView) dialog.findViewById(R.id.tvMessage);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvBtnYes);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.exit(0);
            }
        });
        textExitMessage.setText(R.string.exit_message);

        TextView cancel = (TextView) dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void askOption(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_dialog);
        tvDeativate = (TextView) dialog.findViewById(R.id.tvMessage);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvBtnYes);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Common.isInternet(MainActivity.this)) {
                    dialog.dismiss();
                    saveExit();
                } else {
                    noInternet(MainActivity.this);
                }
            }
        });
        tvDeativate.setText(R.string.deactivate);
        TextView cancel = (TextView) dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void askLogout(final Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.common_dialog);
        tvLogout = (TextView) dialog.findViewById(R.id.tvMessage);
        TextView tvOk = (TextView) dialog.findViewById(R.id.tvBtnYes);

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefUtils.clearPreference(MainActivity.this);
                sessionManager.clearPreference(MainActivity.this);
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
        tvLogout.setText(R.string.logout_string);

        TextView cancel = (TextView) dialog.findViewById(R.id.btnCancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        try {
            if (Common.dialog.isShowing()) {
                Common.dialog.dismiss();
            }
            if (pd.isShowing()) {
                pd.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (PrefUtils.getUser_Profileurl(MainActivity.this).length() != 0) {
            Picasso.with(getApplicationContext()).load(PrefUtils.getUser_Profileurl(MainActivity.this))
                    .placeholder(R.drawable.user).error(R.drawable.user)
                    .into(imgvw);
        }
        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "MainActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        init();
        setupViewPager(viewPager);
    }

    private void saveExit() {
        MyProgressDialog.showDialog(MainActivity.this);
        RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
        JSONObject object = new JSONObject();
        try {
            object.put("UserID", userId);
            object.put("status", 0);

            final String requestBody = object.toString();
            Log.e("Response", requestBody);

            Log.e("EXITapi", CommonUrl.USER_EXIT);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, CommonUrl.USER_EXIT, object, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.e("onResponse", response.toString());
                    MyProgressDialog.hideDialog();
                    PrefUtils.clearPreference(MainActivity.this);
                    sessionManager.logoutUser();
                    sessionManager.clearPreference(MainActivity.this);
                    Intent intent = new Intent(getApplicationContext(), LanguageScreenActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("onErrorResponse", error.toString());
                    MyProgressDialog.hideDialog();
                }
            }) {
                public String getBodyContentType() {
                    return "application/json; charset=utf-8";
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    String user = "unnati";
                    String pwd = "unnati@123";
                    headers.put("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
                    return headers;
                }
            };
            queue.add(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

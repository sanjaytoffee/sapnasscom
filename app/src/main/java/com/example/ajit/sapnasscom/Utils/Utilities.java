package com.example.ajit.sapnasscom.Utils;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utilities {
    static ProgressDialog pDialog = null;
    private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MMM-yyyy");
    private static SharedPreferences sharedPreferences;


    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }


    public static void showLog(String msg) {
       Log.e("LogPrint", msg);
    }
    public static void showErrorLog(String tag, String msg) {
        Log.e(tag, msg);
    }

    public static File bitmapToFile(Context context, String filename, Bitmap bitmap, boolean compress) {
        File f = new File(context.getCacheDir(), filename);
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        if (compress) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 45, bos);
        } else {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        }
        byte[] bitmapdata = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return f;

    }
    /* Fetch data from a network url, using a Get request */
    public static String fetchUrl(String stringUrl) {
        String dataString = "";

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(stringUrl);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000);
            urlConnection.setReadTimeout(8000);
            urlConnection.setDefaultUseCaches(true);
            urlConnection.setUseCaches(true);

            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // Read the input stream into a String
            InputStream inputStream = urlConnection.getInputStream();
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return "";
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line);
            }

            if (buffer.length() == 0) {
                // Stream was empty.  No point in parsing.
                return "";
            }
            dataString = buffer.toString();
//            Log.d(Util.TAG_URL_PARSE + " : ", dataString);

        } catch (IOException e) {
//            Log.d(Util.TAG_URL_PARSE + " : ", "Network IOException In FileFetch: \n" + e.toString());
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    e.printStackTrace();
//                    Log.e(LOG_TAG, "Error closing InputStream", e);
                }
            }
        }

        return dataString;
    }


    public static void showTimeOutToast(Context context) {
        Toast.makeText(context, "Timeout.. Please try again", Toast.LENGTH_SHORT).show();
    }

    public static void showNoConnectionToast(Context context) {
        Toast.makeText(context, "No Internet connection", Toast.LENGTH_SHORT).show();
    }

    public static void showSomethingWrongToast(Context context) {
        Toast.makeText(context, "Something went wrong.\nPlease try again.", Toast.LENGTH_SHORT).show();
    }

    public static void handleVolleyError(Activity context, VolleyError volleyError) {
        volleyError.printStackTrace();

        if (volleyError instanceof TimeoutError) {
//            Utilities.showToast(context, "Time Out Error");

        } else if (volleyError instanceof NoConnectionError) {
//            Utilities.showToast(context, "No Connection Error");

        } else if (volleyError instanceof AuthFailureError) {
            //TODO
//            Toast.makeText(context, "Authentication Failure", Toast.LENGTH_LONG).show();
        } else if (volleyError instanceof ServerError) {
            //TODO
//            Toast.makeText(context, "Server Error", Toast.LENGTH_LONG).show();
        } else if (volleyError instanceof NetworkError) {
            //TODO
//            Toast.makeText(context, "Network Error", Toast.LENGTH_LONG).show();
        } else if (volleyError instanceof ParseError) {
            //TODO
//            Toast.makeText(context, "Parse Error", Toast.LENGTH_LONG).show();
        }
    }

//    public static void handleFailJsonResponse(Context context, String response) {
//        try {
//            JSONObject jsonObject = new JSONObject(response);
//            if (jsonObject.has("status")) {
//                if (jsonObject.getString("status").equalsIgnoreCase("fail")) {
//                    if (jsonObject.has("reason")) {
//                        showErrorLog(jsonObject.getString("reason"));
//                        showToast(context, jsonObject.getString("reason"));
//                    }
//                    if (jsonObject.has("message")) {
//                        showErrorLog(jsonObject.getString("message"));
//                        showToast(context, jsonObject.getString("message"));
//                    }
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//    }

    public static String longToDate(long timeInMillis) {
        Date date = new Date(timeInMillis);
        return simpleDateFormat.format(date);
    }

    public static int dpToPx(int dp, DisplayMetrics metrics) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }


    public static long getAbsoluteTime(long offset, Long epochtime) {
        long currentTime = epochtime;
        long q = offset / (24 * 60 * 60 * 1000);
        long r = offset % (24 * 60 * 60 * 1000);
        long absoluteTime = (currentTime - getOffsetTime(currentTime)) + q * (24 * 60 * 60 * 1000) + r;
        return absoluteTime;
    }

    public static long getOffsetTime(long time) {

        long offset = time % (24 * 60 * 60 * 1000);
        return offset;
    }

    public static boolean checkGPSPermission(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            Toast.makeText(context, "lower version", Toast.LENGTH_SHORT).show();
            return true;
        } else {
//            Toast.makeText(context, "marshmallow or higher", Toast.LENGTH_SHORT).show();
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                Toast.makeText(context, "permission granted", Toast.LENGTH_SHORT).show();
                return true;
            } else {
                Toast.makeText(context, "no permission granted", Toast.LENGTH_SHORT).show();
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(context, "GPS permission allows us to access location data. Please allow in App Settings for additional functionality.", Toast.LENGTH_LONG).show();
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                } else {
                    ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
            }
        }
        return false;
    }





}

package com.example.ajit.sapnasscom.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Base64;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import com.example.ajit.sapnasscom.Common.CommonUrl;
import com.example.ajit.sapnasscom.Common.LocaleClass;
import com.example.ajit.sapnasscom.Common.SessionManager;
import com.example.ajit.sapnasscom.R;
import com.example.ajit.sapnasscom.Utils.Common;
import com.example.ajit.sapnasscom.Utils.Utilities;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.TokenParser;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by HSTPL on 3/7/2018.
 */

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    public static TextView tvTitle, tvDuration, tvLocation, tv_description_duration,tv_languages,tv__cource_location, tvLanguage, tvKeyfeatures, tvDescription, tvEnrolledButton, tv_features_header, tv_course_features_header;
    String courseId, userId, title, duration, location, language, keyFeatures, description,learnStatus;
    Intent intent;
    Boolean enrolled;
    public static boolean enrolledValue, enrolledStatus;
    SessionManager sessionManager;
    LocaleClass localeClass;
    int SkillId;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_detail_description);
        sessionManager = new SessionManager(DetailsActivity.this);
        init();

        try {
            localeClass = new LocaleClass();
            localeClass.loadLocale(getApplicationContext(), "DetailsActivity");
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        intent = getIntent();
        if (intent != null) {
            courseId = intent.getStringExtra("COURSEID");
            //courseId = String.valueOf(d.intValue());
            title = intent.getStringExtra("TITTLE");
            duration = intent.getStringExtra("DURATION");
            location = intent.getStringExtra("LOCATION");
            language = intent.getStringExtra("LANGUAGE");
            enrolledValue = intent.getBooleanExtra("COURSEENROLLED", false);
            keyFeatures = intent.getStringExtra("KEYFEATURES");
            description = intent.getStringExtra("DESCRIPTION");
            learnStatus = intent.getStringExtra("LEARNSTATUS");
            SkillId = intent.getIntExtra("SkillId",0);

            HashMap<String, String> getmail = sessionManager.getEmail();
            userId = getmail.get(SessionManager.KEY_USER_ID);

            setData();
        }
    }

    private void setData() {
        tvTitle.setText(title);
        tvDuration.setText(duration);
        tvLocation.setText(location);
        tvLanguage.setText(language);
        tv_features_header.setText(R.string.Course_Key_Features);
        tv_course_features_header.setText(R.string.Course_Details);
        tv_description_duration.setText(R.string.Duration);
        tv__cource_location.setText(R.string.Location);
        tv_languages.setText(R.string.Languages);

        Utilities.showLog("CouseID" + courseId + " " + "ENROLLEDSTATUS" + " " + enrolledValue);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            tvKeyfeatures.setText(Html.fromHtml(keyFeatures));
            tvDescription.setText(Html.fromHtml(description));
        } else {
            tvKeyfeatures.setText(Html.fromHtml(keyFeatures, Html.FROM_HTML_MODE_COMPACT));
            tvDescription.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
        }

        if (enrolledValue) {
            if (learnStatus.equalsIgnoreCase("Let learn")) {
                tvEnrolledButton.setText(R.string.start_course);
            } else if (learnStatus.equalsIgnoreCase("Finish")) {
                tvEnrolledButton.setText(R.string.finishh);
            } else {
                tvEnrolledButton.setText(R.string.continue_course);
            }
           // tvEnrolledButton.setText(R.string.Enrolled);
        } else {
            tvEnrolledButton.setText(R.string.Enroll_Now);
        }
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) findViewById(R.id.tv_course_title);
        tvDuration = (TextView) findViewById(R.id.tv_description_experience);
        tvLocation = (TextView) findViewById(R.id.tv_des_location_name);
        tvLanguage = (TextView) findViewById(R.id.tv_des_laguages_name);
        tvKeyfeatures = (TextView) findViewById(R.id.tv_des_features_content);
        tvDescription = (TextView) findViewById(R.id.tv_des_course_detail_content);
        tvEnrolledButton = (TextView) findViewById(R.id.tv_des_enroll_now);

        tv_features_header = (TextView) findViewById(R.id.tv_course_features_headerss);
        tv_course_features_header = (TextView) findViewById(R.id.tv_course_detail_header);
        tv_description_duration = (TextView) findViewById(R.id.tv_description_duration);
        tv__cource_location = (TextView) findViewById(R.id.tv__cource_location);
        tv_languages = (TextView) findViewById(R.id.tv_languages);

        tvEnrolledButton.setOnClickListener(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void onClick(View v) {
        if (v == tvEnrolledButton) {
            if (enrolledValue) {
                Intent intent = new Intent(DetailsActivity.this, LetsLearnActivity.class);
                intent.putExtra("courseid",  courseId);
                intent.putExtra("LearnStatus", learnStatus);
                intent.putExtra("enrolledValue", enrolledValue);
                intent.putExtra("courseName", title);
                intent.putExtra("SkillId", SkillId);
                startActivity(intent);
            } else {
                new HttpAsyncTaskCourseEnrolled().execute(CommonUrl.setCourseEnrolled);
            }
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private class HttpAsyncTaskCourseEnrolled extends AsyncTask<String, Void, String> {
        private HttpAsyncTaskCourseEnrolled() {
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected String doInBackground(String... urls) {

            return setEnrolled(urls[0]);
        }

        @SuppressLint({"CommitPrefEdits"})
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
//            rvCourse.setAdapter(courseAdapter);
            Utilities.showLog(result);
            if (enrolledStatus) {
                tvEnrolledButton.setText(R.string.start_course);
                Common.showApplyDialog(DetailsActivity.this, getString(R.string.enrolled));
            }
        }
    }

    public String setEnrolled(String url) {
        String result = "";
        System.out.print("Response : " + url);
        try {
            JSONObject params = new JSONObject();
            params.put("UserId", userId);
            Double d = new Double(courseId);
            params.put("CourseID", d);
            params.put("Enrolled", "true");

            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            try {
                connection.setRequestMethod(HttpPost.METHOD_NAME);
                String user = "unnati";
                String pwd = "unnati@123";
                connection.setRequestProperty("Authorization", "Basic " + Base64.encodeToString((user + ":" + pwd).getBytes(), Base64.NO_WRAP));
            } catch (ProtocolException e) {
                e.printStackTrace();
            }
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(false);
            connection.setInstanceFollowRedirects(false);
            connection.setRequestProperty("Accept", "*/*");
            String line = "";
            StringBuffer response = null;
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(params.toString());
            wr.flush();
            wr.close();
            try {
                BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                response = new StringBuffer();
                line = rd.readLine();
            } catch (Exception e) {
                e.printStackTrace();
            }

            response.append(line);
            response.append(TokenParser.CR);
            if (response != null) {
                try {
                   /* JSONObject jsonoObject = new JSONObject(response.toString());
                    //status = jsonoObject.getString("Result");*/
                    Utilities.showLog(response.toString());
                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject.getString("Result").equalsIgnoreCase("Success")) {
                        enrolledStatus = true;
                        enrolledValue = true;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}

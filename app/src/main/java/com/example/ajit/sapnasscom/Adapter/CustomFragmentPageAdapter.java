package com.example.ajit.sapnasscom.Adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.ajit.sapnasscom.Fragment.CourseContentFragment;
import com.example.ajit.sapnasscom.Fragment.DiscoverFragment;
import com.example.ajit.sapnasscom.Fragment.JobsFragment;
import com.example.ajit.sapnasscom.Fragment.CourseFragment;


public class CustomFragmentPageAdapter extends FragmentPagerAdapter {

    private static final String TAG = CustomFragmentPageAdapter.class.getSimpleName();

    private static final int FRAGMENT_COUNT = 3;

    public CustomFragmentPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new CourseFragment();
            case 1:
                return new JobsFragment();
            case 2:
               // return new DiscoverFragment();
                return new DiscoverFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "Course";
            case 1:
                return "Jobs";
            case 2:
                return "Discover";

        }
        return null;
    }
}
